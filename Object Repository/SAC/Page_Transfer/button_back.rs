<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_back</name>
   <tag></tag>
   <elementGuidId>d659abf3-5e0a-4479-b39b-fc80c5a50edf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div/sac-merchant/app-transaction-report/div[2]/div/div/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SAC/Page_CreateSubAccount/iframe</value>
      <webElementGuid>3bc014f2-d9b2-4117-bdbc-16f8da73992e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
