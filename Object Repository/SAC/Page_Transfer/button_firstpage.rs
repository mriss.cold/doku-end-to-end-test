<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_firstpage</name>
   <tag></tag>
   <elementGuidId>d6f31ba3-af63-441c-a806-e6d471019ab2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div/sac-merchant/app-transaction-report/div[2]/div/div/div/div[2]/table/tbody/tr[1]/td[1]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SAC/Page_CreateSubAccount/iframe</value>
      <webElementGuid>a5549230-189e-4585-a797-5d839254ea68</webElementGuid>
   </webElementProperties>
</WebElementEntity>
