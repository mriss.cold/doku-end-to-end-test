<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_submit</name>
   <tag></tag>
   <elementGuidId>dd3bffde-8f72-4941-af47-2b094a17111e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/ngb-modal-window/div/div/div[2]/form/div[2]/button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SAC/Page_CreateSubAccount/iframe</value>
      <webElementGuid>91bb0bcb-1eb1-42d6-8e5c-d67f9e4bc3fa</webElementGuid>
   </webElementProperties>
</WebElementEntity>
