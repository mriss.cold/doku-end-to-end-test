<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>table_firstrowemail</name>
   <tag></tag>
   <elementGuidId>91d3fcf2-d578-42c6-8edf-2a63da7aab3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div/sac-merchant/app-transaction-report/div[2]/div/div/div/div[2]/table/tbody/tr[1]/td[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SAC/Page_CreateSubAccount/iframe</value>
      <webElementGuid>d4765170-17cf-45eb-88e9-fd145b24c934</webElementGuid>
   </webElementProperties>
</WebElementEntity>
