<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Accounts</name>
   <tag></tag>
   <elementGuidId>63ce702a-5fa3-4e68-83b8-222e85b51170</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;dk_aside_menu&quot;]/ul/app-menu-list-item-v2[6]/li/a/span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#dk_aside_menu > ul > app-menu-list-item-v2.ng-tns-c98-63.ng-star-inserted > div > app-menu-list-item-v2.ng-tns-c98-92.ng-tns-c98-63.d-flex.ng-star-inserted > li > a > span.menu-text.ng-tns-c98-92</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>bc535a2d-0de7-4dc3-b117-5555b6a6297a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>menu-text ng-tns-c98-31</value>
      <webElementGuid>4715ceaa-9cfb-4068-bd4e-72085b6074bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Accounts </value>
      <webElementGuid>b028e81c-6e01-4b64-ba14-31eb4a112575</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dk_aside_menu&quot;)/ul[@class=&quot;menu-nav&quot;]/app-menu-list-item-v2[@class=&quot;ng-tns-c98-2 ng-star-inserted&quot;]/div[@class=&quot;submenu ng-tns-c98-2 expanded&quot;]/app-menu-list-item-v2[@class=&quot;ng-tns-c98-31 ng-tns-c98-2 d-flex ng-star-inserted&quot;]/li[@class=&quot;menu-item menu-item-submenu menu-item-open ng-tns-c98-31&quot;]/a[@class=&quot;menu-link menu-toggle ng-tns-c98-31 ng-star-inserted&quot;]/span[@class=&quot;menu-text ng-tns-c98-31&quot;]</value>
      <webElementGuid>b1f5e16d-bd3d-4a38-a5ca-900810efd318</webElementGuid>
   </webElementProperties>
</WebElementEntity>
