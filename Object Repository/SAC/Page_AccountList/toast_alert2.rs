<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>toast_alert2</name>
   <tag></tag>
   <elementGuidId>3a746242-501c-4bd2-83c6-9850b001e797</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div/sac-merchant/app-transaction-report/app-toast/ngb-toast/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/WEBUI/Page_AccountList/iframe</value>
      <webElementGuid>88b2f6fa-a747-493f-a870-d45fed7e3564</webElementGuid>
   </webElementProperties>
</WebElementEntity>
