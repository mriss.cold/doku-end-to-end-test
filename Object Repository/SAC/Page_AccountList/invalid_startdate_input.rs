<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>invalid_startdate_input</name>
   <tag></tag>
   <elementGuidId>8a0d028e-21a9-461f-b05b-8a6404f70dfa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ngbdatepicker class = 'dk-text-input ng-dirty ng-touched form-control is-invalid error-input ng-invalid']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>formcontrolname</name>
      <type>Main</type>
      <value>startDate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ngbdatepicker class</name>
      <type>Main</type>
      <value>dk-text-input ng-dirty ng-touched form-control is-invalid error-input ng-invalid</value>
   </webElementProperties>
</WebElementEntity>
