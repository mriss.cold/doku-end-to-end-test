<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_id</name>
   <tag></tag>
   <elementGuidId>60ed974a-3333-49c0-a71a-6539cf2aea4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div.content-wrapper > sac-merchant > app-transaction-report > div:nth-child(3) > div > div > div > div.table-responsive > table > tbody > tr:nth-child(1) > td:nth-child(1) > button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/sac-merchant/app-transaction-report/div[2]/div/div/div/div[2]/table/tbody/tr[1]/td[1]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/WEBUI/Page_AccountList/iframe</value>
   </webElementProperties>
</WebElementEntity>
