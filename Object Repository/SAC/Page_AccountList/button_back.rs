<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_back</name>
   <tag></tag>
   <elementGuidId>f64cbc06-c18a-4774-9b0f-08afe644c0c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div.content-wrapper > sac-merchant > app-transaction-report > div:nth-child(3) > div > div > div > button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/sac-merchant/app-transaction-report/div[2]/div/div/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/WEBUI/Page_AccountList/iframe</value>
   </webElementProperties>
</WebElementEntity>
