<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>toast_alert</name>
   <tag></tag>
   <elementGuidId>6a82b2e1-a959-4d52-93ea-7afe4cdbf0f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div/sac-merchant/app-transaction-report/ngb-toast[1]/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'toast-body']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div > sac-merchant > app-transaction-report > ngb-toast:nth-child(3) > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>toast-body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/WEBUI/Page_AccountList/iframe</value>
   </webElementProperties>
</WebElementEntity>
