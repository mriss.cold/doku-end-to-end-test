<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_enddate</name>
   <tag></tag>
   <elementGuidId>4cf3f975-0996-44ff-80e5-6125c40c8ab6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/sac-merchant/app-transaction-report/div[2]/div/div/div/form/div/div/div[3]/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//input[@formcontrolname = 'endDate' and @ref_element = 'Object Repository/WEBUI/Page_AccountList/iframe']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div.content-wrapper > sac-merchant > app-transaction-report > div:nth-child(2) > div > div > div > form > div > div > div:nth-child(2) > div > input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control dk-text-input ng-pristine ng-valid ng-touched</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>formcontrolname</name>
      <type>Main</type>
      <value>endDate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Select date</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/WEBUI/Page_AccountList/iframe</value>
   </webElementProperties>
</WebElementEntity>
