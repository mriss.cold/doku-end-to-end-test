<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Sub Account</name>
   <tag></tag>
   <elementGuidId>034dd969-33f7-4559-aabb-2707bd323532</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;dk_aside_menu&quot;]/ul/app-menu-list-item-v2[6]/div/app-menu-list-item-v2[5]/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.menu-text.ng-tns-c98-2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>d719f6cf-28fc-437a-9048-e4d255bacfe8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>menu-text ng-tns-c98-2</value>
      <webElementGuid>e74d2c2f-3191-4259-92a0-98f1168e8159</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Finance </value>
      <webElementGuid>2192cfc2-1fb7-444f-874b-90a10baf5e7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dk_aside_menu&quot;)/ul[@class=&quot;menu-nav&quot;]/app-menu-list-item-v2[@class=&quot;ng-tns-c98-2 ng-star-inserted&quot;]/li[@class=&quot;menu-item menu-item-submenu menu-item-open ng-tns-c98-2&quot;]/a[@class=&quot;menu-link menu-toggle ng-tns-c98-2 ng-star-inserted expanded&quot;]/span[@class=&quot;menu-text ng-tns-c98-2&quot;]</value>
      <webElementGuid>62df12c6-710e-41ab-927b-bdad30acf61f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dk_aside_menu']/ul/app-menu-list-item-v2[3]/li/a/span[2]</value>
      <webElementGuid>33968d4e-8ce9-4f9a-8c70-c0c530ab2c8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Akulaku'])[1]/following::span[2]</value>
      <webElementGuid>5994b193-6b2f-42f7-8448-91059de5af42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BRI Direct Debit'])[1]/following::span[4]</value>
      <webElementGuid>ad0ec410-871e-4206-acd1-97a6e5d61c9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settlement'])[1]/preceding::span[3]</value>
      <webElementGuid>4a535caa-428b-434d-89bf-7ad52c1df532</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank Account'])[1]/preceding::span[5]</value>
      <webElementGuid>bc3b5909-3c6d-409d-8ae6-59e4f9cb8615</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Finance']/parent::*</value>
      <webElementGuid>3b2d4be2-e830-4508-a172-f482b44709ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-menu-list-item-v2[3]/li/a/span[2]</value>
      <webElementGuid>c816a40a-9063-4f2f-825f-c5d71436c41b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
