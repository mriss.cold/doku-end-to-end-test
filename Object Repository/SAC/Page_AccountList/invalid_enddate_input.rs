<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>invalid_enddate_input</name>
   <tag></tag>
   <elementGuidId>45098cd7-602b-4906-aac4-d1593a39be8f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>body > div.content-wrapper > sac-merchant > app-transaction-report > div:nth-child(3) > div > div > div > form > div > div > div:nth-child(2) > div > input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/sac-merchant/app-transaction-report/div[2]/div/div/div/form/div/div/div[2]/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//input[@formcontrolname = 'endDate' and @ngbdatepicker class = 'dk-text-input form-control is-invalid error-input ng-dirty ng-invalid ng-touched']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>formcontrolname</name>
      <type>Main</type>
      <value>endDate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ngbdatepicker class</name>
      <type>Main</type>
      <value>dk-text-input form-control is-invalid error-input ng-dirty ng-invalid ng-touched</value>
   </webElementProperties>
</WebElementEntity>
