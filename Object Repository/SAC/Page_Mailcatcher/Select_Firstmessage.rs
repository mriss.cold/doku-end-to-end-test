<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select_Firstmessage</name>
   <tag></tag>
   <elementGuidId>bbae734a-e60e-4e7e-b088-f4cd5e1cf53e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#messages > table > tbody > tr:nth-child(1) > td:nth-child(2)</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
