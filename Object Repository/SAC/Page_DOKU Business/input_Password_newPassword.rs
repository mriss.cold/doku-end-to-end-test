<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Password_newPassword</name>
   <tag></tag>
   <elementGuidId>b11a3b59-62fc-41f7-8682-9a7d777566a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='password']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#password</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'password' and @name = 'newPassword' and @placeholder = 'New Password' and @type = 'password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b636ebe3-e882-40ec-8b04-07fd5710fc92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>newPassword</value>
      <webElementGuid>0cc12717-8ec5-4a76-b853-6a5ddb953032</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>formcontrolname</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>f76b6e19-f2d8-4425-ba49-3bba765b671c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>0f79c42d-f47d-4fbf-9019-7e3e9574907e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>475f2717-e236-4b6d-9534-79c6197f3bb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>newPassword</value>
      <webElementGuid>ce8763bd-15eb-49eb-864a-989474b4e320</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>New Password</value>
      <webElementGuid>b2c1ea33-a4c3-4efb-a4df-2f4008f10e28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>753d18ba-99c3-4400-b345-cd656a7afee0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-reflect-maxlength</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>2a27a29f-f426-4f56-9b6d-1d0f2404f45c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-reflect-name</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>9d359546-d3de-4ac5-9f15-943779dee436</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-untouched ng-pristine ng-invalid</value>
      <webElementGuid>487b08d7-c5d9-451e-8f6e-fb33e1285ff7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password&quot;)</value>
      <webElementGuid>d5016ea5-7c42-4eab-9fb4-593462bd1d84</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='password']</value>
      <webElementGuid>ebc331d3-9415-4039-9e34-9df51c804545</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='register']/div/div[3]/input</value>
      <webElementGuid>830d67b5-d990-4dec-a0bb-21592f6a8d5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>3d22e25f-bda7-4ae1-8aeb-c3cad248e223</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'password' and @name = 'newPassword' and @placeholder = 'New Password' and @type = 'password']</value>
      <webElementGuid>26b50116-9aae-481c-86f8-a6276a363c75</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
