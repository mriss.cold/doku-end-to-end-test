<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-BtnAccept</name>
   <tag></tag>
   <elementGuidId>39cf7905-cbe5-4b9d-8819-a281a3d672f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#cookie > div > div > div.col-md-6.pt-4 > button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;cookie&quot;]/div/div/div[2]/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
