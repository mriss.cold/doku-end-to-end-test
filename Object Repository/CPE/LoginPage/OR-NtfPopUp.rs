<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-NtfPopUp</name>
   <tag></tag>
   <elementGuidId>3efaf9fa-580a-4ded-8cb9-8fbeb1d03b9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.swal2-popup.swal2-modal.swal2-icon-error.swal2-show > div.swal2-header</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
