<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-BtnSignOut</name>
   <tag></tag>
   <elementGuidId>62735866-5af6-4beb-8928-b44effe9e2ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.navbar-nav.navbar-nav-right.profile-div > li.nav-item.text-nowrap > a.btn.btn-primary
/html/body/app-root/app-dashboard/div/div/app-navbar/nav[1]/div[2]/ul/li/a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;dk_body&quot;]/div/div/div[1]/div[5]/div/div[2]/div/a[3]
</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
