<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-FormNewPassword</name>
   <tag></tag>
   <elementGuidId>b21d8cfe-9b87-4f4e-a129-50d694d7a153</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='new-password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/UserActivity/UserProfilePage/OR-Iframe</value>
   </webElementProperties>
</WebElementEntity>
