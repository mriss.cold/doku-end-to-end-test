<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-TxtNotificationError</name>
   <tag></tag>
   <elementGuidId>8c1a4c5f-9ebd-4885-b75f-f5bf4b166597</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;profile&quot;]/div[2]/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/UserActivity/UserProfilePage/OR-Iframe</value>
   </webElementProperties>
</WebElementEntity>
