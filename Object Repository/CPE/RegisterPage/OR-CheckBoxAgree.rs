<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-CheckBoxAgree</name>
   <tag></tag>
   <elementGuidId>f15e203a-d6b9-4b8c-b906-561081aaaed0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.form-check > input#defaultCheck1.form-check-input.ng-valid.ng-dirty.ng-touched</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'checkbox1']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;defaultCheck1&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkbox1</value>
   </webElementProperties>
</WebElementEntity>
