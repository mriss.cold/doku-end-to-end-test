<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR008 - BillToEmail</name>
   <tag></tag>
   <elementGuidId>19c003f8-5ba9-439c-9285-d1d395c67580</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'emailBill' and @ref_element = 'Object Repository/Val/iframe']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>emailBill</value>
   </webElementProperties>
</WebElementEntity>
