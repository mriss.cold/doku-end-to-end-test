<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR012 - CustomerHours</name>
   <tag></tag>
   <elementGuidId>50300e1e-4886-4e54-bdff-a45702169f3c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#expiryTime > fieldset > div > div.ngb-tp-input-container.ngb-tp-hour > input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
