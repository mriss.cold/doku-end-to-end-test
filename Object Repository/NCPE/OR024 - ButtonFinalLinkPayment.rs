<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR024 - ButtonFinalLinkPayment</name>
   <tag></tag>
   <elementGuidId>04d27f44-03fa-48c7-9252-c4a5b08bf137</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>body > ngb-modal-window > div > div > div.modal-footer > button.btn.btn-primary.mr-3</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'buttonCreateLinkModal' and @ref_element = 'Object Repository/Val/iframe']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonCreateLinkModal</value>
   </webElementProperties>
</WebElementEntity>
