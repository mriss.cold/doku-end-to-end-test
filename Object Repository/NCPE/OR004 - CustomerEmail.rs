<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR004 - CustomerEmail</name>
   <tag></tag>
   <elementGuidId>27dcbdfe-a879-4fa9-a73b-60047113ab0b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'email' and @ref_element = 'Object Repository/Val/iframe']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>email</value>
   </webElementProperties>
</WebElementEntity>
