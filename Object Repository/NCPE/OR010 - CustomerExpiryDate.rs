<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR010 - CustomerExpiryDate</name>
   <tag></tag>
   <elementGuidId>d8734031-9ccf-4d9b-8fca-db402ea40a23</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@formcontrolname = 'expiryDate' and @ref_element = 'Object Repository/Val/iframe']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>formcontrolname</name>
      <type>Main</type>
      <value>expiryDate</value>
   </webElementProperties>
</WebElementEntity>
