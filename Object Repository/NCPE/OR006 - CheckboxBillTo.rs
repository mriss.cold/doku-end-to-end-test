<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR006 - CheckboxBillTo</name>
   <tag></tag>
   <elementGuidId>f83594b8-8c89-4ed9-b70e-19842dccf912</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'billCheckbox' and @ref_element = 'Object Repository/Val/iframe']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>billCheckbox</value>
   </webElementProperties>
</WebElementEntity>
