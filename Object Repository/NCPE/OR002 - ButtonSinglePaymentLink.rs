<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR002 - ButtonSinglePaymentLink</name>
   <tag></tag>
   <elementGuidId>1f1b0a26-97f6-4998-baa4-9ff44b48d2c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'singlePayment' and @ref_element = 'Object Repository/NCPE/OR025 - iframe']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>singlePayment</value>
   </webElementProperties>
</WebElementEntity>
