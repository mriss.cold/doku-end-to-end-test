<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR019 - ButtonAddItems</name>
   <tag></tag>
   <elementGuidId>5b8e8309-8267-4561-a1ab-cd7ec21a8914</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'buttonAddItem' and @ref_element = 'Object Repository/Val/iframe']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-link.m-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonAddItem</value>
   </webElementProperties>
</WebElementEntity>
