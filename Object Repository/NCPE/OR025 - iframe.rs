<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR025 - iframe</name>
   <tag></tag>
   <elementGuidId>e88c9a5f-dead-464f-a762-889cdfeb3fcc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>iframe</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='dk_content']/div/div/span/app-iframe/div/iframe</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
