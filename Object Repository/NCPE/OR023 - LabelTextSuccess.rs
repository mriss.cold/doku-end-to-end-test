<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR023 - LabelTextSuccess</name>
   <tag></tag>
   <elementGuidId>84ca4caf-9144-477a-a89b-cc06fbc9cdad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/Val/iframe']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > ngb-modal-window > div > div > div.modal-header > h5 > div > div > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Val/iframe</value>
   </webElementProperties>
</WebElementEntity>
