<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR005 - CustomerAddress</name>
   <tag></tag>
   <elementGuidId>546d84a3-a649-4659-974a-50afcbb837b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'address' and @ref_element = 'Object Repository/Val/iframe']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>address</value>
   </webElementProperties>
</WebElementEntity>
