<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_business_id</name>
   <tag></tag>
   <elementGuidId>d86fb7d9-f2f4-4d45-8591-843751b6bcb5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table//tr[1]/td[count(//table//th[contains(text(), 'Business ID')]/preceding-sibling::*)+1]//a</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>979115ee-9e28-48a2-8b39-b582ba3328f2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
