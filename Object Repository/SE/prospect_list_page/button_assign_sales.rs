<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_assign_sales</name>
   <tag></tag>
   <elementGuidId>4a3774eb-d743-45b3-a020-f2d756ea7b8c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class = 'dropdown-menu show']/parent::*//button[contains(text(), 'Assign Sales')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>d1e1c198-c74b-4ee9-94e1-123a705e1cb4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
