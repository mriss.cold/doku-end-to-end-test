<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_tipe_bisnis</name>
   <tag></tag>
   <elementGuidId>b83fd531-8e9d-4784-a142-adb6e44ffd2b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table//tr[1]//td[count(//table//th[contains(text(), 'Tipe Bisnis')]/preceding-sibling::*)+1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>0a4c75be-bd34-40ca-83d7-876c87b18847</webElementGuid>
   </webElementProperties>
</WebElementEntity>
