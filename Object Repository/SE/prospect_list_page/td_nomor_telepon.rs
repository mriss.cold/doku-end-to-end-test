<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_nomor_telepon</name>
   <tag></tag>
   <elementGuidId>2d4e6b48-b802-41c1-ab9c-4131b6e6f04b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table//tr[1]//td[count(//table//th[contains(text(), 'Nomor Telepon')]/preceding-sibling::*)+1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>a5240896-631a-4908-a249-ed312f874dc9</webElementGuid>
   </webElementProperties>
</WebElementEntity>
