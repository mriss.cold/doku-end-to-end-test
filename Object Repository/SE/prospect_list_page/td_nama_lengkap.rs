<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_nama_lengkap</name>
   <tag></tag>
   <elementGuidId>b7b17e0f-7134-45b0-be42-094daa73310a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table//tr[1]//td[count(//table//th[contains(text(), 'Nama Lengkap')]/preceding-sibling::*)+1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>243cac12-a193-4786-8d16-e57be8a8cb03</webElementGuid>
   </webElementProperties>
</WebElementEntity>
