<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_ellipsis</name>
   <tag></tag>
   <elementGuidId>3c5ef339-58ba-4891-a203-239617681c2f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//i[(@class = 'fa fa-ellipsis-v')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>b9275cd0-928c-43b4-813e-c48947db8682</webElementGuid>
   </webElementProperties>
</WebElementEntity>
