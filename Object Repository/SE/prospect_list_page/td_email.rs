<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_email</name>
   <tag></tag>
   <elementGuidId>5d9d1b43-a191-450e-b3b4-b2ac1a23149a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table//tr[1]//td[count(//table//th[contains(text(), 'Email')]/preceding-sibling::*)+1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>6988f13a-a69d-454c-bc3c-f5ecd9286f05</webElementGuid>
   </webElementProperties>
</WebElementEntity>
