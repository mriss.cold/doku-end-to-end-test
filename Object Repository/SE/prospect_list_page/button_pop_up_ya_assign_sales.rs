<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_pop_up_ya_assign_sales</name>
   <tag></tag>
   <elementGuidId>5c16aec1-d3f4-422f-9161-85e6fa0eb3b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(text(), 'YA')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>36f398b3-4a76-42dd-90be-15c088a08fe3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
