<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_data_tidak_ditemukan</name>
   <tag></tag>
   <elementGuidId>b5b43fd2-d25f-49c3-9de6-6684ed81ff2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[contains(text(), 'Data tidak ditemukan')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>deaccd17-174f-46d0-9b8a-1b79ba39e027</webElementGuid>
   </webElementProperties>
</WebElementEntity>
