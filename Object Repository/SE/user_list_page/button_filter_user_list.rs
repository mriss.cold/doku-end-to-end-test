<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_filter_user_list</name>
   <tag></tag>
   <elementGuidId>5e4a4f02-fdaf-4081-850e-c39f3d5e4a61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(text(), 'Filter')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/user_list_page/iframe_user_list</value>
      <webElementGuid>88c51b0c-8b3a-481b-a7ee-ae24fb43c5b8</webElementGuid>
   </webElementProperties>
</WebElementEntity>
