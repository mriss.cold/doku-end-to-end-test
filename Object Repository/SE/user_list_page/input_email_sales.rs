<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_email_sales</name>
   <tag></tag>
   <elementGuidId>a71d5c11-c3e3-4b03-aaed-a472a87f80ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'Input email sales')]/following-sibling::*/input[@type = 'text']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/user_list_page/iframe_user_list</value>
      <webElementGuid>7ba2689b-7f54-4b14-88d6-815451e5185e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
