<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_name</name>
   <tag></tag>
   <elementGuidId>0fdf9fa9-0e57-4409-99ae-0ca4d8537089</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table//tr[1]//td[count(//table//th[contains(text(), 'Name')]/preceding-sibling::*)+1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/user_list_page/iframe_user_list</value>
      <webElementGuid>5749de4b-3318-4fd1-b748-dabfe4a0982a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
