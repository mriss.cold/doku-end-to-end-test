<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_apply_filter</name>
   <tag></tag>
   <elementGuidId>0a7fc092-ba6d-4579-9dba-7a274e322032</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(), 'APPLY FILTER')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/user_list_page/iframe_user_list</value>
      <webElementGuid>ce143e45-70e4-4479-b177-86abc5cf7efb</webElementGuid>
   </webElementProperties>
</WebElementEntity>
