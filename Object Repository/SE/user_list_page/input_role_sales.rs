<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_role_sales</name>
   <tag></tag>
   <elementGuidId>9bb43d76-24f8-4289-b696-be8f09d2b4f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'Input role')]/following-sibling::*/input[@type = 'text']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/user_list_page/iframe_user_list</value>
      <webElementGuid>9df80df6-a907-4d9e-bd1f-2cad4c1d6f05</webElementGuid>
   </webElementProperties>
</WebElementEntity>
