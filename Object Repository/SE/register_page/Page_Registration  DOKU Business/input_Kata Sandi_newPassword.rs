<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Kata Sandi_newPassword</name>
   <tag></tag>
   <elementGuidId>66f9eda3-86c8-4234-8f96-8b8317791695</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='password']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#password</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ad460711-887e-4217-8b6c-70d642734041</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>newPassword</value>
      <webElementGuid>8eb57529-d7d9-4d87-9f97-9f9cbba65ca3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>formcontrolname</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>7f3d808c-bfb1-4f7e-a073-eefda3685c26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>849b18fc-adb3-4656-8155-da2f3da030c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>16</value>
      <webElementGuid>492538c7-1426-4f44-bef8-84d98c34670a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>newPassword</value>
      <webElementGuid>f2238dc9-e136-47fd-8a29-a3ed5bb0e1f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-reflect-maxlength</name>
      <type>Main</type>
      <value>16</value>
      <webElementGuid>d421d544-a8b2-4272-8aa2-4be6f4b22313</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-reflect-name</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>a7c36dd7-2854-4201-9c0f-529ac3a3d675</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>ac66c0e4-442b-4e30-921b-1444fc07cff3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-untouched ng-invalid ng-dirty</value>
      <webElementGuid>a414fb55-8a55-4040-9f99-2594600080ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Masukkan kata sandi</value>
      <webElementGuid>8abfd6c3-c7c4-4278-99cd-180f15009c2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password&quot;)</value>
      <webElementGuid>ce2c5fa7-acd2-4526-8f5c-a89e112ffc92</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='password']</value>
      <webElementGuid>2bd5f2f1-ef1f-46b4-8422-64c71efff7fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='register']/div/div[5]/div/input</value>
      <webElementGuid>cfc46c1a-7e20-4185-a00b-4cd46c45b043</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/input</value>
      <webElementGuid>3e39b6ef-44da-4635-bceb-9f3398a7dc85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'password' and @name = 'newPassword' and @type = 'password' and @placeholder = 'Masukkan kata sandi']</value>
      <webElementGuid>424b3cb7-33ae-4505-9ded-b191c270fd3a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
