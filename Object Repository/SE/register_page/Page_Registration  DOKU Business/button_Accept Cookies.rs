<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Accept Cookies</name>
   <tag></tag>
   <elementGuidId>b870c914-f128-4288-8f76-c72941643a9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@onclick='cookies()']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-md.btn-primary.accept-cookies</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>0670c453-4505-4204-bf5a-2d8f694a7700</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-md btn-primary accept-cookies</value>
      <webElementGuid>d01653a3-98f4-4dab-a5e5-0712d6956af7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>cookies()</value>
      <webElementGuid>44e5b195-fb5d-4993-be11-933d94a65adc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Accept Cookies</value>
      <webElementGuid>291d9003-c896-4d83-85f7-e9babf89add9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cookie&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;col-md-6 pt-4&quot;]/button[@class=&quot;btn btn-md btn-primary accept-cookies&quot;]</value>
      <webElementGuid>3e019d61-2b9b-4de0-86e6-723bdc54444b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@onclick='cookies()']</value>
      <webElementGuid>ab5812ef-f27e-4cbf-9be5-02155c895a0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='cookie']/div/div/div[2]/button</value>
      <webElementGuid>078b5800-eb13-407a-a7d2-b7eeba597609</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Website kami menggunakan teknologi cookie demi pengalaman pengguna yang lebih baik.'])[1]/following::button[1]</value>
      <webElementGuid>b5d3eea3-0a56-4c1e-a88f-a5c3d10577e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In order to provide better user experience. This site is using cookies and related technology.'])[1]/following::button[1]</value>
      <webElementGuid>4f79b34f-9b5c-4820-81e7-4bf3cf7a8204</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Accept Cookies']/parent::*</value>
      <webElementGuid>52f2dad8-f898-4aef-a33f-ea315b430ec3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>b83cce50-a5a5-480d-ab90-9dcf107a2546</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Accept Cookies' or . = 'Accept Cookies')]</value>
      <webElementGuid>24b9681e-a663-46b2-bd6f-92dd9d41b29f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
