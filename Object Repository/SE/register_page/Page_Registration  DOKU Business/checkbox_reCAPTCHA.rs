<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkbox_reCAPTCHA</name>
   <tag></tag>
   <elementGuidId>40e80013-3965-4b2f-97d9-d7a196de88ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='recaptcha-anchor']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#katalon-rec_elementInfoDiv</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0dc5ba44-c5b7-4c4a-ac44-04a3ceb79d24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>katalon-rec_elementInfoDiv</value>
      <webElementGuid>881015f3-a224-4c4b-b212-018adcceebe9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>id(&quot;katalon-rec_elementInfoDiv&quot;)</value>
      <webElementGuid>bcf812f8-6a35-48a8-b789-81f1ea529181</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;katalon-rec_elementInfoDiv&quot;)</value>
      <webElementGuid>344f63b4-55a4-4699-9a24-4be85a618937</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/register_page/Page_Registration  DOKU Business/iframe_reCAPTCHA</value>
      <webElementGuid>cd08f573-cd32-4433-b0ea-9571181284b7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='katalon-rec_elementInfoDiv']</value>
      <webElementGuid>45d93f18-61c5-4a12-a9aa-9ee68984d4e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='katalon']/div</value>
      <webElementGuid>f3838ab3-4c5b-4a87-84af-da2324d79657</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms'])[1]/following::div[2]</value>
      <webElementGuid>edaae985-765e-45b3-be2f-aed9e90adca1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Privacy'])[1]/following::div[2]</value>
      <webElementGuid>663ae4c4-4aff-4b62-8e09-eb09f5ff97f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='id(&quot;katalon-rec_elementInfoDiv&quot;)']/parent::*</value>
      <webElementGuid>ec781ff6-47c8-4d44-8d73-974238dbcf01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>e9a5c337-2bc7-41bb-9f49-22ed9bbe8867</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'katalon-rec_elementInfoDiv' and (text() = 'id(&quot;katalon-rec_elementInfoDiv&quot;)' or . = 'id(&quot;katalon-rec_elementInfoDiv&quot;)')]</value>
      <webElementGuid>3325f536-9de5-485b-bb92-bfe809ee01ff</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
