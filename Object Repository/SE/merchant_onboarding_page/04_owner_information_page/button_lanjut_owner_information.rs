<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_lanjut_owner_information</name>
   <tag></tag>
   <elementGuidId>e0e38b26-4c14-499c-99d7-afabf50bb595</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(), 'Lanjut')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/04_owner_information_page/iframe_step4_owner_information</value>
      <webElementGuid>564035f6-41de-4b59-add6-e023e5776480</webElementGuid>
   </webElementProperties>
</WebElementEntity>
