<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_step4_title</name>
   <tag></tag>
   <elementGuidId>e594d7b6-0891-4696-8d50-7554e7eb9d0b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h3[contains(text(), 'Informasi Pemilik')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/04_owner_information_page/iframe_step4_owner_information</value>
      <webElementGuid>ccf30ce5-567f-48c8-970e-60c9c574d93c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
