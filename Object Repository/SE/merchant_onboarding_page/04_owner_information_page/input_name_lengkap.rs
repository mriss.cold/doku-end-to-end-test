<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_name_lengkap</name>
   <tag></tag>
   <elementGuidId>e94f701b-717d-4ee0-9d69-e58070ff044d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'Nama lengkap wajib diisi')]/parent::*//input[@id = 'fullname']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/04_owner_information_page/iframe_step4_owner_information</value>
      <webElementGuid>f36edc96-7bb3-46c4-bd5a-eaf9fb8e38e1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
