<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_jabatan</name>
   <tag></tag>
   <elementGuidId>a6f49d42-4938-4d54-88d0-880b39d1161f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'Pilih jabatan')]/parent::*//input[@type = 'text'] </value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/04_owner_information_page/iframe_step4_owner_information</value>
      <webElementGuid>c07bae74-da2f-4d3d-985a-017a4e7a04bf</webElementGuid>
   </webElementProperties>
</WebElementEntity>
