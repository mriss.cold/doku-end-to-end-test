<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkbox_as_account</name>
   <tag></tag>
   <elementGuidId>446c381b-897e-466f-a318-861b350c88d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id = 'as-account']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/04_owner_information_page/iframe_step4_owner_information</value>
      <webElementGuid>376513f4-4679-4d10-bfb3-6f1904a80205</webElementGuid>
   </webElementProperties>
</WebElementEntity>
