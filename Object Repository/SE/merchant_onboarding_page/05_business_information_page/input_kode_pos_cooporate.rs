<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_kode_pos_cooporate</name>
   <tag></tag>
   <elementGuidId>c25320fd-452d-49b7-8512-a2b6fc1b8231</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(), 'Kode Pos - Wilayah - Kabupaten - Kota - Provinsi')]/parent::*//input[@type = 'text']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>a8357e97-9bed-4dc5-a250-8580d0d180de</webElementGuid>
   </webElementProperties>
</WebElementEntity>
