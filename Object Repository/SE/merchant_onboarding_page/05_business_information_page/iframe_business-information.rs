<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_business-information</name>
   <tag></tag>
   <elementGuidId>c4837d95-6869-4ecf-ad42-405ca20d87b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//iframe[contains(@src, '/activation/v2/activate-business/business-information')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
