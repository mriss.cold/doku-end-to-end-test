<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_pilih_media_instagram</name>
   <tag></tag>
   <elementGuidId>93659156-6e9a-4ef4-a337-959fff7fa14b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(text(), 'Pilih Media')]/following-sibling::div[@role = 'combobox']//input)[4]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>688bb77b-dd9e-49e7-b0fd-54fe6da1392f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
