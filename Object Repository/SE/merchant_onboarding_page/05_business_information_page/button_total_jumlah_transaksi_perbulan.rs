<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_total_jumlah_transaksi_perbulan</name>
   <tag></tag>
   <elementGuidId>7b66e623-6ec2-459c-b6e0-1fb6e4375414</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(), 'Total Jumlah Transaksi perbulan')]/parent::*//div[contains(text(), '20 Juta - 4 Miliar')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>cb621775-5293-4bcd-9052-4c03327024b3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
