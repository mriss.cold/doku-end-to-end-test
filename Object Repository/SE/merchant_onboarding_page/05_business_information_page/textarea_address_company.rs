<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_address_company</name>
   <tag></tag>
   <elementGuidId>435d5994-5ed5-4fd8-940a-2a49901dd3d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id = 'company-address']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>97d095de-d518-4b15-a5ee-2027ccaa4a21</webElementGuid>
   </webElementProperties>
</WebElementEntity>
