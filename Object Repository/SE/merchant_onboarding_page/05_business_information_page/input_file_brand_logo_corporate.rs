<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_file_brand_logo_corporate</name>
   <tag></tag>
   <elementGuidId>81d1a984-66f3-4232-9c8f-cc82983a78db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/label[contains(text(), 'Brand Logo')]/..//input[@type = 'file']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>bba14ecd-4d67-4669-b6ad-4c865027f5fc</webElementGuid>
   </webElementProperties>
</WebElementEntity>
