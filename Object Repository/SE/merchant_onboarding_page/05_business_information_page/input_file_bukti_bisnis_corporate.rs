<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_file_bukti_bisnis_corporate</name>
   <tag></tag>
   <elementGuidId>93729d8a-6b59-4a4c-b6f1-aeafc67a669f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'Bukti Bisnis')]/following-sibling::*//input[@type = 'file']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>cd2939ed-052d-4b44-bd84-444e5d6043f3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
