<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_step5_title</name>
   <tag></tag>
   <elementGuidId>fb777aaa-7474-44e1-9da0-a7888ee5e507</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h3[contains(text(), 'Informasi Bisnis')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>55b46af6-2960-4354-95d4-a1d593eaabd2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
