<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_pilih_badan_hukum_bisnis</name>
   <tag></tag>
   <elementGuidId>5f55ce22-08b6-44bb-936e-0257fe259af1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'Pilih badan hukum bisnis')]/parent::*//input[@type = 'text']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>69d283e6-e088-4ad3-be4d-6b9235256c4d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
