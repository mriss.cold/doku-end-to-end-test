<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_memiliki_web_media_sosial</name>
   <tag></tag>
   <elementGuidId>1fdd603c-5d70-4fd3-b17b-e8790292d518</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'Saya memiliki web/media sosial untuk berjualan')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>fbf994dd-370c-4689-abbe-dcda941fa22d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
