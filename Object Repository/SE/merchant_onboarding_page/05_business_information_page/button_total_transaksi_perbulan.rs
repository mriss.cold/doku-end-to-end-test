<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_total_transaksi_perbulan</name>
   <tag></tag>
   <elementGuidId>17a2e26d-b77d-4a06-9c42-cecd0c51a692</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(), 'Total transaksi perbulan')]/parent::*//div[contains(text(), '100 - 10.000')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>94f925fb-d37a-4002-982d-feca83b5048a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
