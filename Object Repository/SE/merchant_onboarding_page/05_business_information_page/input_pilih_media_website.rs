<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_pilih_media_website</name>
   <tag></tag>
   <elementGuidId>079ed764-58e9-496a-bbd2-511b2f9cbdb8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(text(), 'Pilih Media')]/following-sibling::div[@role = 'combobox']//input)[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>34690bf2-3335-493a-a0de-79b303b001a8</webElementGuid>
   </webElementProperties>
</WebElementEntity>
