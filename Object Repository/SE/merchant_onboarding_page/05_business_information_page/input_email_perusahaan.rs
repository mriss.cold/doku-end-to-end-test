<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_email_perusahaan</name>
   <tag></tag>
   <elementGuidId>919b8de2-c40d-4507-9e43-49609d3fbe5b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(), 'Email Perusahaan')]/parent::*/input[@type = 'text']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/05_business_information_page/iframe_business-information</value>
      <webElementGuid>3845f50e-627b-4b80-adce-fc9aeb48d7c2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
