<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_bisnis_anda_diaktifkan_title</name>
   <tag></tag>
   <elementGuidId>db0b55f0-1b8e-4e59-9ae5-cda486a5a6ea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h3[contains(text(), 'Bisnis Anda diaktifkan')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/10_complete/iframe_complete</value>
      <webElementGuid>90f39482-f07c-4ff6-bbad-fd1321ed701f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
