<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_step9_title</name>
   <tag></tag>
   <elementGuidId>6b305c4b-27d5-451e-9c4b-c21f5642b50c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h3[contains(text(), 'Syarat &amp; Ketentuan')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/09_term_and_condition/iframe_term_and_condition</value>
      <webElementGuid>a4d5d377-0bd3-4063-8f27-970f52c23feb</webElementGuid>
   </webElementProperties>
</WebElementEntity>
