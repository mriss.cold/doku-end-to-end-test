<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_no_npwp</name>
   <tag></tag>
   <elementGuidId>03a1ed18-dcd9-4fe7-8f81-441dac829915</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(), 'No. NPWP')]/parent::*/input[@type = 'text']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/08_upload_document/iframe_upload_document</value>
      <webElementGuid>84e4a8c9-dc55-42ff-aa1e-a358fb3cc34d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
