<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_file_dokumen_tambahan</name>
   <tag></tag>
   <elementGuidId>83283e0b-757d-4446-a29f-86ba72cb4aa7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'Lisensi Bappebti')]/parent::*/parent::*/parent::*/input[@type = 'file']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/08_upload_document/iframe_upload_document</value>
      <webElementGuid>f09696b1-3807-4171-bd09-a0966e99f1d8</webElementGuid>
   </webElementProperties>
</WebElementEntity>
