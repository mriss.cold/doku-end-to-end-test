<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_file_nomor_induk_berusaha</name>
   <tag></tag>
   <elementGuidId>97c27bed-2938-4b1f-9e49-74ec7e3dbea4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'NIB')]/parent::*/parent::*/parent::*/input[@type = 'file']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/08_upload_document/iframe_upload_document</value>
      <webElementGuid>28c6baf4-4ef7-4489-827b-93244e19783a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
