<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_lanjut_upload_document</name>
   <tag></tag>
   <elementGuidId>c4942129-8986-4abb-80de-47f9aed61b09</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(), 'Lanjut')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/08_upload_document/iframe_upload_document</value>
      <webElementGuid>ccb3579d-e7e2-4a69-a84d-43c5b26ed456</webElementGuid>
   </webElementProperties>
</WebElementEntity>
