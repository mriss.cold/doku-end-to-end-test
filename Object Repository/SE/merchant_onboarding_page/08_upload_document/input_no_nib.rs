<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_no_nib</name>
   <tag></tag>
   <elementGuidId>ff6bb87b-eb45-4dc7-a5f8-5ca76cec61c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(), 'No. NIB')]/parent::*/input[@type = 'text']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/08_upload_document/iframe_upload_document</value>
      <webElementGuid>c445d2a9-be09-48fe-9048-6c89bd5ddbe7</webElementGuid>
   </webElementProperties>
</WebElementEntity>
