<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_step8_corporate_title</name>
   <tag></tag>
   <elementGuidId>4918f609-2d57-43ac-9152-bbae8e87074a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h3[contains(text(), 'Unggah Dokumen')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/08_upload_document/iframe_upload_document</value>
      <webElementGuid>7bf4cfd5-c0b6-49f8-b2b8-e36ef168e84e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
