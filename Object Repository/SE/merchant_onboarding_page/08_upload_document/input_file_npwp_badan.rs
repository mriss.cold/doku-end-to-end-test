<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_file_npwp_badan</name>
   <tag></tag>
   <elementGuidId>43c3bbdb-c5bd-4e9e-a223-ea695a5c7bbf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'NPWP')]/parent::*/parent::*/parent::*/input[@type = 'file']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/08_upload_document/iframe_upload_document</value>
      <webElementGuid>29d4e884-2bc4-4350-b5ed-11f933cd508d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
