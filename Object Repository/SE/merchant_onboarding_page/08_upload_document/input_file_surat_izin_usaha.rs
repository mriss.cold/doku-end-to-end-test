<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_file_surat_izin_usaha</name>
   <tag></tag>
   <elementGuidId>f18b06aa-0003-4ec5-ad85-a1e8de1374f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'Surat Izin Usaha')]/parent::*/parent::*/parent::*/input[@type = 'file']
</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/08_upload_document/iframe_upload_document</value>
      <webElementGuid>4f2afaa1-c309-4048-9c69-a47cf8c31179</webElementGuid>
   </webElementProperties>
</WebElementEntity>
