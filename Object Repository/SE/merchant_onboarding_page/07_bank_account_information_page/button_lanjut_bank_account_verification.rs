<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_lanjut_bank_account_verification</name>
   <tag></tag>
   <elementGuidId>b35e42aa-4486-4a79-99de-23006e8693b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//app-main-action//span[contains(text(), 'Lanjut')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/07_bank_account_information_page/iframe_bank_account_information</value>
      <webElementGuid>715dd14d-5ded-4b10-8189-7c8865e1bafa</webElementGuid>
   </webElementProperties>
</WebElementEntity>
