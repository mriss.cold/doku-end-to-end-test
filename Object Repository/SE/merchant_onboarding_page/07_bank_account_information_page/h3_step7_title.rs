<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_step7_title</name>
   <tag></tag>
   <elementGuidId>dba27948-24b1-4dbf-94f6-8e06cedb5723</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h3[contains(text(), 'Informasi Rekening Bank')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/07_bank_account_information_page/iframe_bank_account_information</value>
      <webElementGuid>b40f60a1-5c21-46af-aa43-b65ca7f8fa57</webElementGuid>
   </webElementProperties>
</WebElementEntity>
