<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_account_number</name>
   <tag></tag>
   <elementGuidId>0befbda2-2966-414b-b682-514f966ad944</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/input[contains(@formcontrolname, 'bankAccountNumber')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/07_bank_account_information_page/iframe_bank_account_information</value>
      <webElementGuid>dc219232-1952-40c1-a04b-847d8f1b69be</webElementGuid>
   </webElementProperties>
</WebElementEntity>
