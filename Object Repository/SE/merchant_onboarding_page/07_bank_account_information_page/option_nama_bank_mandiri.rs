<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>option_nama_bank_mandiri</name>
   <tag></tag>
   <elementGuidId>39aa085c-5456-46c0-8847-0115383db3e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[contains(text(), 'Mandiri')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/07_bank_account_information_page/iframe_bank_account_information</value>
      <webElementGuid>6f4d8b4b-508e-4247-b87e-ca4e88560fd6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
