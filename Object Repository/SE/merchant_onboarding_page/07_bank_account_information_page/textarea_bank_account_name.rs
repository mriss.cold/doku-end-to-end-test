<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_bank_account_name</name>
   <tag></tag>
   <elementGuidId>e95acc0a-bca9-48df-8555-455ebb7a033c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/input[contains(@id, 'account-name')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/07_bank_account_information_page/iframe_bank_account_information</value>
      <webElementGuid>2bced5d0-0d65-4a71-b3df-8a37d325b23e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
