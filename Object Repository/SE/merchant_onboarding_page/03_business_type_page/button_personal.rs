<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_personal</name>
   <tag></tag>
   <elementGuidId>9d5caed7-33ab-4a8b-83f9-026faf526e74</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//strong[text() = 'Personal']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/03_business_type_page/iframe_activate_business</value>
      <webElementGuid>fa43e02f-36be-4c83-bbcd-f7e1a21f9b0e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
