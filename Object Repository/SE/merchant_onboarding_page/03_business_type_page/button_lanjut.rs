<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_lanjut</name>
   <tag></tag>
   <elementGuidId>d7f05b82-c965-4d77-942c-2707bf6d6e58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(), 'Lanjut')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/03_business_type_page/iframe_activate_business</value>
      <webElementGuid>f09f73e7-7ef5-469e-849d-ee5db591793f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
