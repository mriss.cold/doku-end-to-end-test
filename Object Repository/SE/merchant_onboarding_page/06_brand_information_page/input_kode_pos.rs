<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_kode_pos</name>
   <tag></tag>
   <elementGuidId>0dac00e7-dcbc-4969-82ce-cd8350ff7f76</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/div[contains(text(), 'Kode Pos')]/../div[@role = 'combobox']/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/06_brand_information_page/iframe_brand_information</value>
      <webElementGuid>badef2ce-0b50-4d06-b798-260255e4a026</webElementGuid>
   </webElementProperties>
</WebElementEntity>
