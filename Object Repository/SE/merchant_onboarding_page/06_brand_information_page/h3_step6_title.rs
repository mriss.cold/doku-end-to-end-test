<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_step6_title</name>
   <tag></tag>
   <elementGuidId>69077161-b6ef-4338-a337-7c37c6f821a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h3[contains(text(), 'Informasi Merek')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/06_brand_information_page/iframe_brand_information</value>
      <webElementGuid>81324fb0-aa85-4936-923d-c9b2c2df98a1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
