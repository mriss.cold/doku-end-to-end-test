<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_brand_selling</name>
   <tag></tag>
   <elementGuidId>031a8772-5f48-48fe-9766-8b86a23d89e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id = 'brand-selling']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/06_brand_information_page/iframe_brand_information</value>
      <webElementGuid>c1ee5086-71a8-46f8-8200-c581176b4a69</webElementGuid>
   </webElementProperties>
</WebElementEntity>
