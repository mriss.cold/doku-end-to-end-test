<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_dont_have_web</name>
   <tag></tag>
   <elementGuidId>b5547c83-7390-4e4b-9c28-43c5c3f2c893</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//app-input-radio-card[@value = 'DONT_HAVE']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/06_brand_information_page/iframe_brand_information</value>
      <webElementGuid>ed40f1f2-5199-4a30-8deb-ec08f5f4c5ab</webElementGuid>
   </webElementProperties>
</WebElementEntity>
