<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_pilih_media_whatsapp_on_brand_information_page</name>
   <tag></tag>
   <elementGuidId>1788bec8-b1a2-4e01-813e-49d6d3ba53c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(text(), 'Pilih Media')]/following-sibling::div[@role = 'combobox']//input)[5]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/06_brand_information_page/iframe_brand_information</value>
      <webElementGuid>b734c01c-2c23-466c-be37-abf9d7ebe151</webElementGuid>
   </webElementProperties>
</WebElementEntity>
