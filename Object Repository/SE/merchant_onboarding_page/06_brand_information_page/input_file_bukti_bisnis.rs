<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_file_bukti_bisnis</name>
   <tag></tag>
   <elementGuidId>611185b0-95b8-4d34-97a5-9ed25e50b522</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(), 'Lokasi Bisnis')]/parent::*/parent::*/following-sibling::*//input[@type='file']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/06_brand_information_page/iframe_brand_information</value>
      <webElementGuid>54055d20-d6a7-4f85-bbc5-a3e6b01c92c2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
