<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_memiliki_web_media_sosial_on_brand_information_page</name>
   <tag></tag>
   <elementGuidId>1fdd603c-5d70-4fd3-b17b-e8790292d518</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(), 'Saya memiliki web/media sosial untuk berjualan')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/06_brand_information_page/iframe_brand_information</value>
      <webElementGuid>e9a9e634-737b-40b4-8654-779ae3fa8619</webElementGuid>
   </webElementProperties>
</WebElementEntity>
