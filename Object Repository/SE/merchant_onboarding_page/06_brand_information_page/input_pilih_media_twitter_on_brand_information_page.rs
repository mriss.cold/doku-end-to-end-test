<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_pilih_media_twitter_on_brand_information_page</name>
   <tag></tag>
   <elementGuidId>246abef3-3b73-491d-8f0d-a31fc5fc1b2b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(text(), 'Pilih Media')]/following-sibling::div[@role = 'combobox']//input)[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/06_brand_information_page/iframe_brand_information</value>
      <webElementGuid>be88e5f2-00bf-441a-bcc0-8e673503cb65</webElementGuid>
   </webElementProperties>
</WebElementEntity>
