<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_pilih_media_facebook_on_brand_information_page</name>
   <tag></tag>
   <elementGuidId>34f38bb0-c09a-48a0-a418-96e5009cead1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(text(), 'Pilih Media')]/following-sibling::div[@role = 'combobox']//input)[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/06_brand_information_page/iframe_brand_information</value>
      <webElementGuid>2fc0ffc4-31d4-4bd2-aea9-7efa06df66cc</webElementGuid>
   </webElementProperties>
</WebElementEntity>
