<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_tambahkan_lagi_media_sosial_on_brand_information_page</name>
   <tag></tag>
   <elementGuidId>27a5fec6-2ef9-4ca3-9ed6-dbcceef36252</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(text(), 'Tambahkan Lagi')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/merchant_onboarding_page/06_brand_information_page/iframe_brand_information</value>
      <webElementGuid>1f550c01-799c-4d87-865b-8e3af87fafbe</webElementGuid>
   </webElementProperties>
</WebElementEntity>
