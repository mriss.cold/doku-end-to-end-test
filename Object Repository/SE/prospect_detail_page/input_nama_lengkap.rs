<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_nama_lengkap</name>
   <tag></tag>
   <elementGuidId>89197fe0-fcf2-48a3-95ec-e947cf1d02bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[./strong[text()=&quot;Nama Lengkap&quot;]]/following-sibling::input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>04cd6154-ff01-4095-bc41-049320b30404</webElementGuid>
   </webElementProperties>
</WebElementEntity>
