<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_email</name>
   <tag></tag>
   <elementGuidId>73b3132c-7f5b-4add-a7f3-ec008e4a8e25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div//p[.//i[contains(@class, 'envelope')]]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>e6a8f240-ed77-4977-af09-480cc113adb1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
