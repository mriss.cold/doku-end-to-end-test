<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_nama_brand</name>
   <tag></tag>
   <elementGuidId>abfae985-0201-4930-8ce2-4e6307561aa0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[./strong[text()=&quot;Nama Brand&quot;]]/following-sibling::input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>770772c6-8df6-4638-baad-39fbac302976</webElementGuid>
   </webElementProperties>
</WebElementEntity>
