<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_phone</name>
   <tag></tag>
   <elementGuidId>74d9f4fa-3e9b-4372-93cb-810d4ed1d18e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div//p[.//i[contains(@class, 'phone')]]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SE/prospect_list_page/iframe_prospect_list</value>
      <webElementGuid>d607e90d-9f14-44e8-ab56-e1f579f8d924</webElementGuid>
   </webElementProperties>
</WebElementEntity>
