<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_dashboard</name>
   <tag></tag>
   <elementGuidId>cd05bf2c-f2db-4e58-ab8e-995ba1fd5372</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//iframe[contains(@src, '/bo/empty')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>iframe</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>30d5420a-f5da-4d7b-bc91-9733d04dc5d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>http://app-sit.doku.com/bo/empty?uniq_id=1664439302706</value>
      <webElementGuid>9685231c-f366-471b-b3a9-9811e4198823</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dk_content&quot;)/div[@class=&quot;main-content d-flex flex-column-fluid&quot;]/div[@class=&quot;p-0 w-100 iframe_container&quot;]/span[@class=&quot;ng-star-inserted&quot;]/app-iframe[1]/div[@class=&quot;ng-star-inserted&quot;]/iframe[1]</value>
      <webElementGuid>15906b7b-16eb-48af-aa16-0f6e3907d275</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dk_content']/div/div/span/app-iframe/div/iframe</value>
      <webElementGuid>49689e3a-8c48-4cc8-943e-8f23ec7e79e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>e46c61df-c6a0-473b-a3bd-2f2aea6ae417</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@src = 'http://app-sit.doku.com/bo/empty?uniq_id=1664439302706']</value>
      <webElementGuid>df623d30-28ef-43b2-8c8d-4a69374c9f4c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
