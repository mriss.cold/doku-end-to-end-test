<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR015 - ChooseSimulatorCIMB</name>
   <tag></tag>
   <elementGuidId>bb64889f-2a65-40d3-ad32-34569d0ac5f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'simulate-cimb']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>simulate-cimb</value>
      <webElementGuid>dd55c59d-8d52-4ab1-b8e6-2c36ea5dc5a9</webElementGuid>
   </webElementProperties>
</WebElementEntity>
