<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR022 - ChooseVABankLainnya</name>
   <tag></tag>
   <elementGuidId>4e097a4b-99e9-4455-9d53-c3101c146769</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = ' Mandiri ' or . = ' Mandiri ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#cdk-accordion-child-0 > div > div > app-virtualaccount > ul:nth-child(5) > li > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Mandiri </value>
      <webElementGuid>bc911693-3d22-46d0-9c9b-98fa5cab4d50</webElementGuid>
   </webElementProperties>
</WebElementEntity>
