<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR012 - ClickButtonActionExportFile</name>
   <tag></tag>
   <elementGuidId>a432ff70-b6c8-453b-8bd3-f99b16ac4520</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#action-0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='dropdownBasic1</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'action-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>action-0</value>
      <webElementGuid>2069b217-431f-42b0-a2e4-15e7190506c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/NCPE/OR025 - iframe</value>
      <webElementGuid>7f6c5864-2252-4046-956e-2fc368634b92</webElementGuid>
   </webElementProperties>
</WebElementEntity>
