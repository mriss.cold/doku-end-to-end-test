<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR020 - ChooseVABSI</name>
   <tag></tag>
   <elementGuidId>8c42603a-cc36-4be7-b0ab-6d7fee45bd00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = ' Mandiri ' or . = ' Mandiri ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#cdk-accordion-child-0 > div > div > app-virtualaccount > ul:nth-child(7) > li > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Mandiri </value>
      <webElementGuid>43a8930f-02d8-488f-a559-026dcecf4b7c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
