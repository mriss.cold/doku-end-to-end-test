<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR013 - ExportPDF</name>
   <tag></tag>
   <elementGuidId>8ffcd148-10ee-4793-a59d-a93588fe8c58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#action-0 > div > div > button:nth-child(2)</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='action-0']/div/div/button[2</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/NCPE/OR025 - iframe</value>
      <webElementGuid>b629a791-37a4-40ab-b894-8d649117bb06</webElementGuid>
   </webElementProperties>
</WebElementEntity>
