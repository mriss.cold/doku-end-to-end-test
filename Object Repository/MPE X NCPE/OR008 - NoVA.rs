<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR008 - NoVA</name>
   <tag></tag>
   <elementGuidId>ba3958e7-4e08-4929-a0d9-b0b83efeac61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>body > interface-jokul-checkout > app-payment-code > div > div.ng-star-inserted > div > div.left-side > mat-card > ul > li:nth-child(4) > div > div.va-number.font-bold</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
