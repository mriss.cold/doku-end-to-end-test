<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR002 - ButtonFilterSearch</name>
   <tag></tag>
   <elementGuidId>fc2cb24c-4946-4876-957f-7f77dc90ca68</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'btnFilter']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnFilter</value>
      <webElementGuid>314ab223-bdb6-4551-b355-7a3081980bd5</webElementGuid>
   </webElementProperties>
</WebElementEntity>
