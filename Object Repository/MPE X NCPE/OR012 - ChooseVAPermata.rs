<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR012 - ChooseVAPermata</name>
   <tag></tag>
   <elementGuidId>cd78b7e7-ebee-4f92-810f-a3743dd22015</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = ' Permata Bank ' or . = ' Permata Bank ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Permata Bank </value>
      <webElementGuid>fa38fc8d-4876-4e25-ade5-5745688d4529</webElementGuid>
   </webElementProperties>
</WebElementEntity>
