<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR018 - ChooseVAMandiri</name>
   <tag></tag>
   <elementGuidId>242c543a-ff48-4817-9287-0fc21332ae4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = ' Mandiri ' or . = ' Mandiri ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#cdk-accordion-child-0 > div > div > app-virtualaccount > ul:nth-child(4) > li > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Mandiri </value>
      <webElementGuid>35f44177-4e3e-4946-8618-c1f43afee541</webElementGuid>
   </webElementProperties>
</WebElementEntity>
