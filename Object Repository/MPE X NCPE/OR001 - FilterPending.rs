<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR001 - FilterPending</name>
   <tag></tag>
   <elementGuidId>c7dd879e-dccf-42b1-8c0c-bf83ba63eb97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'pending']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>pending</value>
      <webElementGuid>f4a92e65-3d9f-4821-9491-6c77dbdb7012</webElementGuid>
   </webElementProperties>
</WebElementEntity>
