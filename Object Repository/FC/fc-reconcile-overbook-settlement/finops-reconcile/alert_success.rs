<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_success</name>
   <tag></tag>
   <elementGuidId>f5193b86-40e4-4801-97bd-cb75f117187e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//p[@class = 'notifier_notification-message' and (text() = 'Reconcile Successfully' or . = 'Reconcile Successfully') and @ref_element = 'Object Repository/iframeHandler/iframe-reconcile']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > app-finance > notifier-container > ul > li > notifier-notification > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>9d844c4a-dedd-4c15-8b07-ec8a952958e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notifier_notification-message</value>
      <webElementGuid>e727ae11-e59e-4aec-8cda-ccb40b558894</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Reconcile Successfully</value>
      <webElementGuid>0ec822c4-4b70-4b94-ae1c-6dcdba77a2be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FC/iframeHandler/iframe-reconcile</value>
      <webElementGuid>11470306-b482-4674-838a-e9f5a8b6be4e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
