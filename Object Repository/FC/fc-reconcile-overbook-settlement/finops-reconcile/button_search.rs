<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_search</name>
   <tag></tag>
   <elementGuidId>4b9dfe12-1666-4a6f-bbc9-4468b681217d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type  = 'submit' and @title = 'Search' and (text() = ' SEARCH ' or . = ' SEARCH ') and @ref_element = 'Object Repository/iframeHandler/iframe-reconcile']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>d3f0694e-799a-44f8-a7d5-c8861062da66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type </name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>ac951e16-11d4-4eeb-b41f-f22fc040a63c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Search</value>
      <webElementGuid>6356f0bc-da43-4a8e-8e3f-fd891b1ee848</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> SEARCH </value>
      <webElementGuid>bbe05747-4b18-43db-ad0d-422b74fc0ac2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FC/iframeHandler/iframe-reconcile</value>
      <webElementGuid>10023b5b-2232-49d4-91a5-7413c3b41e91</webElementGuid>
   </webElementProperties>
</WebElementEntity>
