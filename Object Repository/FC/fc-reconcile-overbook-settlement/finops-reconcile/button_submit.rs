<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_submit</name>
   <tag></tag>
   <elementGuidId>9d868855-3594-4a46-89bb-1cde4e9e6230</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[10]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type  = 'submit' and (text() = 'Submit' or . = 'Submit') and @ref_element = 'Object Repository/iframeHandler/iframe-reconcile']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#canReconcile > div.modal-dialog.modal-md.modal-dialog-centered > div.modal-content > form.ng-untouched.ng-pristine.ng-valid > div.modal-footer > button.btn.btn-primary</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>538978f7-9b85-42d0-a4b9-d858d8041884</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type </name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>cae92eab-5a48-4e47-ab0f-bc2efbc18c24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit</value>
      <webElementGuid>95f53de9-5e0a-4d54-8334-c9e5d6484865</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FC/iframeHandler/iframe-reconcile</value>
      <webElementGuid>5f631ab5-e16a-4f2c-9c5d-f45a9a38396f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Submit' or . = 'Submit')]</value>
      <webElementGuid>87de6529-a5e9-4110-be17-a3ed92c6c365</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
