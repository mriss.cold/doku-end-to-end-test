<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_paymentSuccess</name>
   <tag></tag>
   <elementGuidId>fea72ffd-df91-4e0c-8052-fe2b34ba5281</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[(text() = 'Payment Success' or . = 'Payment Success') and @ref_element = 'Object Repository/iframeHandler/iframe-reconcile']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>a894d46e-a7d1-44e7-95bc-cb5c7ad4a113</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Payment Success</value>
      <webElementGuid>ffa7b7c0-c226-4c65-8c3a-4896b214dc22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FC/iframeHandler/iframe-reconcile</value>
      <webElementGuid>67543233-d8a6-4438-bc98-58eb04f43633</webElementGuid>
   </webElementProperties>
</WebElementEntity>
