<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_overbook</name>
   <tag></tag>
   <elementGuidId>b2cf83b3-c7f8-42e9-8961-ce6db12e7994</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > app-finance > notifier-container > ul > li > notifier-notification > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FC/iframeHandler/iframe-reconcile</value>
      <webElementGuid>f53fad5f-037d-4d46-9a59-8cfab2295c01</webElementGuid>
   </webElementProperties>
</WebElementEntity>
