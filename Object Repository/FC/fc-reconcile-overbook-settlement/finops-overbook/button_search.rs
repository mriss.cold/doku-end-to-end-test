<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_search</name>
   <tag></tag>
   <elementGuidId>bc62314a-3d3f-4190-aab3-4b459c22f0d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type  = 'submit' and @title = 'Search' and (text() = ' SEARCH ' or . = ' SEARCH ') and @ref_element = 'Object Repository/iframeHandler/iframe-reconcile']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c8be675e-b771-4051-ba0c-c74e568ded44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type </name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>557eec2b-d236-4828-8873-846debd994ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Search</value>
      <webElementGuid>3589eb6b-43ea-44cd-adaf-00e4236d169e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> SEARCH </value>
      <webElementGuid>0d6ed77d-14be-4a5a-972a-e7fd4be25c80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FC/iframeHandler/iframe-reconcile</value>
      <webElementGuid>855d101f-c600-4e1c-960a-042fd98ee465</webElementGuid>
   </webElementProperties>
</WebElementEntity>
