<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_overbook</name>
   <tag></tag>
   <elementGuidId>93f9dae3-f27e-45ea-aa7a-91699ec573a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > app-finance > notifier-container > ul > li > notifier-notification > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FC/iframeHandler/iframe-reconcile</value>
      <webElementGuid>d3cfeef5-2947-45e6-8b9f-99649915c3c2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
