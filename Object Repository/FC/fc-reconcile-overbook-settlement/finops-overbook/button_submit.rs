<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_submit</name>
   <tag></tag>
   <elementGuidId>53f0acb8-29cf-4990-a8ad-96192f014d53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='submit'])[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type  = 'submit' and (text() = 'Submit' or . = 'Submit') and @ref_element = 'Object Repository/iframeHandler/iframe-reconcile']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#canReconcile > div.modal-dialog.modal-md.modal-dialog-centered > div.modal-content > form.ng-untouched.ng-pristine.ng-valid > div.modal-footer > button.btn.btn-primary</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>67c4a701-ae34-4d84-bb45-6584a77d6518</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type </name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>0f7867d4-81f8-4f08-a07f-c3ce0c37506a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit</value>
      <webElementGuid>154c6805-ee77-4b6f-9584-8f5a8db504a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FC/iframeHandler/iframe-reconcile</value>
      <webElementGuid>4ca5a598-ab5d-427a-8f6b-f951998ea6a4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
