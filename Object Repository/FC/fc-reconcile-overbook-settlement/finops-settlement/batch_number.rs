<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>batch_number</name>
   <tag></tag>
   <elementGuidId>19360b60-074f-43e1-9bff-894d160f0437</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//body/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/span[1]/app-iframe[1]/div[1]/iframe[1][count(. | //td[@ref_element = 'Object Repository/FC/iframeHandler/iframe-reconcile']) = count(//td[@ref_element = 'Object Repository/FC/iframeHandler/iframe-reconcile'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='tableOne']/tbody/tr[1]/td[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>f55678c3-62ad-43c2-8a69-8fc4f61210b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//body/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/span[1]/app-iframe[1]/div[1]/iframe[1]</value>
      <webElementGuid>3cce04dc-6e7f-4b27-9529-a93db9a33a13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FC/iframeHandler/iframe-reconcile</value>
      <webElementGuid>33903486-99ac-4b11-aeb3-118445074255</webElementGuid>
   </webElementProperties>
</WebElementEntity>
