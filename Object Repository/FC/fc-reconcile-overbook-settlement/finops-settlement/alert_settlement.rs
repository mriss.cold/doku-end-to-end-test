<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_settlement</name>
   <tag></tag>
   <elementGuidId>280763c4-b749-4f95-8c5e-ea383c931694</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > app-finance > notifier-container > ul > li > notifier-notification > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FC/iframeHandler/iframe-reconcile</value>
      <webElementGuid>99acd766-4a9e-4a16-80d3-76f7b8aef29b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
