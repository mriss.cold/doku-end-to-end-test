<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-SubMenuReportCreditCard</name>
   <tag></tag>
   <elementGuidId>864b5b1e-1807-47ba-98f0-6828ea2657c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@class='menu-link menu-toggle ng-tns-c2-36 ng-star-inserted']/span[@class='menu-text' and text()=' Credit Card ']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
