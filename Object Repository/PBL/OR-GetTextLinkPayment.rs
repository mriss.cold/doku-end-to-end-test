<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-GetTextLinkPayment</name>
   <tag></tag>
   <elementGuidId>0d7251f3-b784-41c1-bfad-bc85a3ad328e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@style='word-wrap: break-word;']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-Iframe</value>
      <webElementGuid>6d610765-579a-4eeb-b09c-7bc9a5bbc64b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
