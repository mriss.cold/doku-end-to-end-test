<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-MenuTransaction</name>
   <tag></tag>
   <elementGuidId>de2213e8-76b0-4cb4-a9a6-64405590238c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@ng-reflect-klass='menu-link menu-toggle ']/span[@class='menu-text' and contains(text(),'Transaction')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
