<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-GetUrlLinkPayment</name>
   <tag></tag>
   <elementGuidId>f62c2239-bae9-4ead-8406-5acfdcbfb59b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='url-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-Iframe</value>
      <webElementGuid>d7651127-9706-4ac9-bace-5f21a3b36a9f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
