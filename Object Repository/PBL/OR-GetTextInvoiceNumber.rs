<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-GetTextInvoiceNumber</name>
   <tag></tag>
   <elementGuidId>b26bd7c6-2ea9-461f-8b34-cdc1d47da453</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='invoiceNumber-0']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-Iframe</value>
      <webElementGuid>958ca0d1-a04d-4f8c-89c1-b1086d5cc585</webElementGuid>
   </webElementProperties>
</WebElementEntity>
