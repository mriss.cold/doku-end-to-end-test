<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-ButtonNext</name>
   <tag></tag>
   <elementGuidId>1cbfbe1d-747f-43b2-ab11-bca43bdbe9ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@type='submit' and @value='NEXT']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-IframeStepUp</value>
      <webElementGuid>3c8dbda1-0271-4421-9880-f365a00fa09f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
