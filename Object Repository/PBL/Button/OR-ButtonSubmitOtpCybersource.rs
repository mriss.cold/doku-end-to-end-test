<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-ButtonSubmitOtpCybersource</name>
   <tag></tag>
   <elementGuidId>9fe1048d-86e9-47b2-a087-f086aa1ad211</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@type='submit']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-IframeStepUp</value>
      <webElementGuid>caf28f92-e59d-439c-897b-eb62aac9e479</webElementGuid>
   </webElementProperties>
</WebElementEntity>
