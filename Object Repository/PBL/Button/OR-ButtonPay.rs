<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-ButtonPay</name>
   <tag></tag>
   <elementGuidId>6ec436fc-df07-4784-92c6-fa2df608be20</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(text(),'BAYAR')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-IframePayment</value>
      <webElementGuid>e8ae1092-829b-4dcf-9885-16853c88ebb4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
