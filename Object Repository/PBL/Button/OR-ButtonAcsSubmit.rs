<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-ButtonAcsSubmit</name>
   <tag></tag>
   <elementGuidId>b434df95-08fc-497d-a906-eb52cb50431a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tr//td[@align='right']//input[@id='acssubmit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-IframePayment</value>
      <webElementGuid>b6dd2c11-be28-4e20-b2c2-ecf465254980</webElementGuid>
   </webElementProperties>
</WebElementEntity>
