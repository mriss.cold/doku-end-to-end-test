<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-ButtonCreateInvoice</name>
   <tag></tag>
   <elementGuidId>b03a1761-028c-4567-8135-71bdb82f1c56</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonCreateLinkForm']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-Iframe</value>
      <webElementGuid>f47046ff-80dd-4f72-8216-e12d8e98270d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
