<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-ButtonDone</name>
   <tag></tag>
   <elementGuidId>3a9a0107-66ef-4a3b-babf-3548f69f532d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonDoneGeneratePaymentSuccessModal']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-Iframe</value>
      <webElementGuid>0e0c6920-c170-4684-af42-48cf51ba84c4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
