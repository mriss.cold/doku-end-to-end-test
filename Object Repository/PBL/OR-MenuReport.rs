<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-MenuReport</name>
   <tag></tag>
   <elementGuidId>4a9fbcf3-71d2-49af-ace9-5eb4f1a3c965</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a//span[@class='menu-text' and contains(text(),' Report  ')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
