<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-VerifyStatusTransactionReport</name>
   <tag></tag>
   <elementGuidId>f5368d52-219b-401b-b693-5970978b4350</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[contains(text(),'${status}')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-Iframe</value>
      <webElementGuid>6a686c1f-afce-4727-9429-67f1c13d0c06</webElementGuid>
   </webElementProperties>
</WebElementEntity>
