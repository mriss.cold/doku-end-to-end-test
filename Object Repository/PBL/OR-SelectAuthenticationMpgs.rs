<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-SelectAuthenticationMpgs</name>
   <tag></tag>
   <elementGuidId>25b2771a-aabd-4895-835e-37fd084e0437</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='selectAuthResult']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-IframePayment</value>
      <webElementGuid>57a4c250-d081-4572-9da9-da1e5d717e85</webElementGuid>
   </webElementProperties>
</WebElementEntity>
