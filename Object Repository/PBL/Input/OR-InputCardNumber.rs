<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-InputCardNumber</name>
   <tag></tag>
   <elementGuidId>6be868e6-2e25-4fc8-acb5-29f5f93d1fec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='card-number']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-IframePayment</value>
      <webElementGuid>1e249632-198c-4fbf-b7cb-169e3a5a4407</webElementGuid>
   </webElementProperties>
</WebElementEntity>
