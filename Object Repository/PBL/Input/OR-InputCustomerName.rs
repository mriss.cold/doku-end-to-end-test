<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-InputCustomerName</name>
   <tag></tag>
   <elementGuidId>cfb9d28f-0114-4b29-9ee7-d9b6862b8a23</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='customerName']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-Iframe</value>
      <webElementGuid>a1b126bd-ad95-4438-8109-c109e41852d1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
