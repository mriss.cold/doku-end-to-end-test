<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-InputExpirationMinutes</name>
   <tag></tag>
   <elementGuidId>c1089197-3ce0-4166-a0b2-7a1b4626b652</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ngb-timepicker[@id='expiryTime']//input[@placeholder='MM']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-Iframe</value>
      <webElementGuid>6b0e436a-a5b1-4a56-82fd-7904d6fc98ae</webElementGuid>
   </webElementProperties>
</WebElementEntity>
