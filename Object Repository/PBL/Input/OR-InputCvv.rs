<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-InputCvv</name>
   <tag></tag>
   <elementGuidId>92f6953d-2c17-4ff3-96af-d417772143ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='cvv']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-IframePayment</value>
      <webElementGuid>5da87674-f1a1-4bcf-944c-65c8a3b98218</webElementGuid>
   </webElementProperties>
</WebElementEntity>
