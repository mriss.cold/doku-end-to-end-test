<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR-InputExpirationHour</name>
   <tag></tag>
   <elementGuidId>06812beb-a36e-4c38-876f-a70a5f958369</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ngb-timepicker[@id='expiryTime']//input[@placeholder='HH']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/PBL/Iframe/OR-Iframe</value>
      <webElementGuid>563f8e0e-8967-429f-be79-1c682478472d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
