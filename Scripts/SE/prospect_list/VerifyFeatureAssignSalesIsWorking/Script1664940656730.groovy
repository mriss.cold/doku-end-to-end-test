import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.doku.katalon.util.MyUtils as MyUtils
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('SE/prospect_list/common/RegistrationMerchantUntilStep00Tc'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SE/common/LoginSalesManagerSuccessTc'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('SE/dashboard_page/button_menu_prospect'))

WebUI.click(findTestObject('SE/prospect_list_page/button_menu_prospect_list'))

WebUI.waitForElementVisible(findTestObject('SE/prospect_list_page/button_export'), GlobalVariable.defaultTimeoutInSecond)

WebUI.delay(1)

WebUI.click(findTestObject('SE/prospect_list_page/button_filter_prospect_list'))

String namaLengkap = MyUtils.get(MyUtils.KEY_NAMA_LENGKAP)

WebUI.setText(findTestObject('SE/prospect_list_page/input_nama_lengkap'), namaLengkap)

WebUI.click(findTestObject('SE/prospect_list_page/button_apply_filter_prospect_list'))

WebUI.verifyElementText(findTestObject('SE/prospect_list_page/td_nama_lengkap'), namaLengkap)

WebUI.click(findTestObject('SE/prospect_list_page/button_ellipsis'))

WebUI.click(findTestObject('SE/prospect_list_page/button_assign_sales'))

String assignedSales = 'sales@doku.com'

WebUI.sendKeys(findTestObject('SE/prospect_list_page/input_select_sales'), assignedSales)

WebUI.delay(1)

WebUI.sendKeys(findTestObject('SE/prospect_list_page/input_select_sales'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('SE/prospect_list_page/button_terapkan_pilihan'))

WebUI.click(findTestObject('SE/prospect_list_page/button_pop_up_ya_assign_sales'))

WebUI.click(findTestObject('SE/prospect_list_page/button_oke'))

WebUI.verifyElementText(findTestObject('SE/prospect_list_page/td_nama_sales'), assignedSales)

