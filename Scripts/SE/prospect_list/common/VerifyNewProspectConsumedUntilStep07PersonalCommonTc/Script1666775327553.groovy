import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.doku.katalon.util.MyUtils as MyUtils
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('SE/prospect_list/common/VerifyNewProspectConsumedUntilStep06PersonalCommonTc'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.verifyMatch(MyUtils.getSelectOptionValue(findTestObject('SE/prospect_detail_page/select_tipe_entitas'), findTestObject('SE/prospect_list_page/iframe_prospect_list')), "PERSEORANGAN", false)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/textarea_bukti_bisnis'), 'value', MyUtils.get(MyUtils.KEY_ABOUT_BRAND), 0)

WebUI.scrollToElement(findTestObject('SE/prospect_detail_page/input_kode_pos'), 0)

String actualKodePos = WebUI.getAttribute(findTestObject('SE/prospect_detail_page/input_kode_pos'), 'value')

String kodePosRegex = '.*' + MyUtils.get(MyUtils.KEY_KODE_POS) + '.*'

WebUI.verifyMatch(actualKodePos, kodePosRegex, true)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_alamat_lengkap'), 'value', MyUtils.get(MyUtils.KEY_ALAMAT_LENGKAP), 0)

WebUI.scrollToElement(findTestObject('SE/prospect_detail_page/input_link_website'), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_link_website'), 'value', MyUtils.get(MyUtils.KEY_WEBSITE), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_facebook'), 'value', MyUtils.get(MyUtils.KEY_FACEBOOK), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_instagram'), 'value', MyUtils.get(MyUtils.KEY_INSTAGRAM), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_twitter'), 'value', MyUtils.get(MyUtils.KEY_TWITTER), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_whatsapp'), 'value', MyUtils.get(MyUtils.KEY_WHATSAPP), 0)


