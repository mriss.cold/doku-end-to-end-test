import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('SE/prospect_list/common/RegistrationMerchantUntilStep00Tc'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.setText(findTestObject('SE/otp_verification_page/input_otp_0'), '0')

WebUI.setText(findTestObject('SE/otp_verification_page/input_otp_1'), '0')

WebUI.setText(findTestObject('SE/otp_verification_page/input_otp_2'), '0')

WebUI.setText(findTestObject('SE/otp_verification_page/input_otp_3'), '0')

WebUI.setText(findTestObject('SE/otp_verification_page/input_otp_4'), '0')

WebUI.setText(findTestObject('SE/otp_verification_page/input_otp_5'), '0')

WebUI.verifyElementText(findTestObject('SE/success_register_page/p_title_page'), 'Yay! Akun baru Anda telah berhasil didaftarkan.')
