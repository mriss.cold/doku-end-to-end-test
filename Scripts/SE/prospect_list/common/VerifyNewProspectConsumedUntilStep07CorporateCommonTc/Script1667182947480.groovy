import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.doku.katalon.util.MyUtils
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

MyUtils.put(MyUtils.KEY_BUSINESS_ID, WebUI.getText(findTestObject('SE/prospect_list_page/a_business_id')))

WebUI.click(findTestObject('SE/prospect_list_page/a_business_id'))

WebUI.verifyElementText(findTestObject('SE/prospect_detail_page/header_nama'), MyUtils.get(MyUtils.KEY_NAMA_LENGKAP))

WebUI.verifyElementText(findTestObject('SE/prospect_detail_page/strong_business_id'), MyUtils.get(MyUtils.KEY_BUSINESS_ID))

WebUI.verifyElementText(findTestObject('SE/prospect_detail_page/p_email'), MyUtils.get(MyUtils.KEY_EMAIL))

WebUI.verifyElementText(findTestObject('SE/prospect_detail_page/p_phone'), "+62" + MyUtils.get(MyUtils.KEY_PHONE_NUMBER))

WebUI.verifyMatch(MyUtils.getSelectOptionValue(findTestObject('SE/prospect_detail_page/select_tipe_bisnis'), findTestObject('SE/prospect_list_page/iframe_prospect_list')), MyUtils.get(MyUtils.KEY_BUSINESS_TYPE), false)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_nama_lengkap'), 'value', MyUtils.get(MyUtils.KEY_NAMA_LENGKAP_ORIGINAL), 0)

WebUI.verifyMatch(MyUtils.getSelectOptionValue(findTestObject('SE/prospect_detail_page/select_phone_calling_code_owner'), findTestObject('SE/prospect_list_page/iframe_prospect_list')), "62", false)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_phone_number_owner'), 'value', MyUtils.get(MyUtils.KEY_PHONE_NUMBER), 1000)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_email_owner'), 'value', MyUtils.get(MyUtils.KEY_EMAIL), 1000)

WebUI.verifyMatch(MyUtils.getSelectOptionValue(findTestObject('SE/prospect_detail_page/select_nationality'), findTestObject('SE/prospect_list_page/iframe_prospect_list')), "ID", false)

WebUI.scrollToElement(findTestObject('SE/prospect_detail_page/input_nama_perusahaan'), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_nama_perusahaan'), 'value', MyUtils.get(MyUtils.KEY_NAMA_LENGKAP), 1000)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_nama_brand'), 'value', MyUtils.get(MyUtils.KEY_NAMA_MEREK), 1000)

WebUI.verifyMatch(MyUtils.getSelectOptionValue(findTestObject('SE/prospect_detail_page/select_kategori_bisnis'), findTestObject('SE/prospect_list_page/iframe_prospect_list')), "CAPITAL_MARKET", false)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/textarea_bukti_bisnis'), 'value', MyUtils.get(MyUtils.KEY_ABOUT_BRAND), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_email_bisnis'), 'value', MyUtils.get(MyUtils.KEY_EMAIL), 1000)

WebUI.verifyMatch(MyUtils.getSelectOptionValue(findTestObject('SE/prospect_detail_page/select_phone_calling_code_bisnis'), findTestObject('SE/prospect_list_page/iframe_prospect_list')), "62", false)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_phone_number_bisnis'), 'value', MyUtils.get(MyUtils.KEY_PHONE_NUMBER), 1000)

WebUI.verifyMatch(MyUtils.getSelectOptionValue(findTestObject('SE/prospect_detail_page/select_tipe_entitas'), findTestObject('SE/prospect_list_page/iframe_prospect_list')), "UD", false)

WebUI.scrollToElement(findTestObject('SE/prospect_detail_page/input_kode_pos'), 0)

String actualKodePos = WebUI.getAttribute(findTestObject('SE/prospect_detail_page/input_kode_pos'), 'value')

String kodePosRegex = '.*' + MyUtils.get(MyUtils.KEY_KODE_POS) + '.*'

WebUI.verifyMatch(actualKodePos, kodePosRegex, true)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_alamat_lengkap'), 'value', MyUtils.get(MyUtils.KEY_ALAMAT_LENGKAP), 0)

WebUI.scrollToElement(findTestObject('SE/prospect_detail_page/input_link_website'), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_link_website'), 'value', MyUtils.get(MyUtils.KEY_WEBSITE), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_facebook'), 'value', MyUtils.get(MyUtils.KEY_FACEBOOK), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_instagram'), 'value', MyUtils.get(MyUtils.KEY_INSTAGRAM), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_twitter'), 'value', MyUtils.get(MyUtils.KEY_TWITTER), 0)

WebUI.verifyElementAttributeValue(findTestObject('SE/prospect_detail_page/input_whatsapp'), 'value', MyUtils.get(MyUtils.KEY_WHATSAPP), 0)
