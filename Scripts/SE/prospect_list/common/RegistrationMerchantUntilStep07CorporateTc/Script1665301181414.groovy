import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.awt.RenderingHints.Key as Key
import com.doku.katalon.util.MyUtils as MyUtils
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String uniqueId = MyUtils.getUniqueId()

WebUI.callTestCase(findTestCase('SE/prospect_list/common/RegistrationMerchantUntilStep05CorporateTc'), [:], FailureHandling.STOP_ON_FAILURE)

String badanHukum = 'UD (Usaha Dagang)'

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_pilih_badan_hukum_bisnis'), badanHukum + Keys.chord(Keys.ENTER))

String namaPerusahaan = ('Testing Katalon' + uniqueId) + ' UD'

MyUtils.put(MyUtils.KEY_NAMA_PERUSAHAAN, namaPerusahaan)

MyUtils.put(MyUtils.KEY_NAMA_LENGKAP, namaPerusahaan)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_nama_perusahaan'), namaPerusahaan)

String emailPerusahaan = ('testingkatalon' + uniqueId) + '.ud@doku.com'

MyUtils.put(MyUtils.KEY_EMAIL_PERUSAHAAN, emailPerusahaan)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_email_perusahaan'), emailPerusahaan)

String numberPhonePerusahaan = MyUtils.get(MyUtils.KEY_PHONE_NUMBER) + '1'

MyUtils.put(MyUtils.KEY_PHONE_NUMBER_PERUSAHAAN, numberPhonePerusahaan)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_phone_number'), numberPhonePerusahaan)

WebUI.scrollToElement(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_kode_pos_cooporate'), 0)

String kodePosCorporate = '12920'
MyUtils.put(MyUtils.KEY_KODE_POS, kodePosCorporate)

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_kode_pos_cooporate'), kodePosCorporate + Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('SE/merchant_onboarding_page/05_business_information_page/option_kode_pos_perusahaan'))

String alamatLengkap = 'Alamat Testing Katalon ' + MyUtils.getUniqueId()

MyUtils.put(MyUtils.KEY_ALAMAT_LENGKAP, alamatLengkap)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/05_business_information_page/textarea_address_company'), alamatLengkap)

WebUI.click(findTestObject('SE/merchant_onboarding_page/05_business_information_page/button_lanjut_business_information'))

WebUI.verifyElementPresent(findTestObject('SE/merchant_onboarding_page/05_business_information_page/h3_step6_corporate'), 0)

WebUI.uploadFile(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_file_brand_logo_corporate'), MyUtils.getDataFilesAbsolutePath(
        '/prospect/images/brand-logo-corporate.jpg'))

String kategoriBisnis = 'Pasar Modal'

MyUtils.put(MyUtils.KEY_KATEGORI_BISNIS, kategoriBisnis)

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_kategori_bisnis'), kategoriBisnis + Keys.chord(Keys.ENTER))

String bisnisSayaCorporate = 'Bisnis saya adalah testing katalon ' + uniqueId

MyUtils.put(MyUtils.KEY_ABOUT_BRAND, bisnisSayaCorporate)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/05_business_information_page/textarea_bisnis_saya_adalah_corporate'), bisnisSayaCorporate)

WebUI.click(findTestObject('SE/merchant_onboarding_page/05_business_information_page/button_total_jumlah_transaksi_perbulan'))

WebUI.click(findTestObject('SE/merchant_onboarding_page/05_business_information_page/button_total_transaksi_perbulan'))

WebUI.uploadFile(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_file_bukti_bisnis_corporate'), MyUtils.getDataFilesAbsolutePath(
        '/prospect/images/bukti-bisnis-corporate.jpg'))

WebUI.click(findTestObject('SE/merchant_onboarding_page/05_business_information_page/button_memiliki_web_media_sosial'))

//input website

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_pilih_media_website'), 'Web' + Keys.chord(Keys.ENTER))

String mediaSosialWebsite = 'https://www.doku.com/testkatalon' + uniqueId

MyUtils.put(MyUtils.KEY_WEBSITE, mediaSosialWebsite)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_media_website'), mediaSosialWebsite)

WebUI.click(findTestObject('SE/merchant_onboarding_page/05_business_information_page/button_tambahkan_lagi_media_sosial'))

//input facebook

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_pilih_media_facebook'), 'Facebook' + Keys.chord(Keys.ENTER))

String mediaSosialFaceboook = 'https://www.facebook.com/testkatalon' + uniqueId

MyUtils.put(MyUtils.KEY_FACEBOOK, mediaSosialFaceboook)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_media_facebook'), mediaSosialFaceboook)

WebUI.click(findTestObject('SE/merchant_onboarding_page/05_business_information_page/button_tambahkan_lagi_media_sosial'))

//input twitter

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_pilih_media_twitter'), 'Twitter' + Keys.chord(Keys.ENTER))

String mediaSosialTwitter = 'https://twitter.com/testkatalon' + uniqueId

MyUtils.put(MyUtils.KEY_TWITTER, mediaSosialTwitter)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_media_twitter'), mediaSosialTwitter)

WebUI.click(findTestObject('SE/merchant_onboarding_page/05_business_information_page/button_tambahkan_lagi_media_sosial'))

// input instagram

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_pilih_media_instagram'), 'Instagram' + Keys.chord(Keys.ENTER))

String mediaSosialInstagram = 'https://www.instagram.com/testkatalon' + uniqueId

MyUtils.put(MyUtils.KEY_INSTAGRAM, mediaSosialInstagram)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_media_instagram'), mediaSosialInstagram)

WebUI.click(findTestObject('SE/merchant_onboarding_page/05_business_information_page/button_tambahkan_lagi_media_sosial'))

// input whatsapp

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_pilih_media_whatsapp'), 'Whatsapp' + Keys.chord(Keys.ENTER))

String mediaSosialWhatsapp = 'https://wa.me/62' + MyUtils.get(MyUtils.KEY_PHONE_NUMBER)

MyUtils.put(MyUtils.KEY_WHATSAPP, mediaSosialWhatsapp)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/05_business_information_page/input_media_whatsapp'), mediaSosialWhatsapp)

// input media sosial

MyUtils.put(MyUtils.KEY_NAMA_LENGKAP, MyUtils.get(MyUtils.KEY_NAMA_MEREK))

WebUI.click(findTestObject('SE/merchant_onboarding_page/05_business_information_page/button_lanjut_business_information'))

WebUI.verifyElementPresent(findTestObject('SE/merchant_onboarding_page/07_bank_account_information_page/h3_step7_title'), 0)

