import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.doku.katalon.util.MyUtils as MyUtils
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


String uniqueId = MyUtils.getUniqueId()

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.registerUrl)

WebUI.click(findTestObject('Object Repository/SE/register_page/Page_Registration  DOKU Business/button_Accept Cookies'), FailureHandling.OPTIONAL)

String namaLengkap = 'Test Katalon' + uniqueId

MyUtils.put(MyUtils.KEY_NAMA_LENGKAP, namaLengkap)

MyUtils.put(MyUtils.KEY_NAMA_LENGKAP_ORIGINAL, namaLengkap)

WebUI.setText(findTestObject('Object Repository/SE/register_page/Page_Registration  DOKU Business/input_Nama Lengkap_userFullName'), 
    namaLengkap)

String namaMerek = ('Test Katalon' + uniqueId) + ' Merk'

MyUtils.put(MyUtils.KEY_NAMA_MEREK, namaMerek)

WebUI.setText(findTestObject('Object Repository/SE/register_page/Page_Registration  DOKU Business/input_Nama Merk_userMerk'), 
    namaMerek)

String email = ('test.katalon+' + uniqueId) + '@doku.com'

MyUtils.put(MyUtils.KEY_EMAIL, email)

WebUI.setText(findTestObject('Object Repository/SE/register_page/Page_Registration  DOKU Business/input_Email_userEmail'), 
    email)

String phoneNumber = MyUtils.generatePhoneNumber()

MyUtils.put(MyUtils.KEY_PHONE_NUMBER, phoneNumber)

WebUI.setText(findTestObject('Object Repository/SE/register_page/Page_Registration  DOKU Business/input_ID_userNumber'), phoneNumber)

WebUI.setText(findTestObject('Object Repository/SE/register_page/Page_Registration  DOKU Business/input_Kata Sandi_newPassword'), 
    GlobalVariable.defaultPassword)

WebUI.setText(findTestObject('Object Repository/SE/register_page/Page_Registration  DOKU Business/input_Konfirmasi Kata Sandi_rePassword'), 
    GlobalVariable.defaultPassword)

WebUI.click(findTestObject('Object Repository/SE/register_page/Page_Registration  DOKU Business/input_Kode Referensi (Opsional)_defaultCheck1'))

// reCAPTCHA on SIT env can be skipped by not check the checkbox.
//WebUI.click(findTestObject('Object Repository/SE/register_page/Page_Registration  DOKU Business/checkbox_reCAPTCHA'))
WebUI.click(findTestObject('Object Repository/SE/register_page/Page_Registration  DOKU Business/button_DAFTAR'))

