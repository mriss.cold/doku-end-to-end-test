import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.doku.katalon.util.MyUtils as MyUtils
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String uniqueId = MyUtils.getUniqueId()

WebUI.callTestCase(findTestCase('SE/prospect_list/common/RegistrationMerchantUntilStep06PersonalTc'), [:], FailureHandling.STOP_ON_FAILURE)

MyUtils.put(MyUtils.KEY_NAMA_LENGKAP, MyUtils.get(MyUtils.KEY_NAMA_MEREK))

WebUI.uploadFile(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_file_brand_logo'), MyUtils.getDataFilesAbsolutePath('/prospect/images/brand-logo.jpg'))

WebUI.click(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/button_retail'))

String aboutBrand = 'Bisnis Saya adalah ' + MyUtils.getUniqueId() + '.';
MyUtils.put(MyUtils.KEY_ABOUT_BRAND, aboutBrand)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/textarea_about_brand'), aboutBrand)

//WebUI.setText(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/textarea_brand_selling'), 'Saya menjual ' + MyUtils.getUniqueId())//

WebUI.scrollToElement(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_kode_pos'), 0)

String kodePos = '12920';
MyUtils.put(MyUtils.KEY_KODE_POS, kodePos)

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_kode_pos'), kodePos)

WebUI.click(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/option_kode_pos'))

String alamatLengkap = 'Setiabudi ' + MyUtils.getUniqueId()

MyUtils.put(MyUtils.KEY_ALAMAT_LENGKAP, alamatLengkap)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/textarea_address'), alamatLengkap)

WebUI.uploadFile(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_file_bukti_bisnis'), MyUtils.getDataFilesAbsolutePath('/prospect/images/bukti-bisnis.jpg'))

// WebUI.click(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/button_dont_have_web'))

WebUI.click(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/button_memiliki_web_media_sosial_on_brand_information_page'))

//input website

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_pilih_media_website_on_brand_information_page'), 'Web' + Keys.chord(Keys.ENTER))

String mediaSosialWebsite = 'https://www.doku.com/testkatalon' + uniqueId

WebUI.setText(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_media_website_on_brand_information_page'), mediaSosialWebsite)

WebUI.click(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/button_tambahkan_lagi_media_sosial_on_brand_information_page'))

MyUtils.put(MyUtils.KEY_WEBSITE, mediaSosialWebsite)

//input facebook

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_pilih_media_facebook_on_brand_information_page'), 'Facebook' + Keys.chord(Keys.ENTER))

String mediaSosialFaceboook = 'https://www.facebook.com/testkatalon' + uniqueId

WebUI.setText(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_media_facebook_on_brand_information_page'), mediaSosialFaceboook)

WebUI.click(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/button_tambahkan_lagi_media_sosial_on_brand_information_page'))

MyUtils.put(MyUtils.KEY_FACEBOOK, mediaSosialFaceboook)

//input twitter

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_pilih_media_twitter_on_brand_information_page'), 'Twitter' + Keys.chord(Keys.ENTER))

String mediaSosialTwitter = 'https://twitter.com/testkatalon' + uniqueId

WebUI.setText(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_media_twitter_on_brand_information_page'), mediaSosialTwitter)

WebUI.click(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/button_tambahkan_lagi_media_sosial_on_brand_information_page'))

MyUtils.put(MyUtils.KEY_TWITTER, mediaSosialTwitter)

// input instagram

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_pilih_media_instagram_on_brand_information_page'), 'Instagram' + Keys.chord(Keys.ENTER))

String mediaSosialInstagram = 'https://www.instagram.com/testkatalon' + uniqueId

WebUI.setText(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_media_instagram_on_brand_information_page'), mediaSosialInstagram)

WebUI.click(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/button_tambahkan_lagi_media_sosial_on_brand_information_page'))

MyUtils.put(MyUtils.KEY_INSTAGRAM, mediaSosialInstagram)

// input whatsapp

WebUI.sendKeys(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_pilih_media_whatsapp_on_brand_information_page'), 'Whatsapp' + Keys.chord(Keys.ENTER))

String mediaSosialWhatsapp = 'https://wa.me/62' + MyUtils.get(MyUtils.KEY_PHONE_NUMBER)

WebUI.setText(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/input_media_whatsapp_on_brand_information_page'), mediaSosialWhatsapp)

MyUtils.put(MyUtils.KEY_WHATSAPP, mediaSosialWhatsapp)

WebUI.click(findTestObject('SE/merchant_onboarding_page/06_brand_information_page/button_lanjut_on_brand_information_page'))

WebUI.verifyElementPresent(findTestObject('SE/merchant_onboarding_page/07_bank_account_information_page/h3_step7_title'), 0)
