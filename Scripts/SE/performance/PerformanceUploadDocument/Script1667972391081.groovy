import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.doku.katalon.util.MyUtils as MyUtils
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


	WebUI.callTestCase(findTestCase('SE/prospect_list/common/RegistrationMerchantUntilStep08CorporateTc'), [:], FailureHandling.STOP_ON_FAILURE)
	
	WebUI.uploadFile(findTestObject('SE/merchant_onboarding_page/08_upload_document/input_file_npwp_badan'), MyUtils.getDataFilesAbsolutePath('/prospect/images/npwp-badan.jpg'))
	
	String noNpwp = MyUtils.generateNoNpwp()
	
	MyUtils.put(MyUtils.KEY_NO_NPWP, noNpwp)
	
	WebUI.setText(findTestObject('SE/merchant_onboarding_page/08_upload_document/input_no_npwp'), noNpwp)
	
	WebUI.uploadFile(findTestObject('SE/merchant_onboarding_page/08_upload_document/input_file_surat_izin_usaha'), MyUtils.getDataFilesAbsolutePath(
			'/prospect/images/upload-image3-8mb.jpg'))
	
	WebUI.uploadFile(findTestObject('SE/merchant_onboarding_page/08_upload_document/input_file_nomor_induk_berusaha'), MyUtils.getDataFilesAbsolutePath(
			'/prospect/images/upload-image3-8mb.jpg'))
	
	String noNib = MyUtils.generateNoNib()
	
	MyUtils.put(MyUtils.KEY_NO_NIB, noNib)
	
	WebUI.setText(findTestObject('SE/merchant_onboarding_page/08_upload_document/input_no_nib'), noNib)
	
	WebUI.uploadFile(findTestObject('SE/merchant_onboarding_page/08_upload_document/input_file_dokumen_tambahan'), MyUtils.getDataFilesAbsolutePath(
			'/prospect/images/upload-image3-8mb.jpg'))
	
	WebUI.click(findTestObject('SE/merchant_onboarding_page/08_upload_document/button_lanjut_upload_document'))
	
	WebUI.verifyElementPresent(findTestObject('SE/merchant_onboarding_page/09_term_and_condition/h3_step9_title'), 0)
	
	WebUI.click(findTestObject('SE/merchant_onboarding_page/09_term_and_condition/checkbox_term_and_condition'))
	
	WebUI.click(findTestObject('SE/merchant_onboarding_page/09_term_and_condition/button_lanjut_term_and_condition'))
	
	WebUI.verifyElementPresent(findTestObject('SE/merchant_onboarding_page/10_complete/h3_bisnis_anda_diaktifkan_title'), 0)

