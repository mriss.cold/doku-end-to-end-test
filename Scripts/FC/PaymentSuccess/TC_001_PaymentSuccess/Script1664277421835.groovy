import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//def businessService = new businessService(
//  id : date,
//  name: number)

def now = new Date()
String invoiceNumber = now.format('yyyyMMddhhmmss')

String transactionDate = now.format('yyyy-MM-dd')

String transactionTime = now.format('hh:mm:ss')

GlobalVariable.G_number = invoiceNumber

String transactionDateTime = transactionDate+"T"+transactionTime+"Z"

System.out.println('"' + transactionDateTime + '"');

def request = WS.sendRequestAndVerify(findTestObject('Object Repository/FC/kafka-publish-controller/create-payment-success', 
    [('number') : GlobalVariable.G_number, ('va_date') : GlobalVariable.G_number, ('transaction_date') : '"' + transactionDateTime + '"', ('service_id') : service_id, ('service_name') : service_name, 
        ('client_id') : client_id, ('client_name') : client_name, ('client_sharedKey') : client_sharedKey, ('client_status') : client_status, ('partner_id') : partner_id, 
        ('partner_name') : partner_name, ('acquirer_id') : acquirer_id, ('acquirer_name') : acquirer_name, ('channel_id') : channel_id, ('channel_name') : channel_name, 
        ('order_amount') : order_amount, ('transaction_status') : transaction_status, ('originalReqId') : originalReqId, ('costumer_name') : costumer_name, 
        ('costumer_email') : costumer_email, ('vaConfig_method') : vaConfig_method, ('vaConfig_feature') : vaConfig_feature, ('Identifier_name') : Identifier_name, 
        ('Identifier_name') : Identifier_name, ('Identifier_name1') : Identifier_name1, ('Identifier_name2') : Identifier_name2, ('Identifier_value') : Identifier_value, 
        ('Identifier_value1') : Identifier_value1, ('Identifier_value2') : Identifier_value2, ('va_created') : va_created, ('va_expired') : va_expired, 
        ('va_billingType') : va_billingType, ('vaPayment_status') : vaPayment_status, ('vaPayment_name') : vaPayment_name, ('vaPayment_value') : vaPayment_value, 
        ('jdm_regisuuid') : jdm_regisuuid, ('jdminquiry_uuid') : jdminquiry_uuid, ('jdm_divisionName') : jdm_divisionName, ('jdm_department') : jdm_department, 
        ('jdm_database') : jdm_database, ('jdm_clientCreateTime') : jdm_clientCreateTime, ('jdm_journeyid') : jdm_journeyid]))

//System.out.println(GlobalVariable.G_number)
WebUI.openBrowser(GlobalVariable.url_login_uat)

WebUI.maximizeWindow()

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_email'), 1)

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_password'), 1)

WebUI.verifyElementClickable(findTestObject('FC/fc-reconcile-overbook-settlement/login/button_login'))

WebUI.setText(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_email'), email)

WebUI.setMaskedText(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_password'), password)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/login/button_login'))

//Check Payment Success

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/merchant-paymentSuccess/span_report'))

WebUI.delay(2)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/merchant-paymentSuccess/span_transaction'))

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/merchant-paymentSuccess/input_invoice'), 1)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/merchant-paymentSuccess/button_clear'))

WebUI.setText(findTestObject('FC/fc-reconcile-overbook-settlement/merchant-paymentSuccess/input_invoice'), GlobalVariable.G_number)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/button_search'))

WebUI.delay(3)

if (scenario == 'found') {
    'Available data'
    
WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/merchant-paymentSuccess/text_paymentSuccess'), 1)

//  WebUI.takeScreenshot()
}

WebUI.closeBrowser()

