import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url_login_uat)

WebUI.maximizeWindow()

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/login/button_cookies'))

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_email'), 1)

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_password'), 1)

WebUI.verifyElementClickable(findTestObject('FC/fc-reconcile-overbook-settlement/login/button_login'))

WebUI.setText(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_email'), email)

WebUI.setMaskedText(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_password'), password)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/login/button_login'))

//Reconcile

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/span_finance'))

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/span_transaction'))

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/button_clear'))

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/input_invoice'), 1)

//WebUI.setText(findTestObject('FC/finops-reconcile/input_invoice'), invoiceNumber)

WebUI.setText(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/input_invoice'), GlobalVariable.G_number)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/button_search'))

WebUI.delay(5)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/button_toggle'))

WebUI.delay(3)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/span_reconcile'))

WebUI.verifyElementClickable(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/button_submit'))

WebUI.delay(3)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/button_submit'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/finops-reconcile/alert_success'), 1)

WebUI.takeScreenshot()

WebUI.delay(3)

//Overbook

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-overbook/span_monitoring'))

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-overbook/span_overbook'))

WebUI.delay(8)

WebUI.verifyElementClickable(findTestObject('FC/fc-reconcile-overbook-settlement/finops-overbook/option_accountNo'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('FC/fc-reconcile-overbook-settlement/finops-overbook/option_accountNo'), number, false)

WebUI.delay(2)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-overbook/button_search'))

WebUI.delay(8)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-overbook/button_overbook'))

WebUI.verifyElementClickable(findTestObject('FC/fc-reconcile-overbook-settlement/finops-overbook/button_submit'))

WebUI.delay(2)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-overbook/button_submit'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/finops-overbook/alert_overbook'), 1)

WebUI.delay(5)

WebUI.closeBrowser()