import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static org.assertj.core.api.Assertions.*
import org.junit.After
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import groovy.json.JsonSlurper as JsonSlurper

WebUI.openBrowser(GlobalVariable.url_login_uat)

WebUI.maximizeWindow()


WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/login/button_cookies'))

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_email'), 1)

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_password'), 1)

WebUI.verifyElementClickable(findTestObject('FC/fc-reconcile-overbook-settlement/login/button_login'))

WebUI.setText(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_email'), email)

WebUI.setMaskedText(findTestObject('FC/fc-reconcile-overbook-settlement/login/input_password'), password)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/login/button_login'))

//Settlement

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/span_monitoring'))

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/span_settlement'))

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/button_clear'))

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/button_more'))

WebUI.selectOptionByIndex(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/option_status'), 2)

WebUI.setText(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/input_business'), business)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/button_search'))

batch_number = WebUI.getText(findTestObject('Object Repository/FC/fc-reconcile-overbook-settlement/finops-settlement/batch_number'))

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/button_toggle'))

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/button_manualTransfer'))

WebUI.setText(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/textarea_notes'), notes)

WebUI.delay(2)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/button_submit'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/alert_settlement'), 10)

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/alert_settlement'), 1)

WebUI.delay(5)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/button_clear'))

WebUI.delay(2)

WebUI.selectOptionByIndex(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/option_status'), 1)

WebUI.delay(2)

WebUI.setText(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/input_business'), business)

WebUI.delay(2)

WebUI.setText(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/input_batch'), batch_number)

WebUI.click(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/button_search'))

WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('FC/fc-reconcile-overbook-settlement/finops-settlement/span_status'), 1)

WebUI.takeScreenshot()

WebUI.delay(5)

WebUI.closeBrowser()