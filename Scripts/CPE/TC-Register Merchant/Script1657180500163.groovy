import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.openqa.selenium.WebDriver as WebDriver

WebUI.openBrowser(GlobalVariable.url_register)

WebUI.delay(3)

WebUI.comment(note)

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginPage/OR-BtnAccept'))

WebUI.setText(findTestObject('RegisterPage/OR-FormName'), name)

WebUI.setText(findTestObject('RegisterPage/OR-FormBrandName'), brandName)

WebUI.setText(findTestObject('RegisterPage/OR-FormEmail'), email)

WebUI.setText(findTestObject('RegisterPage/OR-FormPhoneNumber'), phonenumber)

WebUI.setText(findTestObject('RegisterPage/OR-FormPassword'), password)

WebUI.setText(findTestObject('RegisterPage/OR-FormRePassword'), repassword)

WebUI.click(findTestObject('RegisterPage/OR-CheckBoxAgree'))

WebUI.delay(3)

WebUI.delay(3)

WebUI.click(findTestObject('RegisterPage/OR-BtnRegister'))

if (status == 'Failed') {
    WebUI.delay(3)

    WebUI.verifyTextPresent(expectedResult, false)

    WebUI.takeScreenshot()

    return null
} else {
    WebUI.delay(3)

    WebUI.takeScreenshot()

    WebUI.navigateToUrl(GlobalVariable.url_mail)

    WebUI.delay(3)

    WebUI.click(findTestObject('EmailSend/MailCatcherPage/OR-TxtMailChatcher'))

    mailchatcher = (('<' + email) + '>')

    WebUI.comment(mailchatcher)

    WebUI.verifyElementText(findTestObject('EmailSend/MailCatcherPage/OR-TxtMailChatcher'), mailchatcher)

    WebUI.click(findTestObject('EmailSend/MailCatcherPage/OR-TxtMailChatcher'))

    WebUI.delay(3)

    WebUI.scrollToElement(findTestObject('EmailSend/MailCatcherPage/OR-BtnCreatePassword'), 5)

    WebUI.click(findTestObject('EmailSend/MailCatcherPage/OR-BtnCreatePassword'))

    WebUI.switchToWindowIndex('1')

    WebUI.delay(3)

    WebUI.takeScreenshot()

    WebUI.delay(3)

    WebUI.setText(findTestObject('LoginPage/OR-FormEmail'), email)

    WebUI.setText(findTestObject('LoginPage/OR-FormPassword'), password)

    WebUI.click(findTestObject('LoginPage/OR-LoginSIT'))

    WebUI.delay(3)

    WebUI.takeScreenshot()

    WebUI.navigateToUrl('http://app-sit.doku.com/bo/dashboard')

    WebUI.takeScreenshot()

    WebUI.delay(3)

    WebUI.closeBrowser()
}

