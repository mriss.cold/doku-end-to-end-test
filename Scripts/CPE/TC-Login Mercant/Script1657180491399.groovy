import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser(GlobalVariable.url_login)

WebUI.maximizeWindow()

//WebUI.click(findTestObject('CPE/LoginPage/OR-BtnAccept'))

//WebUI.comment(note)

WebUI.setText(findTestObject('CPE/LoginPage/OR-FormEmail'), email)

WebUI.setText(findTestObject('CPE/LoginPage/OR-FormPassword'), password)

WebUI.delay(5)

WebUI.click(findTestObject('CPE/LoginPage/OR-LoginSIT'))

WebUI.delay(5)

WebUI.takeScreenshot()

if (status == 'Success') {
    WebUI.delay(5)

    WebUI.takeScreenshot()

//    WebUI.click(findTestObject('CPE/UserProfilePage/OR-BtnProfileDropdown'))
//
//    WebUI.click(findTestObject('CPE/LoginPage/OR-BtnSignOut'))

    WebUI.delay(5)

   // WebUI.closeBrowser()
} else {
    WebUI.delay(5)

    if (notification == 'field') {
        WebUI.delay(5)

        WebUI.verifyTextPresent(expectedResult, false)

        WebUI.delay(5)

        WebUI.takeScreenshot()
    } else {
        WebUI.delay(5)

        WebUI.verifyTextPresent(expectedResult, false)

        WebUI.delay(5)

        WebUI.takeScreenshot()

        WebUI.click(findTestObject('CPE/LoginPage/OR-BtnOk'))
    }
    
    WebUI.delay(5)

    WebUI.closeBrowser()
}

