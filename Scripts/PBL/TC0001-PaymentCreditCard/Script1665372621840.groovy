import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import utils.Utils as Utils

WebUI.callTestCase(findTestCase('CPE/TC-Login Mercant'), [('note') : 'Login', ('email') : email, ('password') : password
		, ('status') : 'Success'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/PBL/OR-MenuAcceptPayment'))

WebUI.click(findTestObject('Object Repository/PBL/OR-SubMenuPaymentLink'))

WebUI.scrollToElement(findTestObject('Object Repository/PBL/Button/OR-ButtonCreatePaymentLink'), 2)

WebUI.delay(3)

WebUI.takeFullPageScreenshot(FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonCreatePaymentLink'))

WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonSinglePayment'))

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputCustomerName'), customerName)

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputAddress'), customerAddress)

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputExpirationDate'), new Date().format('dd/MM/yyyy'))

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputExpirationHour'), expirationHour)

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputExpirationMinutes'), expirationMinutes)

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputItemName'), itemName)

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputPrice'), price)

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputQuantity'), quantity)

WebUI.verifyElementClickable(findTestObject('Object Repository/PBL/Button/OR-ButtonCreateInvoice'))

WebUI.takeFullPageScreenshot(FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonCreateInvoice'))

WebUI.takeFullPageScreenshot(FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonCreateLink'))

GlobalVariable.G_paymentLink = WebUI.getText(findTestObject('Object Repository/PBL/OR-GetTextLinkPayment'))

WebUI.comment(GlobalVariable.G_paymentLink)

WebUI.takeFullPageScreenshot(FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonDone'))

WebUI.delay(4)

GlobalVariable.G_invoiceNumber = WebUI.getText(findTestObject('Object Repository/PBL/OR-GetTextInvoiceNumber'))

if (GlobalVariable.G_paymentLink == '') {
	GlobalVariable.G_paymentLink = WebUI.getText(findTestObject('Object Repository/PBL/OR-GetUrlLinkPayment'))
}

WebUI.comment(GlobalVariable.G_paymentLink)

WebDriver driver = DriverFactory.getWebDriver()

WebUI.executeJavaScript('window.open();', [])

currentWindow1 = WebUI.getWindowIndex()

// Go in to new tab
WebUI.switchToWindowIndex(currentWindow1 + 1)

WebUI.navigateToUrl(GlobalVariable.G_paymentLink)

WebUI.delay(3)

WebUI.takeFullPageScreenshot(FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/PBL/OR-PaymentMethod'))

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputCardNumber'), cardNumber)

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputExpiry'), '0139')

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputCvv'), '100')

WebUI.verifyElementClickable(findTestObject('Object Repository/PBL/Button/OR-ButtonPay'))

WebUI.takeFullPageScreenshot(FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonPay'))

if (threeDs == 'true') {
	WebUI.delay(30)

	if (acquirerType == 'EDC') {
		if (isFrictionless == 'false') {
			WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonNext'))

			WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputOtpCybersource'), '1234')

			WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonSubmitOtpCybersource'))
		} else {
			WebUI.comment('this transaction is frictionless')
		}
	} else {
		WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonAcsSubmit'))

		WebUI.delay(7)
	}
}

WebUI.delay(7)

//WebUI.verifyTextPresent('Payment Success', false)
WebUI.takeFullPageScreenshot(FailureHandling.OPTIONAL)

WebUI.switchToWindowIndex(WebUI.getWindowIndex() - 1)

WebUI.refresh()

WebUI.click(findTestObject('Object Repository/PBL/OR-MenuTransaction'))

WebUI.scrollToElement(findTestObject('Object Repository/PBL/OR-SubMenuReportCreditCard'), 2)

WebUI.click(findTestObject('Object Repository/PBL/OR-SubMenuReportCreditCard'))

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Object Repository/PBL/Input/OR-InputInvoiceNumberReport'), GlobalVariable.G_invoiceNumber)

WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonSearchReport'))

WebUI.takeScreenshot()

//WebUI.verifyElementPresent(findTestObject('PBL/OR-VerifyStatusTransactionReport', [('status') : status]), 2)
WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonActionReport'))

WebUI.click(findTestObject('Object Repository/PBL/Button/OR-ButtonShowDetails'))

WebUI.takeScreenshot()