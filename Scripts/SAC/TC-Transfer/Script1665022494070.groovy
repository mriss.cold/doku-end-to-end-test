import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WS.comment(desc)

WebUI.callTestCase(findTestCase('SAC/TC-RR-Login'), [('url') : 'https://app-uat.doku.com/bo/login', ('email') : email_merchant
        , ('password') : pass_merchant], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/SAC/Page_AccountList/span_Accounts'))
//WebUI.click(findTestObject('SAC/Page_AccountList/span_Sub Account'))

WebUI.click(findTestObject('Object Repository/SAC/Page_AccountList/span_Sub Account'))
//WebUI.click(findTestObject('SAC/Page_AccountList/span_Accounts'))

WebUI.delay(1)

WebUI.click(findTestObject('SAC/Page_Transfer/button_firstpage'))

destination = WebUI.getText(findTestObject('SAC/Page_Transfer/text_destination'))

WebUI.click(findTestObject('SAC/Page_Transfer/button_back'))

WebUI.click(findTestObject('SAC/Page_Transfer/button_transfer'))

WebUI.setText(findTestObject('SAC/Page_Transfer/input_origin'), origin)

WebUI.setText(findTestObject('SAC/Page_Transfer/input_destination'), destination)

WebUI.setText(findTestObject('SAC/Page_Transfer/input_amount'), amount)

WebUI.click(findTestObject('SAC/Page_Transfer/button_submit'))

WebUI.delay(0)

WebUI.click(findTestObject('SAC/Page_Transfer/button_firstpage'))

WebUI.verifyElementPresent(findTestObject('SAC/Page_Transfer/span_active'), 0)

WebUI.click(findTestObject('SAC/Page_Transfer/tab_balance'))

WebUI.takeScreenshot()

WebUI.closeBrowser()

