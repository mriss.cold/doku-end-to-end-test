import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

WS.comment(desc)

WebUI.callTestCase(findTestCase('SAC/TC-RR-Login'), [('url') : 'https://app-uat.doku.com/bo/login', ('email') : email_merchant
        , ('password') : pass_merchant], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/SAC/Page_AccountList/span_Accounts'))
//WebUI.click(findTestObject('SAC/Page_AccountList/span_Sub Account'))

WebUI.click(findTestObject('Object Repository/SAC/Page_AccountList/span_Sub Account'))
//WebUI.clicfindTestObject('Object Repository/SAC/Page_AccountList/span_Sub Account')k(findTestObject('SAC/Page_AccountList/span_Accounts'))

WebUI.delay(1)

WebUI.click(findTestObject('SAC/Page_CreateSubAccount/button_CREATE'))

def now = new Date()

String requestid = now.format('yyyymmddhhmmss')

String email = ('mail' + requestid) + '@doku.com'

String name = 'test ' + requestid

WebUI.setText(findTestObject('SAC/Page_CreateSubAccount/input_Email_emailAccount'), email)

WebUI.setText(findTestObject('SAC/Page_CreateSubAccount/input_Name_nameAccount'), name)

WebUI.takeScreenshot()

WebUI.click(findTestObject('SAC/Page_CreateSubAccount/button_Submit'))

notif = WebUI.getText(findTestObject('SAC/Page_CreateSubAccount/notifier_validation'))

WebUI.takeScreenshot()

WS.comment(notif)

WebUI.refresh()

WebUI.delay(1)

mail = WebUI.getText(findTestObject('SAC/Page_AccountList/table_firstrowemail'))

WebUI.verifyMatch(mail, email, false)

WebUI.navigateToUrl('https://app-uat.doku.com/mail')

WebUI.delay(1)

WebUI.click(findTestObject('SAC/Page_Mailcatcher/Select_Firstmessage'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('SAC/Page_Mailcatcher/Button_Confirm'), 3)

WebUI.click(findTestObject('SAC/Page_Mailcatcher/Button_Confirm'))

WebUI.switchToWindowIndex(1)

WebUI.waitForElementVisible(findTestObject('SAC/Page_DOKU2/input_Full Name_userFullName'), 3)

WebUI.setText(findTestObject('SAC/Page_DOKU2/input_Full Name_userFullName'), name)

WebUI.setText(findTestObject('SAC/Page_DOKU2/input_Mobile Phone Number_userNumber'), phone_sub)

WebUI.setText(findTestObject('SAC/Page_DOKU2/input_Password_newPassword'), pass_sub)

WebUI.setText(findTestObject('SAC/Page_DOKU2/input_Re-Type Password_rePassword'), repass_sub)

WebUI.scrollToPosition(0, 1000)

WebUI.check(findTestObject('SAC/Page_DOKU Business/input_At least 8 characters_defaultCheck1'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('SAC/Page_DOKU2/button_Register'))

WebUI.takeScreenshot()

WebUI.callTestCase(findTestCase('SAC/TC-RR-Login'), [('url') : 'https://app-uat.doku.com/bo/login', ('email') : mail, ('password') : pass_sub], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.verifyTextPresent(name, false)

WebUI.takeScreenshot()

WebUI.closeBrowser()

