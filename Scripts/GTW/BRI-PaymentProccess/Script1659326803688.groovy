//import com.doku.cm.lib.katalon.mva.bca.InputBillPresentment as InquiryRequest
//import com.doku.cm.lib.katalon.mva.bca.BillPresentment as InquiryProcess
//import com.doku.cm.lib.katalon.mva.bca.InputPaymentFlag as PaymentRequest
//import com.doku.cm.lib.katalon.mva.bca.PaymentFlag as PaymentFlag
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import internal.GlobalVariable as GlobalVariable
import utils.Utils as Utils

CustomKeywords.'com.database.DBHelper.connectDBPostgres'('BRIVA')
CustomKeywords.'com.database.DBHelper.execute'('delete from inquiry where virtual_account_number = \'2101200000099999\'')

if (testCase == 'Payment with va number expired (expired time from DOKU)')
{
	CustomKeywords.'com.database.DBHelper.execute'((('update merchant set expired_time = \'2\' where client_id = \'') + clientId) + '\'')
}

'Click below to know about test case'
WebUI.comment ('Test Case = '+testCase+' - '+notes)

invoiceNumber = CustomKeywords.'utils.Utils.generateInvoice'()

'Generate Amount'
fixAmount = amount

'Generate Request Timestamp'
TimeZone.setDefault(TimeZone.getTimeZone('GMT'))
def requestTimestamp = new Date()
requestTimestamp= requestTimestamp.format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');

'Generate Request Id'
requestId = CustomKeywords.'utils.Utils.generateInvoice'()

endPoint = 'v2/payment-code'
	
'Payment Process'

if(doGenerateVA == 'yes'){
'Generate Va Number Process'
WebUI.callTestCase(findTestCase('GTW/Template/BRI/Default_BRI_GenVANumber'), [('clientId') : clientId, ('requestId') : requestId, ('requestTimestamp') : requestTimestamp, ('endPoint') : endPoint, ('requestTarget') : requestTarget
	    , ('billingType') : billingType, ('invoiceNumber') : invoiceNumber,('bankAccountSettlementId1') : bankAccountSettlementId1,  ('type1') : type1,  ('value1') : value1, ('bankAccountSettlementId2') : bankAccountSettlementId2
		, ('type2') : type2,  ('value2') : value2,  ('plugin') : plugin,  ('removeAdditionalInfo') : removeAdditionalInfo, ('verifySignature'):'yes'
        , ('amount') : fixAmount, ('maxAmount') : maxAmount, ('minAmount') : minAmount, ('customerName') : customerName, ('customerEmail') : customerEmail, ('expiredTime') : expiredTime
        , ('reusableStatus') : reusableStatus, ('info1') : info1, ('info2') : info2, ('info3') : info3, ('info4') : info4, ('info5') : info5,  ('result') : 'success', ('errorCode') : 'null', ('paymentCode') : '', 
        , ('createdDate') : '', ('expiredDate') : '', ('errorMessage') : 'null', ('sharedKey') : sharedKey, ('testCase') : ' ', ('expectedStatusCode') : '200'], 
    FailureHandling.CONTINUE_ON_FAILURE)

}else{

'Register Paycode Process'
//WebUI.callTestCase(findTestCase('GTW/Template/BRI/Default_BRI_RegisterVANumber'),
//	[ ('clientId') : clientId, ('requestId') : requestId, ('requestTimestamp') : requestTimestamp, ('endPoint') : endPoint, ('requestTarget') : requestTarget, ('removeAdditionalInfo') : removeAdditionalInfo
//	, ('invoiceNumber') : fixInvoiceNumber, ('vaNumber') : vaNumber, ('bankAccountSettlementId1') : bankAccountSettlementId1,  ('type1') : type1,  ('value1') : value1, ('bankAccountSettlementId2') : bankAccountSettlementId2
//	, ('type2') : type2,  ('value2') : value2,  ('plugin') : plugin, ('billingType') : billingType, ('amount') : fixAmount, ('maxAmount') : maxAmount, ('minAmount') : minAmount,('verifySignature'):verifySignature
//	, ('customerName') : customerName, ('customerEmail') : customerEmail, ('expiredTime') : expiredTime, ('reusableStatus') : reusableStatus, ('info1') : info1, ('info2') : info2, ('info3') : info3, ('info4') : info4, ('info5') : info5
//	, ('result') : fixResult, ('fixVerifyInvoiceNumber') : fixVerifyInvoiceNumber, ('fixVerifyVaNumber') : fixVerifyVaNumber, ('errorMessage') : errorMessage, ('errorType') : errorType, ('errorCode') : errorCode, ('sharedKey') : sharedKey
//	, ('testCase') : testCase, ('no') : no, ('expectedStatusCode') : fixStatusCode, ('howToPayAPI') : howToPayAPI, ('howToPayPage') : howToPayPage ], FailureHandling.CONTINUE_ON_FAILURE)

vaNumber = 	'123622' + CustomKeywords.'utils.Utils.generateInvoice'().substring(0,11)
WebUI.callTestCase(findTestCase('GTW/Template/BRI/Default_BRI_RegisterVANumber'),
	[ ('clientId') : clientId, ('requestId') : requestId, ('requestTimestamp') : requestTimestamp, ('endPoint') : endPoint, ('requestTarget') : requestTarget, ('removeAdditionalInfo') : 'yes'
	, ('invoiceNumber') : invoiceNumber, ('vaNumber') : vaNumber, ('bankAccountSettlementId1') : 'ACCOUNT001',  ('type1') : 'FIX',  ('value1') : '10000', ('bankAccountSettlementId2') : 'ACCOUNT002'
	, ('type2') : 'PERCENTAGE',  ('value2') : '5',  ('plugin') : 'Wordpress', ('billingType') : billingType, ('amount') : fixAmount, ('maxAmount') : maxAmount, ('minAmount') : minAmount, ('verifySignature'):'yes'
	, ('customerName') : customerName, ('customerEmail') : customerEmail, ('expiredTime') : expiredTime, ('reusableStatus') : reusableStatus, ('info1') : info1, ('info2') : info2, ('info3') : info3, ('info4') : info4, ('info5') : info5, 
	, ('result') : 'success', ('fixVerifyInvoiceNumber') : 'yes', ('fixVerifyVaNumber') : 'yes', ('errorMessage') : 'null', ('errorType') : '', ('errorCode') : '', ('sharedKey') : sharedKey
	, ('testCase') : testCase, ('no') : no, ('expectedStatusCode') : '200'], FailureHandling.CONTINUE_ON_FAILURE)
	
	
}

String fixVaNumber = GlobalVariable.G_paymentCode
WebUI.comment('payment code = '+fixVaNumber)
fixCompanyCode = fixVaNumber.substring(0,5)

retrySameTransaksiId = CustomKeywords.'utils.Utils.generateInvoice'()
trxDateTime = CustomKeywords.'utils.Utils.generateDate'()

i = retry == 'yes' ? 2 : 1

'Payment Process'
for (j = 0; j < i; j++) {
	WebUI.delay(3)
	if ( j == 0)
		{
			fixStatus = status
			fixVerifyAmount = verifyAmount
			fixVerifyInfo1 = verifyInfo1
			fixVerifyCustomerName = verifyCustomerName
			fixVerifyCurrency = verifyCurrency
			fixStatusBill = statusBill
			fixErrorCode = errorCode
			fixErrorDesc = errorDesc
			fixPaymentNumber = paymentNumber
			fixAmountPayment = paidAmountPayment
			
			fixErrorCodePayment = errorCodePayment
			fixErrorDescPayment = errorDescPayment
			fixIsError = isError
			
			fixStatusPayment = statusPayment
			
			G_amountPayment = fixAmountPayment
			
			fixVerifyErrorCodePaymentDb = errorCodePayment
			fixVerifyErrorDescPaymentDb = errorDescPayment
			
		}
	else	
		{
			fixStatus = secondStatusInquiry
			fixVerifyAmount = secondVerifyAmount
			fixVerifyInfo1 = secondVerifyInfo1
			fixVerifyCustomerName = secondVerifyCustomerName
			fixVerifyCurrency = secondVerifyCurrency
			fixStatusBill = secondStatusBill
			fixErrorCode = secondErrorCode
			fixErrorDesc = secondErrorDesc
			fixPaymentNumber = paymentNumber2
			fixAmountPayment = secondPaidAmountPayment
			
			fixErrorCodePayment = secondErrorCodePayment
			fixErrorDescPayment = secondErrorDescPayment
			fixIsError = secondIsError
			
			fixStatusPayment = secondStatusPayment
			
			fixVerifyErrorCodePaymentDb = verifySecondErrorCodeDb
			fixVerifyErrorDescPaymentDb = verifySecondErrorDescDb
		}
				
if(testCase == 'Payment with response inquiry failed')
	{
		WebUI.delay(62)
	}
	
'Inquiry Process'
if ((j == 0 && firstTriger == 'with Inquiry')	|| (j == 1 && secondTriger == 'with Inquiry'))
{	

if(j == 1 && secondTriger == 'with Inquiry'){
	trxDateTime = CustomKeywords.'utils.Utils.generateDate'()
}
		
'Inquiry Process'
WebUI.callTestCase(findTestCase('GTW/Template/BRI/Default_BRI_Inquiry'), [('idApp') : idApp, ('passApp') : passApp
			   , ('transmisiDateTime') : trxDateTime, ('bankId') : bankId, ('terminalId') : terminalId, ('brivaNum') : fixVaNumber,('expectedStatusCode') : statusCodeInquiry
			   , ('billingType') : billingType, ('statusBill') : fixStatusBill, ('currency') : fixVerifyCurrency, ('verifyAmount') : fixVerifyAmount
			   , ('verifyInfo1') : fixVerifyInfo1, ('verifyInfo2') : verifyInfo2, ('verifyInfo3') : verifyInfo3, ('verifyInfo4') : verifyInfo4, ('verifyInfo5') : verifyInfo5, ('verifyCustomerName') : fixVerifyCustomerName
			   ], FailureHandling.STOP_ON_FAILURE)
} 

transmisiDateTimePayment = CustomKeywords.'utils.Utils.generateDate'()
trxDateTimePayment = CustomKeywords.'utils.Utils.generateDate'()
if(invalidTrxDate != '')
{
  trxDateTimePayment = CustomKeywords.'utils.Utils.generateLocalTime2'()
}

transaksiId = CustomKeywords.'utils.Utils.generateInvoice'()
if(retryWithSameTransaksiId == 'yes')
{
transaksiId = retrySameTransaksiId
}

G_transactionIdPayment = ''
G_amountPayment = ''
if ( j == 0)
{
	G_transactionIdPayment = transaksiId
}


if(notes == 'invalid va number')
{
 fixVaNumber = '2101200000099999'
}

if(testCase == 'Payment with va number expired (expired time from merchant)')
{
	WebUI.delay(62)
}
else if (testCase == 'Payment with va number expired (expired time from DOKU)')
{
	WebUI.delay(122)
}

'Payment Process'
WebUI.callTestCase(findTestCase('GTW/Template/BRI/Default_BRI_Payment'), [('idApp') : idApp, ('passApp') : passApp
		, ('transmisiDateTime') : transmisiDateTimePayment, ('transaksiDateTime') : trxDateTimePayment, ('bankId') : bankId, ('terminalId') : terminalId, ('brivaNum') : fixVaNumber, ('transaksiId') : transaksiId, ('expectedStatusCode') : statusCodeInquiry
		, ('paymentAmount') : fixAmountPayment, ('errorCodePayment') : fixErrorCodePayment, ('errorDescPayment') : fixErrorDescPayment, ('isError') : fixIsError
		, ('info1') : info1, ('info2') : info2, ('info3') : info3, ('info4') : info4, ('info5') : info5
		, ('verifyInfo1') : verifyInfo1Payment, ('verifyInfo2') : verifyInfo2, ('verifyInfo3') : verifyInfo3, ('verifyInfo4') : verifyInfo4, ('verifyInfo5') : verifyInfo5
		], FailureHandling.STOP_ON_FAILURE)


'Get merchant_company_code'
if (Method == 'DIRECT_MERCHANT')
{
	if (billingType == 'FIX_BILL')
			{
				rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from identifier where client_id = \'' +clientId) + '\' and name = \'BIN\' and feature = \'DDGPC_FIX_BILL\'')
				while (rset.next()) 
					{
						merchantCompanyCode = rset.getString('value')
						WebUI.comment('company_code = '+merchantCompanyCode)
					}
			}
	else if (billingType == 'NO_BILL')
			{
				rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from identifier where client_id = \'' +clientId) + '\' and name = \'BIN\' and feature = \'DDGPC_NO_BILL\'')
				while (rset.next()) 
					{
						merchantCompanyCode = rset.getString('value')
						WebUI.comment('company_code = '+merchantCompanyCode)
					}
			}
}
if (Method == 'AGGREGATOR_MERCHANT')
{
	if (billingType == 'FIX_BILL')
			{
				rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from identifier where client_id = \'' +clientId) + '\' and name = \'BIN\' and feature = \'ADGPC_FIX_BILL\'')
				while (rset.next())
					{
						merchantCompanyCode = rset.getString('value')
						WebUI.comment('company_code = '+merchantCompanyCode)
					}
			}
	else if (billingType == 'NO_BILL')
			{
				rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from identifier where client_id = \'' +clientId) + '\' and name = \'BIN\' and feature = \'ADGPC_NO_BILL\'')
				while (rset.next())
					{
						merchantCompanyCode = rset.getString('value')
						WebUI.comment('company_code = '+merchantCompanyCode)
					}
			}
}

'Verify Register Table in db'
rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from register where virtual_account_number = \'' +fixVaNumber) + '\' order by id desc limit 1')

WebUI.comment('select * from register where virtual_account_number = \'' +fixVaNumber + '\' order by id desc limit 1')

while (rset.next()) {

	idRegister = rset.getString('id')
	WebUI.comment('idRegister in Register ='+idRegister)
	
	createdDateRegister = rset.getString('created_date')
	WebUI.comment('created_date in Register ='+createdDateRegister)
	
	expiredDateRegister = rset.getString('expired_date')
	WebUI.comment('expired_date in Register ='+expiredDateRegister)

	WebUI.verifyMatch(rset.getString('payment_number'), fixPaymentNumber, false, FailureHandling.CONTINUE_ON_FAILURE)
	
}

'Verify Inquiry Table in db'
rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from inquiry where virtual_account_number = \'' +fixVaNumber) + '\' order by id desc limit 1')
WebUI.comment('select * from inquiry where virtual_account_number = \'' +fixVaNumber + '\' order by id desc limit 1')

while (rset.next()) {
	String NULL = 'NULL'
	
	inquiryId = rset.getString('id') == null ? NULL : rset.getString('id')
	WebUiBuiltInKeywords.verifyNotMatch(inquiryId, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
	
	uuid = rset.getString('uuid') == null ? NULL : rset.getString('uuid')
	WebUiBuiltInKeywords.verifyNotMatch(uuid, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
	
	created_date = rset.getString('created_date') == null ? NULL : rset.getString('created_date')
	WebUiBuiltInKeywords.verifyNotMatch(created_date, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
	
	transmisi_date = rset.getString('transmisi_date') == null ? NULL : rset.getString('transmisi_date')
	WebUiBuiltInKeywords.verifyNotMatch(transmisi_date, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
	
	updated_date = rset.getString('updated_date') == null ? NULL : rset.getString('updated_date')
	WebUiBuiltInKeywords.verifyMatch(updated_date, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
	header_request_id = rset.getString('header_request_id') == null ? NULL : rset.getString('header_request_id')
	WebUiBuiltInKeywords.verifyMatch(header_request_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
	
	header_response_id = rset.getString('header_response_id') == null ? NULL : rset.getString('header_response_id')
	WebUiBuiltInKeywords.verifyMatch(header_response_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
	WebUI.verifyMatch(rset.getString('client_id'), clientId, false, FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.verifyMatch(rset.getString('company_code'), fixCompanyCode, false, FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.verifyMatch(rset.getString('merchant_company_code'), merchantCompanyCode, false, FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.verifyMatch(rset.getString('register_id'), idRegister, false, FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.verifyMatch(rset.getString('invoice_number'), invoiceNumber, false, FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.verifyMatch(rset.getString('virtual_account_number'), fixVaNumber, false, FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.verifyMatch(rset.getString('bank_id'), bankId, false, FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.verifyMatch(rset.getString('terminal_id'), terminalId, false, FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.verifyMatch(rset.getString('customer_name'), customerName, false, FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.verifyMatch(rset.getString('customer_email'), customerEmail, false, FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.verifyMatch(rset.getString('expired_date'), expiredDateRegister, false, FailureHandling.CONTINUE_ON_FAILURE)
			
	if (billingType == '' )
		 {
				WebUI.verifyMatch(rset.getString('billing_type'), 'FIX_BILL', false, FailureHandling.CONTINUE_ON_FAILURE)
		 }
	else
		 {
				WebUI.verifyMatch(rset.getString('billing_type'), billingType, false, FailureHandling.CONTINUE_ON_FAILURE)
		 }
								
	if (billingType == 'FIX_BILL')
		 {
				WebUI.verifyMatch(rset.getString('amount'), fixAmount+'.00', false, FailureHandling.CONTINUE_ON_FAILURE)
		 }
	else if (billingType == 'NO_BILL')
		 {
				WebUI.verifyMatch(rset.getString('amount'), '0', false, FailureHandling.CONTINUE_ON_FAILURE)
		 }
			
		   
	if(amountType == 'limit')
		 {
				WebUI.verifyMatch(rset.getString('min_amount'), minAmount, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('max_amount'), maxAmount, false, FailureHandling.CONTINUE_ON_FAILURE)
		 }
	else if(amountType == 'minAmount')
		 {
				WebUI.verifyMatch(rset.getString('min_amount'), minAmount, false, FailureHandling.CONTINUE_ON_FAILURE)
					
				max_amount = rset.getString('max_amount') == null ? NULL : rset.getString('max_amount')
				WebUiBuiltInKeywords.verifyMatch(max_amount, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		 }
			
	else if (amountType == 'maxAmount')
		{
				WebUI.verifyMatch(rset.getString('max_amount'), maxAmount, false, FailureHandling.CONTINUE_ON_FAILURE)
					
				min_amount = rset.getString('min_amount') == null ? NULL : rset.getString('min_amount')
				WebUiBuiltInKeywords.verifyMatch(min_amount, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		}
	else
		{
				min_amount = rset.getString('min_amount') == null ? NULL : rset.getString('min_amount')
				WebUiBuiltInKeywords.verifyMatch(min_amount, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					
				max_amount = rset.getString('max_amount') == null ? NULL : rset.getString('max_amount')
				WebUiBuiltInKeywords.verifyMatch(max_amount, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		}
		
		
			
	if (j == 0)
	{
		WebUI.verifyMatch(rset.getString('state'), state, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('status'), status, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('status_bill'), statusBill, false, FailureHandling.CONTINUE_ON_FAILURE)
		
	}
	else
	{
		WebUI.verifyMatch(rset.getString('state'), secondStateInquiry, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('status'), secondStatusInquiry, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('status_bill'), secondStatusBill, false, FailureHandling.CONTINUE_ON_FAILURE)
	}
	
	if (fixStatus == 'SUCCESS')
		{
			error_code = rset.getString('error_code') == null ? NULL : rset.getString('error_code')
			WebUiBuiltInKeywords.verifyMatch(error_code, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			error_desc = rset.getString('error_desc') == null ? NULL : rset.getString('error_desc')
			WebUiBuiltInKeywords.verifyMatch(error_desc, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			WebUI.verifyMatch(rset.getString('currency_code'), fixVerifyCurrency, false, FailureHandling.CONTINUE_ON_FAILURE)
		}
    else
		{
			WebUI.verifyMatch(rset.getString('error_code'), fixErrorCode, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('error_desc'), fixErrorDesc, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			currency_code = rset.getString('currency_code') == null ? NULL : rset.getString('currency_code')
			WebUiBuiltInKeywords.verifyMatch(currency_code, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		}
	
    
	if(testCase.contains('optional params') )
		{
			WebUI.verifyMatch(rset.getString('id_app'), idApp, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('pass_app'), passApp, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			additional_info = rset.getString('additional_info') == null ? NULL : rset.getString('additional_info')
			WebUiBuiltInKeywords.verifyNotMatch(additional_info, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		}
	else
		{
			id_app = rset.getString('id_app') == null ? NULL : rset.getString('id_app')
			WebUiBuiltInKeywords.verifyMatch(id_app, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			pass_app = rset.getString('pass_app') == null ? NULL : rset.getString('pass_app')
			WebUiBuiltInKeywords.verifyMatch(pass_app, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			additional_info = rset.getString('additional_info') == null ? NULL : rset.getString('additional_info')
			WebUiBuiltInKeywords.verifyMatch(additional_info, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		}
		
	if(notes == 'plus all info params' )
		{
			WebUI.verifyMatch(rset.getString('info1'), info1, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('info2'), info2, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('info3'), info3, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('info4'), info4, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('info5'), info5, false, FailureHandling.CONTINUE_ON_FAILURE)
		}
		
	else if(notes == 'plus info1')
		{
			WebUI.verifyMatch(rset.getString('info1'), info1, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info2 = rset.getString('info2') == null ? NULL : rset.getString('info2')
			WebUiBuiltInKeywords.verifyMatch(info2, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info3 = rset.getString('info3') == null ? NULL : rset.getString('info3')
			WebUiBuiltInKeywords.verifyMatch(info3, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info4 = rset.getString('info4') == null ? NULL : rset.getString('info4')
			WebUiBuiltInKeywords.verifyMatch(info4, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info5 = rset.getString('info5') == null ? NULL : rset.getString('info5')
			WebUiBuiltInKeywords.verifyMatch(info5, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
	    }
		
	else if(notes == 'plus info2')
		{
			WebUI.verifyMatch(rset.getString('info2'), info2, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info1 = rset.getString('info1') == null ? NULL : rset.getString('info1')
			WebUiBuiltInKeywords.verifyMatch(info1, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info3 = rset.getString('info3') == null ? NULL : rset.getString('info3')
			WebUiBuiltInKeywords.verifyMatch(info3, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info4 = rset.getString('info4') == null ? NULL : rset.getString('info4')
			WebUiBuiltInKeywords.verifyMatch(info4, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info5 = rset.getString('info5') == null ? NULL : rset.getString('info5')
			WebUiBuiltInKeywords.verifyMatch(info5, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		}
		
	else if(notes == 'plus info3')
		{
			WebUI.verifyMatch(rset.getString('info3'), info3, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info1 = rset.getString('info1') == null ? NULL : rset.getString('info1')
			WebUiBuiltInKeywords.verifyMatch(info1, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info2 = rset.getString('info2') == null ? NULL : rset.getString('info2')
			WebUiBuiltInKeywords.verifyMatch(info2, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)	
			
			info4 = rset.getString('info4') == null ? NULL : rset.getString('info4')
			WebUiBuiltInKeywords.verifyMatch(info4, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info5 = rset.getString('info5') == null ? NULL : rset.getString('info5')
			WebUiBuiltInKeywords.verifyMatch(info5, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		}	
	else
		{
			info1 = rset.getString('info1') == null ? NULL : rset.getString('info1')
			WebUiBuiltInKeywords.verifyMatch(info1, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
			info2 = rset.getString('info2') == null ? NULL : rset.getString('info2')
			WebUiBuiltInKeywords.verifyMatch(info2, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info3 = rset.getString('info3') == null ? NULL : rset.getString('info3')
			WebUiBuiltInKeywords.verifyMatch(info3, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info4 = rset.getString('info4') == null ? NULL : rset.getString('info4')
			WebUiBuiltInKeywords.verifyMatch(info4, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			info5 = rset.getString('info5') == null ? NULL : rset.getString('info5')
			WebUiBuiltInKeywords.verifyMatch(info5, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)

		}
	}


rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from inquiry where virtual_account_number = \'' +fixVaNumber) + '\' order by id asc limit 1')

while (rset.next()) {
	inquiryIdAsc = rset.getString('id')
}


'Verify Payment Table in db'
		rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from payment where virtual_account_number = \'' +fixVaNumber) + '\' order by id desc limit 1')
		WebUI.comment('select * from payment where virtual_account_number = \'' +fixVaNumber + '\' order by id desc limit 1')
		
		while (rset.next()) {
			
		String NULL = 'NULL'
		
		id = rset.getString('id') == null ? NULL : rset.getString('id')
		WebUiBuiltInKeywords.verifyNotMatch(id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		uuid = rset.getString('uuid') == null ? NULL : rset.getString('uuid')
		WebUiBuiltInKeywords.verifyNotMatch(uuid, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		created_date = rset.getString('created_date') == null ? NULL : rset.getString('created_date')
		WebUiBuiltInKeywords.verifyNotMatch(created_date, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		transmisi_date = rset.getString('transmisi_date') == null ? NULL : rset.getString('transmisi_date')
		WebUiBuiltInKeywords.verifyNotMatch(transmisi_date, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		transaction_date = rset.getString('transaction_date') == null ? NULL : rset.getString('transaction_date')
		WebUiBuiltInKeywords.verifyNotMatch(transaction_date, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		if (getIdInquiryAsc == 'yes')
			{
				WebUI.verifyMatch(rset.getString('inquiry_id'), inquiryIdAsc, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
		else
			{
				WebUI.verifyMatch(rset.getString('inquiry_id'), inquiryId, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
		
		WebUI.verifyMatch(rset.getString('client_id'), clientId, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('company_code'), fixCompanyCode, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('merchant_company_code'), merchantCompanyCode, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('invoice_number'), invoiceNumber, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		WebUI.verifyMatch(rset.getString('virtual_account_number'), fixVaNumber, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('bank_id'), bankId, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('terminal_id'), terminalId, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		WebUI.verifyMatch(rset.getString('error_code'), fixVerifyErrorCodePaymentDb, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('error_desc'), fixVerifyErrorDescPaymentDb, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		WebUI.verifyMatch(rset.getString('currency_code'), verifyCurrency, false, FailureHandling.CONTINUE_ON_FAILURE)
				
		if (j == 0)
		{
			WebUI.verifyMatch(rset.getString('state'), statePayment, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('status'), statusPayment, false, FailureHandling.CONTINUE_ON_FAILURE)	
			WebUI.verifyMatch(rset.getString('amount'), fixAmountPayment+'.00', false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('transaction_id'), transaksiId, false, FailureHandling.CONTINUE_ON_FAILURE)
		}
		else
		{
			WebUI.verifyMatch(rset.getString('state'), secondStatePayment, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('status'), secondStatusPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			if (saveSecondAmountPayment == 'yes')
				{
					WebUI.verifyMatch(rset.getString('amount'), fixAmountPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
			else
				{
					WebUI.verifyMatch(rset.getString('amount'), G_amountPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
				
				
			if (saveSecondTransaksiId == 'yes')
				{
					WebUI.verifyMatch(rset.getString('transaction_id'), transaksiId, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
			else
				{
					WebUI.verifyMatch(rset.getString('transaction_id'), G_transactionIdPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
		}
		
		if (fixStatusPayment == 'SUCCESS')
			{	
				header_request_id = rset.getString('header_request_id') == null ? NULL : rset.getString('header_request_id')
				WebUiBuiltInKeywords.verifyNotMatch(header_request_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				header_response_id = rset.getString('header_response_id') == null ? NULL : rset.getString('header_response_id')
				WebUiBuiltInKeywords.verifyNotMatch(header_response_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
		else
			{	
				header_request_id = rset.getString('header_request_id') == null ? NULL : rset.getString('header_request_id')
				WebUiBuiltInKeywords.verifyMatch(header_request_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				header_response_id = rset.getString('header_response_id') == null ? NULL : rset.getString('header_response_id')
				WebUiBuiltInKeywords.verifyMatch(header_response_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
			}
		
		
		if(testCase.contains('optional params') )
			{
				WebUI.verifyMatch(rset.getString('id_app'), idApp, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('pass_app'), passApp, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				additional_info = rset.getString('additional_info') == null ? NULL : rset.getString('additional_info')
				WebUiBuiltInKeywords.verifyNotMatch(additional_info, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
		else
			{
				id_app = rset.getString('id_app') == null ? NULL : rset.getString('id_app')
				WebUiBuiltInKeywords.verifyMatch(id_app, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				pass_app = rset.getString('pass_app') == null ? NULL : rset.getString('pass_app')
				WebUiBuiltInKeywords.verifyMatch(pass_app, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				additional_info = rset.getString('additional_info') == null ? NULL : rset.getString('additional_info')
				WebUiBuiltInKeywords.verifyMatch(additional_info, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
			
		if(notes == 'plus all info params' )
			{
				WebUI.verifyMatch(rset.getString('info1'), info1, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('info2'), info2, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('info3'), info3, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('info4'), info4, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('info5'), info5, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
			
		else if(notes == 'plus info1')
			{
				WebUI.verifyMatch(rset.getString('info1'), info1, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info2 = rset.getString('info2') == null ? NULL : rset.getString('info2')
				WebUiBuiltInKeywords.verifyMatch(info2, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info3 = rset.getString('info3') == null ? NULL : rset.getString('info3')
				WebUiBuiltInKeywords.verifyMatch(info3, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info4 = rset.getString('info4') == null ? NULL : rset.getString('info4')
				WebUiBuiltInKeywords.verifyMatch(info4, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info5 = rset.getString('info5') == null ? NULL : rset.getString('info5')
				WebUiBuiltInKeywords.verifyMatch(info5, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
			
		else if(notes == 'plus info2')
			{
				WebUI.verifyMatch(rset.getString('info2'), info2, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info1 = rset.getString('info1') == null ? NULL : rset.getString('info1')
				WebUiBuiltInKeywords.verifyMatch(info1, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info3 = rset.getString('info3') == null ? NULL : rset.getString('info3')
				WebUiBuiltInKeywords.verifyMatch(info3, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info4 = rset.getString('info4') == null ? NULL : rset.getString('info4')
				WebUiBuiltInKeywords.verifyMatch(info4, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info5 = rset.getString('info5') == null ? NULL : rset.getString('info5')
				WebUiBuiltInKeywords.verifyMatch(info5, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
			
		else if(notes == 'plus info3')
			{
				WebUI.verifyMatch(rset.getString('info3'), info3, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info1 = rset.getString('info1') == null ? NULL : rset.getString('info1')
				WebUiBuiltInKeywords.verifyMatch(info1, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info2 = rset.getString('info2') == null ? NULL : rset.getString('info2')
				WebUiBuiltInKeywords.verifyMatch(info2, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info4 = rset.getString('info4') == null ? NULL : rset.getString('info4')
				WebUiBuiltInKeywords.verifyMatch(info4, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info5 = rset.getString('info5') == null ? NULL : rset.getString('info5')
				WebUiBuiltInKeywords.verifyMatch(info5, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
		else
			{
				info1 = rset.getString('info1') == null ? NULL : rset.getString('info1')
				WebUiBuiltInKeywords.verifyMatch(info1, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
				info2 = rset.getString('info2') == null ? NULL : rset.getString('info2')
				WebUiBuiltInKeywords.verifyMatch(info2, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info3 = rset.getString('info3') == null ? NULL : rset.getString('info3')
				WebUiBuiltInKeywords.verifyMatch(info3, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info4 = rset.getString('info4') == null ? NULL : rset.getString('info4')
				WebUiBuiltInKeywords.verifyMatch(info4, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				info5 = rset.getString('info5') == null ? NULL : rset.getString('info5')
				WebUiBuiltInKeywords.verifyMatch(info5, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
			}
		  }
		}
if (testCase == 'Payment with va number expired (expired time from DOKU)')
{
	CustomKeywords.'com.database.DBHelper.execute'((('update merchant set expired_time = \'120\' where client_id = \'') + clientId) + '\'')
}