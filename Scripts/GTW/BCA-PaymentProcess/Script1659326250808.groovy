import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.doku.cm.lib.katalon.mva.bca.BillPresentment as InquiryProcess
import com.doku.cm.lib.katalon.mva.bca.InputBillPresentment as InquiryRequest
import com.doku.cm.lib.katalon.mva.bca.InputPaymentFlag as PaymentRequest
import com.doku.cm.lib.katalon.mva.bca.PaymentFlag as PaymentFlag
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.logging.KeywordLogger


import internal.GlobalVariable as GlobalVariable


def containerEnv = System.getenv('CONTAINER_ENV')
println('CONTAINER_DEV: ' + containerEnv)

inquiryEndPoint = GlobalVariable.hostNameSIT+'/bca-virtual-account/v1/BCAVirtualAccount'
//if (containerEnv != null) {
//    inquiryEndPoint = GlobalVariable.hostNameDevBCA+'v1/BCAVirtualAccount'
//	}

CustomKeywords.'com.database.DBHelper.connectDBPostgres'('BCAVA')

CustomKeywords.'com.database.DBHelper.execute'((('update identifier set status = \'ACTIVE\' where client_id = \'') + clientId) + '\'')
CustomKeywords.'com.database.DBHelper.execute'('update identifier set name = \'PREFIX_MERCHANT\' where id in (52)')
CustomKeywords.'com.database.DBHelper.execute'('update identifier set value = \'DOKUMerc\' where id = \'52\'')


if (testCase == 'Payment with va number expired (expired time from DOKU)') {
	CustomKeywords.'com.database.DBHelper.execute'((('update merchant set expired_time = \'2\' where client_id = \'') + clientId) + '\'')
	}

CustomKeywords.'com.database.DBHelper.execute'((('update merchant set encrypt_status = \'f\' where client_id = \'') + clientId) + '\'')
if (testCase.contains('encryptedStatus true'))
{
	CustomKeywords.'com.database.DBHelper.execute'((('update merchant set encrypt_status = \'t\' where client_id = \'') + clientId) + '\'')
}

KeywordLogger log = new KeywordLogger()

'Click below to know about test case'
WebUI.comment('Test Case = '+testCase+' - '+notes)

invoiceNumber = CustomKeywords.'utils.Utils.generateInvoice'()
fixAmount = amount
	
'Generate Request Timestamp'
TimeZone.setDefault(TimeZone.getTimeZone('GMT'))
def requestTimestamp = new Date()
requestTimestamp= requestTimestamp.format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');

'Generate Request Id'
requestId = CustomKeywords.'utils.Utils.generateInvoice'()

endPoint = 'v2/payment-code'
String fixVaNumber;
if(doGenerateVA == 'yes'){
	'Generate Paycode Process'
	WebUI.callTestCase(findTestCase('GTW/Template/BCA/Default_BCAGenVANumber'), [('clientId') : clientId,('requestId') : requestId, ('requestTimestamp') : requestTimestamp, ('endPoint') : endPoint, ('requestTarget') : requestTarget
		    , ('billingType') : billingType, ('invoiceNumber') : invoiceNumber,  ('itemName1') : itemName1, ('itemPrice1') : itemPrice1, ('itemQuantity1') : itemQuantity1, ('verifySignature') : 'yes'
	        , ('itemName2') : itemName2, ('itemPrice2') : itemPrice2, ('itemQuantity2') : itemQuantity2,  ('amount') : fixAmount, ('maxAmount') : maxAmount, ('minAmount') : minAmount, ('customerName') : customerName, ('customerEmail') : customerEmail, ('expiredTime') : expiredTime
	        , ('reusableStatus') : reusableStatus, ('info1') : info1, ('info2') : info2, ('info3') : info3, ('result') : 'success', ('errorCode') : 'null', ('paymentCode') : '', 
	        , ('createdDate') : '', ('expiredDate') : '', ('errorMessage') : 'null', ('sharedKey') : sharedKey, ('testCase') : ' ', ('expectedStatusCode') : '200'], 
	    FailureHandling.CONTINUE_ON_FAILURE)
	
}else{
//	fixVaNumber = vaNumber
//	CustomKeywords.'com.database.DBHelper.execute'('delete from inquiry where virtual_account_number = \'' + fixVaNumber + '\'')
//	CustomKeywords.'com.database.DBHelper.execute'('delete from payment where virtual_account_number = \'' + fixVaNumber + '\'')
	vaNumber = '190080'+CustomKeywords.'utils.Utils.generateRandom'().substring(0,10)
	'Register Paycode Process'
	WebUI.callTestCase(findTestCase('GTW/Template/BCA/Default_BCA_RegisterVANumber'),
		[ ('clientId') : clientId, ('requestId') : requestId, ('requestTimestamp') : requestTimestamp, ('endPoint') : endPoint, ('requestTarget') : requestTarget, ('removeAdditionalInfo') : 'yes'
		, ('invoiceNumber') : invoiceNumber, ('vaNumber') : vaNumber, ('bankAccountSettlementId1') : 'ACCOUNT001',  ('type1') : 'FIX',  ('value1') : '10000', ('bankAccountSettlementId2') : 'ACCOUNT002'
		, ('type2') : 'PERCENTAGE',  ('value2') : '5',  ('plugin') : 'Wordpress', ('billingType') : billingType, ('amount') : fixAmount, ('maxAmount') : maxAmount, ('minAmount') : minAmount, ('verifySignature'):'yes'
		, ('customerName') : customerName, ('customerEmail') : customerEmail, ('expiredTime') : expiredTime, ('reusableStatus') : reusableStatus, ('info1') : info1, ('info2') : info2, ('info3') : info3
		, ('result') : 'success', ('fixVerifyInvoiceNumber') : 'yes', ('fixVerifyVaNumber') : 'yes', ('errorMessage') : 'null', ('errorType') : '', ('errorCode') : '', ('sharedKey') : sharedKey
		, ('testCase') : testCase, ('no') : no, ('expectedStatusCode') : '200'], FailureHandling.CONTINUE_ON_FAILURE)
		
	
	
}

fixVaNumber = GlobalVariable.G_paymentCode
//	WebUI.comment('payment code = '+paymentCode)
fixCompanyCode = fixVaNumber.substring(0,5)

requestId = CustomKeywords.'utils.Utils.generateLocalTime'()
referencePayment = CustomKeywords.'utils.Utils.generateLocalTime'()
trxDateTime = CustomKeywords.'utils.Utils.generateLocalTime2'()

customerNumber = fixVaNumber.substring(fixVaNumber.lastIndexOf(fixCompanyCode) + 5);
customerNumber = customerNumber.substring(0, 11);

if(testCase == 'Payment with response inquiry failed')
{
	WebUI.delay(62)
}

i = retry == 'yes' ? 2 : 1

'Payment Process'
for (j = 0; j < i; j++) {
	WebUI.delay(2)
	WebUI.comment('looping ke - ' +j)
	
if (diffTrx == 'yes' && j == 1) 
{
	WebUI.delay(3)
	requestId = CustomKeywords.'utils.Utils.generateLocalTime'()
	referencePayment = CustomKeywords.'utils.Utils.generateLocalTime'()
}

if (diffRequestIdPayment == 'yes') 
{
	WebUI.delay(3)
	requestId = CustomKeywords.'utils.Utils.generateLocalTime'()
} 
else
{
	requestId = requestId
} 

if (diffReferencePayment == 'yes')
{
	WebUI.delay(3)
	referencePayment = CustomKeywords.'utils.Utils.generateLocalTime'()
}
else
{
	referencePayment = referencePayment
}


if(testCase == 'Payment with mandatory params with PREFIX_MERCHANT inactive')
{
	CustomKeywords.'com.database.DBHelper.execute'('update identifier set status = \'INACTIVE\' where id = \'52\'')
}

if(testCase == 'Payment with mandatory params with PREFIX_MERCHANT value more than 8 char')
{
	CustomKeywords.'com.database.DBHelper.execute'('update identifier set value = \'DOKUMerc123456789\' where id = \'52\'')
}

if(testCase == 'Payment with mandatory params with value name in identifier table not "PREFIX_MERCHANT"')
{
	CustomKeywords.'com.database.DBHelper.execute'('update identifier set name = \'BIN2\' where id = \'52\'')
}

	
'Inquiry Process'
if ((j == 0 && firstTriger == 'with Inquiry')	|| (j == 1 && secondTriger == 'with Inquiry'))
{
	
WebUI.comment(inquiryEndPoint) 
InquiryProcess inquiryProcess = new InquiryProcess()
InquiryRequest inquiryRequest = new InquiryRequest()

requestId_Inquiry = CustomKeywords.'utils.Utils.generateInvoice'()

inquiryRequest.setCompanyCode(fixCompanyCode)
inquiryRequest.setCustomerNumber(customerNumber)
inquiryRequest.setRequestID(requestId_Inquiry)
inquiryRequest.setChannelType(channelType)
inquiryRequest.setTransactionDate(trxDateTime)
if (additionalData == '#')
{
	inquiryRequest.setAdditionalData('')
}
else
{
	inquiryRequest.setAdditionalData(additionalData)
}


responseInq = inquiryProcess.getBillPresentment(inquiryRequest, inquiryEndPoint)
if(j == 0) {
	WS.verifyEqual(responseInq.companyCode, fixCompanyCode, FailureHandling.STOP_ON_FAILURE)
	WS.verifyEqual(responseInq.customerNumber, customerNumber, FailureHandling.STOP_ON_FAILURE)
	WS.verifyEqual(responseInq.requestID, requestId_Inquiry, FailureHandling.STOP_ON_FAILURE)
	WS.verifyEqual(responseInq.inquiryStatus, inquiryStatus, FailureHandling.STOP_ON_FAILURE)
	WS.verifyEqual(responseInq.inquiryReason.indonesian, inquiryReasonIndonesia, FailureHandling.STOP_ON_FAILURE)
	WS.verifyEqual(responseInq.inquiryReason.english, inquiryReasonEnglish, FailureHandling.STOP_ON_FAILURE)
	
	if(inquiryReasonIndonesia == 'Sukses')
		{
			WS.verifyEqual(responseInq.customerName, verifyCustomerName, FailureHandling.STOP_ON_FAILURE)
			WS.verifyEqual(responseInq.currencyCode, currency, FailureHandling.STOP_ON_FAILURE)
		
				if (billingType == 'FIX_BILL' || billingType == 'BILL_VARIABLE_AMOUNT' || billingType =='')
					{
						WS.verifyEqual(responseInq.totalAmount, fixAmount+'.00', FailureHandling.STOP_ON_FAILURE)
					}
				else if (billingType == 'NO_BILL')
					{
						WS.verifyEqual(responseInq.totalAmount, '0.00', FailureHandling.STOP_ON_FAILURE)
					}
	
					
			WS.verifyEqual(responseInq.subCompany, subCompanyInquiryResponse, FailureHandling.STOP_ON_FAILURE)
			
//			if(notes == 'plus all info params' )
//			{
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(0).indonesian, info1, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(0).english, info1, FailureHandling.STOP_ON_FAILURE)
//			
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(1).indonesian, info2, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(1).english, info2, FailureHandling.STOP_ON_FAILURE)
//			
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(2).indonesian, info3, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(2).english, info3, FailureHandling.STOP_ON_FAILURE)
//			
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).billAmount, itemPrice1+'.00', FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).billSubCompany, subCompanyInquiryResponse, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).getBillDescription().indonesian, itemName1, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).getBillDescription().english, itemName1, FailureHandling.STOP_ON_FAILURE)
//			
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).billAmount, itemPrice2+'.00', FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).billSubCompany, subCompanyInquiryResponse, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).getBillDescription().indonesian, itemName2, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).getBillDescription().english, itemName2, FailureHandling.STOP_ON_FAILURE)
//			}
//			else if(notes == 'plus info1')
//			{
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(0).indonesian, info1, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(0).english, info1, FailureHandling.STOP_ON_FAILURE)
//			
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).billAmount, itemPrice1+'.00', FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).billSubCompany, subCompanyInquiryResponse, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).getBillDescription().indonesian, itemName1, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).getBillDescription().english, itemName1, FailureHandling.STOP_ON_FAILURE)
//			
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).billAmount, itemPrice2+'.00', FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).billSubCompany, subCompanyInquiryResponse, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).getBillDescription().indonesian, itemName2, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).getBillDescription().english, itemName2, FailureHandling.STOP_ON_FAILURE)
//			}
//			else if(notes == 'plus info2')
//			{
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(0).indonesian, info2, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(0).english, info2, FailureHandling.STOP_ON_FAILURE)
//			
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).billAmount, itemPrice1+'.00', FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).billSubCompany, subCompanyInquiryResponse, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).getBillDescription().indonesian, itemName1, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).getBillDescription().english, itemName1, FailureHandling.STOP_ON_FAILURE)
//			
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).billAmount, itemPrice2+'.00', FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).billSubCompany, subCompanyInquiryResponse, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).getBillDescription().indonesian, itemName2, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).getBillDescription().english, itemName2, FailureHandling.STOP_ON_FAILURE)
//			}
//			else if(notes == 'plus info3')
//			{
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(0).indonesian, info3, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.freeTexts.freeText.get(0).english, info3, FailureHandling.STOP_ON_FAILURE)
//			
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).billAmount, itemPrice1+'.00', FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).billSubCompany, subCompanyInquiryResponse, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).getBillDescription().indonesian, itemName1, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(0).getBillDescription().english, itemName1, FailureHandling.STOP_ON_FAILURE)
//			
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).billAmount, itemPrice2+'.00', FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).billSubCompany, subCompanyInquiryResponse, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).getBillDescription().indonesian, itemName2, FailureHandling.STOP_ON_FAILURE)
//			WS.verifyEqual(responseInq.detailBills.detailBill.get(1).getBillDescription().english, itemName2, FailureHandling.STOP_ON_FAILURE)
//			}
	}
}
// Jika inquiry menggunakan requestId yang sama, maka tidak akan insert ke table inquiry lagi, jadi data inquiry hanya terinsert 1 kali diawal
else
{
	WS.verifyEqual(responseInq.companyCode, fixCompanyCode, FailureHandling.STOP_ON_FAILURE)
	WS.verifyEqual(responseInq.customerNumber, customerNumber, FailureHandling.STOP_ON_FAILURE)
	WS.verifyEqual(responseInq.requestID, requestId_Inquiry, FailureHandling.STOP_ON_FAILURE)
	WS.verifyEqual(responseInq.inquiryStatus, secondinquiryStatus, FailureHandling.STOP_ON_FAILURE)
	WS.verifyEqual(responseInq.inquiryReason.indonesian, secondInquiryReasonIndonesia, FailureHandling.STOP_ON_FAILURE)
	WS.verifyEqual(responseInq.inquiryReason.english, secondInquiryReasonEnglish, FailureHandling.STOP_ON_FAILURE)
	
	if(secondInquiryReasonIndonesia == 'Sukses')
	{
		WS.verifyEqual(responseInq.customerName, verifyCustomerName, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responseInq.currencyCode, currency, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responseInq.totalAmount, fixAmount, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responseInq.subCompany, subCompany, FailureHandling.STOP_ON_FAILURE)
	}
}
}

if(testCase == 'Payment with va number not found')
{
   invalidCustomerNumber = '00000000999'
}

if(testCase == 'Payment with va number expired (expired time from merchant)')
{
	WebUI.delay(62)
}
else if (testCase == 'Payment with va number expired (expired time from DOKU)')
{
	WebUI.delay(122)
}

if(testCase == 'Payment with inquiry status already expired')
{
	CustomKeywords.'com.database.DBHelper.execute'("update inquiry set status ='EXPIRED' where virtual_account_number = '"+fixCompanyCode+customerNumber+"'")
}

'Payment Process'

fixAmountPayment=paidAmountPayment

if(j==1 && diffAmount == 'yes')
{
	fixAmountPayment = '99'
}
else if (paymentFailed == 'yes' && j==0)
{
	fixAmountPayment='99'
} 
else if ((j==1 && testCase == 'Payment with va number already paid but status payment is failed, same Amount, reusable true') || (j==1 && testCase == 'Payment with va number already paid but status payment is failed, same Amount, reusable false')) 
{
	fixAmountPayment = secondPaidAmountPayment
}

trxDateTimePayment = CustomKeywords.'utils.Utils.generateLocalTime2'()

if (j==0 || j==1)
{
	PaymentRequest paymentRequest = new PaymentRequest()
	InquiryProcess paymentProcess = new InquiryProcess()
	
	PaymentFlag paymentFlag = new PaymentFlag()
	
	paymentRequest.setCurrencyCode(currency)
	paymentRequest.setTransactionDate(trxDateTimePayment)
	paymentRequest.setRequestID(requestId)
	paymentRequest.setChannelType(channelType)
	paymentRequest.setCustomerName(customerName)
	paymentRequest.setTotalAmount(totalAmountPayment)
	paymentRequest.setPaidAmount(fixAmountPayment)
	paymentRequest.setReference(referencePayment)
	
	if (testCase == 'Payment flagAdvice null')
	{
		paymentRequest.setFlagAdvice('')
	} 
	else 
	{
		if (j==0)
		{
			paymentRequest.setFlagAdvice(flagAdvice)
		}
		else if (j==1)
		{
			paymentRequest.setFlagAdvice(secondflagAdvice)
		}
	}
	if (testCase== 'Payment with unregistered company code')
	{
		companyCodePayment = '12345'
		paymentRequest.setCompanyCode(companyCodePayment)
	}
	else
	{
		paymentRequest.setCompanyCode(fixCompanyCode)
	}
	
	if(testCase == 'Payment with va number not found') {
		paymentRequest.setCustomerNumber('00000000999')
	}
	else
	{
		paymentRequest.setCustomerNumber(customerNumber)
	}
	
	if (additionalData != '#')
	{
		paymentRequest.setAdditionalData(additionalData)
	}
	
	
	if (subCompany != '#')
	{
		paymentRequest.setSubCompany(subCompany)
	}
	
	responsePay = paymentFlag.getPaymentFlag(paymentRequest, inquiryEndPoint)
	String NULL = 'NULL'
	if(j == 0)
	{
		'Verify Payment Response'
		if (testCase== 'Payment with unregistered company code')
		{
			WS.verifyEqual(responsePay.companyCode, companyCodePayment, FailureHandling.STOP_ON_FAILURE)
		}
		else
		{
			WS.verifyEqual(responsePay.companyCode, fixCompanyCode, FailureHandling.STOP_ON_FAILURE)
		}
		
		if (testCase== 'Payment with va number not found')
		{
			WS.verifyEqual(responsePay.customerNumber, invalidCustomerNumber, FailureHandling.STOP_ON_FAILURE)
		}
		else
		{
			WS.verifyEqual(responsePay.customerNumber, customerNumber, FailureHandling.STOP_ON_FAILURE)
		}
		
		WS.verifyEqual(responsePay.requestID, requestId, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.paymentFlagStatus, paymentFlagStatus, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.paymentFlagReason.indonesian, paymentFlagReasonIndonesia, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.paymentFlagReason.english, paymentFlagReasonEnglish, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.customerName, verifyCustomerName, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.currencyCode, currency, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.transactionDate, trxDateTimePayment, FailureHandling.STOP_ON_FAILURE)
		
		if(notes == 'plus all info params' )
		{
		WS.verifyEqual(responsePay.freeTexts.freeText.get(0).indonesian, info1, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.freeTexts.freeText.get(0).english, info1, FailureHandling.STOP_ON_FAILURE)
		
		WS.verifyEqual(responsePay.freeTexts.freeText.get(1).indonesian, info2, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.freeTexts.freeText.get(1).english, info2, FailureHandling.STOP_ON_FAILURE)
		
		WS.verifyEqual(responsePay.freeTexts.freeText.get(2).indonesian, info3, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.freeTexts.freeText.get(2).english, info3, FailureHandling.STOP_ON_FAILURE)
		
		}
		else if(notes == 'plus info1')
		{
		WS.verifyEqual(responsePay.freeTexts.freeText.get(0).indonesian, info1, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.freeTexts.freeText.get(0).english, info1, FailureHandling.STOP_ON_FAILURE)
		
		}
		else if(notes == 'plus info2')
		{
		WS.verifyEqual(responsePay.freeTexts.freeText.get(0).indonesian, info2, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.freeTexts.freeText.get(0).english, info2, FailureHandling.STOP_ON_FAILURE)
		
		}
		else if(notes == 'plus info3')
		{
		WS.verifyEqual(responsePay.freeTexts.freeText.get(0).indonesian, info3, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.freeTexts.freeText.get(0).english, info3, FailureHandling.STOP_ON_FAILURE)
		}
		
		if(additionalData != '#')
		{
			WS.verifyEqual(responsePay.additionalData, additionalData, FailureHandling.STOP_ON_FAILURE)
		}
			WS.verifyEqual(responsePay.paidAmount, paidAmountPayment, FailureHandling.STOP_ON_FAILURE)
			WS.verifyEqual(responsePay.totalAmount, totalAmountPayment, FailureHandling.STOP_ON_FAILURE)
	}
	else
	{ 
		if(diffRequestIdPayment != 'yes')
		{
			WS.verifyEqual(responsePay.requestID, requestId, FailureHandling.STOP_ON_FAILURE)
		}
		WS.verifyEqual(responsePay.paymentFlagStatus, secondPaymentFlagStatus, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.paymentFlagReason.indonesian, secondPaymentFlagReasonIndonesia, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.paymentFlagReason.english, secondPaymentFlagReasonEnglish, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.customerName, verifyCustomerName, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.currencyCode, currency, FailureHandling.STOP_ON_FAILURE)
		WS.verifyEqual(responsePay.totalAmount, totalAmountPayment, FailureHandling.STOP_ON_FAILURE)
		
		if (returnExistingPayment != 'yes')
		{
			if (diffRequestIdPayment != 'yes')
			{
				WS.verifyEqual(responsePay.transactionDate, trxDateTimePayment, FailureHandling.STOP_ON_FAILURE)
				
				if(amountType == 'close' || amountType == 'open')
				{
					WS.verifyEqual(responsePay.paidAmount, secondPaidAmountPayment, FailureHandling.STOP_ON_FAILURE)
					WS.verifyEqual(responsePay.totalAmount, totalAmountPayment, FailureHandling.STOP_ON_FAILURE)
				}
			}
		}
		else
		{
			if(amountType == 'close' || amountType == 'open')
			{
				WS.verifyEqual(responsePay.paidAmount, paidAmountPayment, FailureHandling.STOP_ON_FAILURE)
				WS.verifyEqual(responsePay.totalAmount, totalAmountPayment, FailureHandling.STOP_ON_FAILURE)
			}
			if(amountType == 'min')
			{
				WS.verifyEqual(responsePay.totalAmount, minAmount, FailureHandling.STOP_ON_FAILURE)
			}
			else if(amountType == 'max')
			{
				WS.verifyEqual(responsePay.totalAmount, maxAmount, FailureHandling.STOP_ON_FAILURE)
			}else if( amountType == 'limit')
			{
				WS.verifyEqual(responsePay.totalAmount, minAmount, FailureHandling.STOP_ON_FAILURE)
			}
		}
	}
}

'Get data Id Inquiry for case = Payment with va number already success paid, same Amount, reusable false , with inquiry'
rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from inquiry where invoice_number = \'' +invoiceNumber) + '\' order by id desc limit 1')
String NULL = 'NULL'
while (rset.next()) {
	idInquiryAsc = rset.getString('id')
	WebUI.comment('id in Inquiry ='+idInquiryAsc)
}

//CustomKeywords.'com.database.DBHelper.connectDBPostgres'()
'Verify Payment Table in db'
rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from payment where invoice_number = \'' +invoiceNumber) + '\' order by id desc limit 1')

while (rset.next()) {
	
	if ((savePaymentDb == 'yes' && j == 0 && rset.getString('id') != null) || (saveSecondPaymentDb == 'yes' && j == 1 && rset.getString('id') != null)) 
	{
		id = rset.getString('id') == null ? NULL : rset.getString('id')
		WebUiBuiltInKeywords.verifyNotMatch(id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		uuid = rset.getString('uuid') == null ? NULL : rset.getString('uuid')
		WebUiBuiltInKeywords.verifyNotMatch(uuid, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			
		WebUI.verifyMatch(rset.getString('client_id'), clientId, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('company_code'), fixCompanyCode, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('customer_number'), customerNumber, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('virtual_account_number'), fixVaNumber, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('invoice_number'), invoiceNumber, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyNotMatch(rset.getString('created_date'), trxDateTimePayment, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('channel_type'), channelType, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyMatch(rset.getString('currency_code'), currency, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		additional_info = rset.getString('additional_info') == null ? NULL : rset.getString('additional_info')
		WebUiBuiltInKeywords.verifyMatch(additional_info, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		transaction_date = rset.getString('transaction_date') == null ? NULL : rset.getString('transaction_date')
		WebUiBuiltInKeywords.verifyNotMatch(transaction_date, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		if (additionalData == "#")
			{
				additional_data = rset.getString('additional_data') == null ? NULL : rset.getString('additional_data')
				WebUiBuiltInKeywords.verifyMatch(additional_data, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)		
			}
		else
			{
				WebUI.verifyMatch(rset.getString('additional_data'), additionalData, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
		
		
		if (subCompany == "#")
			{	
				sub_company = rset.getString('sub_company') == null ? NULL : rset.getString('sub_company')
				WebUiBuiltInKeywords.verifyMatch(sub_company, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
		else
			{
				WebUI.verifyMatch(rset.getString('sub_company'), subCompany, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
		
		if (j == 0)
		{
			WebUI.verifyMatch(rset.getString('state'), statePayment, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('status'), statusPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('paid_amount'), paidAmountPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('total_amount'),totalAmountPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('flag_advice'), flagAdviceDB, false, FailureHandling.CONTINUE_ON_FAILURE)		
			WebUI.verifyMatch(rset.getString('reference'), referencePayment, false, FailureHandling.CONTINUE_ON_FAILURE)
			WebUI.verifyMatch(rset.getString('request_id'), requestId, false, FailureHandling.CONTINUE_ON_FAILURE)
			
			
//			if (variableAmount != '')
//				{
//					WS.verifyEqual(rset.getString('variable_amount'), variableAmount, FailureHandling.STOP_ON_FAILURE)
//				}
//			else
//				{
//					variable_amount = rset.getString('variable_amount') == null ? NULL : rset.getString('variable_amount')
//					WebUiBuiltInKeywords.verifyMatch(variable_amount, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
//				}
			
			if (getIdInquiryDesc == 'yes')
			{
				WebUI.verifyMatch(rset.getString('inquiry_id'), idInquiryAsc, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
			
			if (statusPayment == 'SUCCESS')
				{
					header_request_id = rset.getString('header_request_id') == null ? NULL : rset.getString('header_request_id')
					WebUiBuiltInKeywords.verifyNotMatch(header_request_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					
					header_response_id = rset.getString('header_response_id') == null ? NULL : rset.getString('header_response_id')
					WebUiBuiltInKeywords.verifyNotMatch(header_response_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)	
				}
			else
				{
					header_request_id = rset.getString('header_request_id') == null ? NULL : rset.getString('header_request_id')
					WebUiBuiltInKeywords.verifyMatch(header_request_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					
					header_response_id = rset.getString('header_response_id') == null ? NULL : rset.getString('header_response_id')
					WebUiBuiltInKeywords.verifyMatch(header_response_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
		
			
			if (savePaymentFlagReason == 'yes')
			{
				WebUI.verifyMatch(rset.getString('payment_flag_status'), paymentFlagStatus, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('payment_flag_reason_id'), paymentFlagReasonIndonesia, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('payment_flag_reason_en'), paymentFlagReasonEnglish, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
			else
			{
				paymentFlagStatus = rset.getString('payment_flag_status') == null ? NULL : rset.getString('payment_flag_status')
				WebUiBuiltInKeywords.verifyMatch(paymentFlagStatus, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				paymentFlagReasonId = rset.getString('payment_flag_reason_id') == null ? NULL : rset.getString('payment_flag_reason_id')
				WebUiBuiltInKeywords.verifyMatch(paymentFlagReasonId, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				paymentFlagReasonEn = rset.getString('payment_flag_reason_en') == null ? NULL : rset.getString('payment_flag_reason_en')
				WebUiBuiltInKeywords.verifyMatch(paymentFlagReasonEn, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
			}
		}
		else
		{
			if (secondPaymentFlagReasonEnglish == 'Bill Not Found')
				{
					WebUI.verifyNotMatch(rset.getString('reference'), referencePayment, false, FailureHandling.CONTINUE_ON_FAILURE)
					if (diffTrx == 'yes')
						{	
							WebUI.verifyNotMatch(rset.getString('request_id'), requestId, false, FailureHandling.CONTINUE_ON_FAILURE)
						}
					else
						{
							WebUI.verifyMatch(rset.getString('request_id'), requestId, false, FailureHandling.CONTINUE_ON_FAILURE)
						}
				}
			else
				{	
					WebUI.verifyMatch(rset.getString('reference'), referencePayment, false, FailureHandling.CONTINUE_ON_FAILURE)
					WebUI.verifyMatch(rset.getString('request_id'), requestId, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
			
			
			if(secondTriger != 'without Inquiry' && reusableStatus != 'false' && returnExistingPayment != 'yes'){
				WebUI.verifyMatch(rset.getString('state'), secondStatePayment, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('status'), secondStatusPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('flag_advice'), secondflagAdviceDB, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				if (getIdInquiryDesc == 'yes')
				{
					WebUI.verifyMatch(rset.getString('inquiry_id'), idInquiryAsc, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
				
				if (savePaymentFlagReason == 'yes')
				{
					WebUI.verifyMatch(rset.getString('payment_flag_status'), secondPaymentFlagStatus, false, FailureHandling.CONTINUE_ON_FAILURE)
					WebUI.verifyMatch(rset.getString('payment_flag_reason_id'), secondPaymentFlagReasonIndonesia, false, FailureHandling.CONTINUE_ON_FAILURE)
					WebUI.verifyMatch(rset.getString('payment_flag_reason_en'), secondPaymentFlagReasonEnglish, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
				else
				{
					paymentFlagStatus = rset.getString('payment_flag_status') == null ? NULL : rset.getString('payment_flag_status')
					WebUiBuiltInKeywords.verifyMatch(paymentFlagStatus, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					
					paymentFlagReasonId = rset.getString('payment_flag_reason_id') == null ? NULL : rset.getString('payment_flag_reason_id')
					WebUiBuiltInKeywords.verifyMatch(paymentFlagReasonId, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					
					paymentFlagReasonEn = rset.getString('payment_flag_reason_en') == null ? NULL : rset.getString('payment_flag_reason_en')
					WebUiBuiltInKeywords.verifyMatch(paymentFlagReasonEn, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
				
				if (secondPaymentFlagReasonIndonesia == 'Sukses')
				{
					WebUI.verifyMatch(rset.getString('paid_amount'), secondPaidAmountPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
					WebUI.verifyMatch(rset.getString('total_amount'),totalAmountPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
					
					header_request_id = rset.getString('header_request_id') == null ? NULL : rset.getString('header_request_id')
					WebUiBuiltInKeywords.verifyNotMatch(header_request_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					
					header_response_id = rset.getString('header_response_id') == null ? NULL : rset.getString('header_response_id')
					WebUiBuiltInKeywords.verifyNotMatch(header_response_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
				else
				{
					WebUI.verifyMatch(rset.getString('paid_amount'), fixAmountPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
					WebUI.verifyMatch(rset.getString('total_amount'), totalAmountPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
					
					header_request_id = rset.getString('header_request_id') == null ? NULL : rset.getString('header_request_id')
					WebUiBuiltInKeywords.verifyMatch(header_request_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					
					header_response_id = rset.getString('header_response_id') == null ? NULL : rset.getString('header_response_id')
					WebUiBuiltInKeywords.verifyMatch(header_response_id, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
			}
		}
	}
}


//CustomKeywords.'com.database.DBHelper.connectDBPostgres'()
'Verify Register Table in db'
rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from register where virtual_account_number = \'' +fixVaNumber) + '\' order by id desc limit 1')

while (rset.next()) {

	idRegister = rset.getString('id')
	WebUI.comment('idRegister in Register ='+idRegister)
	
	createdDateRegister = rset.getString('created_date')
	WebUI.comment('created_date in Register ='+createdDateRegister)
	
	expiredDateRegister = rset.getString('expired_date')
	WebUI.comment('expired_date in Register ='+expiredDateRegister)
	
	if (j == 0 )
	{
		WebUI.verifyMatch(rset.getString('payment_number'), paymentNumber, false, FailureHandling.CONTINUE_ON_FAILURE)
	}
	else
	{
		WebUI.verifyMatch(rset.getString('payment_number'), paymentNumber2, false, FailureHandling.CONTINUE_ON_FAILURE)
	}
	
}
if(retry!='yes' || j == 0 ){
'Verify Inquiry Table in db'
rset = CustomKeywords.'com.database.DBHelper.executeQuery'(('select * from inquiry where virtual_account_number = \'' +fixVaNumber) + '\' order by id desc limit 1')

		while (rset.next()) {
			if ((saveInquiryDB == 'yes' && j == 0 && rset.getString('id') != null) || (saveSecondInquiryDB == 'yes' && j == 1 && rset.getString('id') != null))
			{
				WebUiBuiltInKeywords.verifyNotMatch(rset.getString('id'), NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUiBuiltInKeywords.verifyNotMatch(rset.getString('uuid'), NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('company_code'), fixCompanyCode, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('virtual_account_number'), fixVaNumber, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('channel_type'), channelType, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('request_id'), requestId_Inquiry, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('currency_code'), currency, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('customer_number'), customerNumber, false, FailureHandling.CONTINUE_ON_FAILURE)
				createdDateRegister = testCase.contains('direct inquiry') ? NULL : createdDateRegister
				WebUI.verifyNotMatch(rset.getString('created_date'), createdDateRegister, false, FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.verifyMatch(rset.getString('sub_company'), subCompanyInquiryResponse, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				updated_date = rset.getString('updated_date') == null ? NULL : rset.getString('updated_date')
				WebUiBuiltInKeywords.verifyMatch(updated_date, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				if (billingType == 'FIX_BILL' || billingType == 'BILL_VARIABLE_AMOUNT' || billingType == '')
					{
						WebUI.verifyMatch(rset.getString('amount'), fixAmount, false, FailureHandling.CONTINUE_ON_FAILURE)
					}
				else if (billingType == 'NO_BILL')
					{
						expAmount = testCase.contains('direct inquiry (min amount)') || testCase.contains('direct inquiry (max amount)') || testCase.contains('amount is null') ? NULL : '0'
						dbAmount = rset.getString('amount') == null ? NULL : rset.getString('amount')
						WebUI.verifyMatch(expAmount, dbAmount, false, FailureHandling.CONTINUE_ON_FAILURE)
					}
			
		   
				if(amountType == 'limit')
					{
						WebUI.verifyMatch(rset.getString('min_amount'), minAmount, false, FailureHandling.CONTINUE_ON_FAILURE)
						WebUI.verifyMatch(rset.getString('max_amount'), maxAmount, false, FailureHandling.CONTINUE_ON_FAILURE)
					}
				else if(amountType == 'min')
					{
						WebUI.verifyMatch(rset.getString('min_amount'), minAmount, false, FailureHandling.CONTINUE_ON_FAILURE)
					
						max_amount = rset.getString('max_amount') == null ? NULL : rset.getString('max_amount')
						WebUiBuiltInKeywords.verifyMatch(max_amount, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					}
			
				else if (amountType == 'max')
					{
						WebUI.verifyMatch(rset.getString('max_amount'), maxAmount, false, FailureHandling.CONTINUE_ON_FAILURE)
					
						min_amount = rset.getString('min_amount') == null ? NULL : rset.getString('min_amount')
						WebUiBuiltInKeywords.verifyMatch(min_amount, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					}
				else
					{
						min_amount = rset.getString('min_amount') == null ? NULL : rset.getString('min_amount')
						WebUiBuiltInKeywords.verifyMatch(min_amount, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
						
						max_amount = rset.getString('max_amount') == null ? NULL : rset.getString('max_amount')
						WebUiBuiltInKeywords.verifyMatch(max_amount, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					}
				
				
				if (j == 0)
				{
					WebUI.verifyMatch(rset.getString('state'), stateInquiry, false, FailureHandling.CONTINUE_ON_FAILURE)
					WebUI.verifyMatch(rset.getString('status'), statusInquiry, false, FailureHandling.CONTINUE_ON_FAILURE)
					WebUI.verifyMatch(rset.getString('inquiry_status'), inquiryStatus, false, FailureHandling.CONTINUE_ON_FAILURE)
					WebUI.verifyMatch(rset.getString('inquiry_reason_id'), inquiryReasonIndonesia, false, FailureHandling.CONTINUE_ON_FAILURE)
					WebUI.verifyMatch(rset.getString('inquiry_reason_en'), inquiryReasonEnglish, false, FailureHandling.CONTINUE_ON_FAILURE)
					
				}
				else
				{
					if(secondTriger != 'without Inquiry'){
						WebUI.verifyMatch(rset.getString('state'), secondStateInquiry, false, FailureHandling.CONTINUE_ON_FAILURE)
						WebUI.verifyMatch(rset.getString('status'), secondStatusInquiry, false, FailureHandling.CONTINUE_ON_FAILURE)
						WebUI.verifyMatch(rset.getString('inquiry_status'), secondinquiryStatus, false, FailureHandling.CONTINUE_ON_FAILURE)
						WebUI.verifyMatch(rset.getString('inquiry_reason_id'), secondInquiryReasonIndonesia, false, FailureHandling.CONTINUE_ON_FAILURE)
						WebUI.verifyMatch(rset.getString('inquiry_reason_en'), secondInquiryReasonEnglish, false, FailureHandling.CONTINUE_ON_FAILURE)
					}
				}
					
				
				if (testCase == 'Success payment with mandatory and optional params')
				{
					if(notes == 'plus all info params' )
					{
						WebUI.verifyMatch(rset.getString('info1'), info1, false, FailureHandling.CONTINUE_ON_FAILURE)
						WebUI.verifyMatch(rset.getString('info2'), info2, false, FailureHandling.CONTINUE_ON_FAILURE)
						WebUI.verifyMatch(rset.getString('info3'), info3, false, FailureHandling.CONTINUE_ON_FAILURE)
					}
					else if(notes == 'plus info1')
					{
						WebUI.verifyMatch(rset.getString('info1'), info1, false, FailureHandling.CONTINUE_ON_FAILURE)
						
						info2 = rset.getString('info2') == null ? NULL : rset.getString('info2')
						WebUiBuiltInKeywords.verifyMatch(info2, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
						
						info3 = rset.getString('info3') == null ? NULL : rset.getString('info3')
						WebUiBuiltInKeywords.verifyMatch(info3, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					}
					else if(notes == 'plus info2')
					{
						WebUI.verifyMatch(rset.getString('info2'), info2, false, FailureHandling.CONTINUE_ON_FAILURE)
						
						info1 = rset.getString('info1') == null ? NULL : rset.getString('info1')
						WebUiBuiltInKeywords.verifyMatch(info1, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
						
						info3 = rset.getString('info3') == null ? NULL : rset.getString('info3')
						WebUiBuiltInKeywords.verifyMatch(info3, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					}
					else if(notes == 'plus info3')
					{
						WebUI.verifyMatch(rset.getString('info3'), info3, false, FailureHandling.CONTINUE_ON_FAILURE)
						
						info1 = rset.getString('info1') == null ? NULL : rset.getString('info1')
						WebUiBuiltInKeywords.verifyMatch(info1, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
						
						info2 = rset.getString('info2') == null ? NULL : rset.getString('info2')
						WebUiBuiltInKeywords.verifyMatch(info2, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
					}
				}
			}
		}
	}
}
if (testCase == 'Payment with va number expired (expired time from DOKU)')
{
	CustomKeywords.'com.database.DBHelper.execute'((('update merchant set expired_time = \'120\' where client_id = \'') + clientId) + '\'')
}

CustomKeywords.'com.database.DBHelper.execute'((('update identifier set status = \'ACTIVE\' where client_id = \'') + clientId) + '\'')
CustomKeywords.'com.database.DBHelper.execute'('update identifier set name = \'PREFIX_MERCHANT\' where id in (52)')
CustomKeywords.'com.database.DBHelper.execute'('update identifier set value = \'DOKUMerc\' where id = \'52\'')
CustomKeywords.'com.database.DBHelper.execute'((('update merchant set encrypt_status = \'f\' where client_id = \'') + clientId) + '\'')