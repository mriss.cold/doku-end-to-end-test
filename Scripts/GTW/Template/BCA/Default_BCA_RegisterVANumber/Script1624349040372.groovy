import org.json.JSONObject
import org.json.JSONArray as JSONArray
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

def containerEnv = System.getenv("CONTAINER_ENV");
print("CONTAINER_ENV : "+containerEnv);

endpoint = GlobalVariable.hostNameSIT+ requestTarget
howToPayAPI = GlobalVariable.hostNameSIT+"/bca-virtual-account/v2/how-to-pay-api/"
howToPayPage = GlobalVariable.hostNameSITHowToPayPage+"/how-to-pay/v2/bca-virtual-account/"
//if (containerEnv != null)
//{
//  endpoint =  GlobalVariable.hostNameDevBCA+ endPoint
//  howToPayAPI = GlobalVariable.hostNameDevHowToPayApiBCA+"bca-virtual-account/v2/how-to-pay-api/"
//  howToPayPage = GlobalVariable.hostNameDevGetPayPageBCA+"how-to-pay/v2/bca-virtual-account/"
//}
WebUI.comment(endpoint)
WebUI.comment(howToPayAPI)
WebUI.comment(howToPayPage)

'Register Process'
JSONObject parameters = new JSONObject()
JSONObject orderData = new JSONObject()
JSONObject vaData = new JSONObject()
JSONObject customerData = new JSONObject()
JSONObject additionalInfoData = new JSONObject()
JSONObject integrationData = new JSONObject()


'========Set Order Data========'
orderData.put('invoice_number',invoiceNumber);
orderData.put('amount',amount);
orderData.put('max_amount',maxAmount);
orderData.put('min_amount',minAmount);
if(invoiceNumber.equals("")) {
	orderData.remove("invoice_number");
}
if(amount.equals("")) {
	orderData.remove("amount");
}
if(maxAmount.equals("")) {
	orderData.remove("max_amount");
}
if(minAmount.equals("")) {
	orderData.remove("min_amount");
}
parameters.put('order', orderData);

'========Set Virtual Account Data========'
vaData.put('virtual_account_number',vaNumber);
vaData.put('expired_time',expiredTime);
vaData.put('reusable_status',reusableStatus);
vaData.put('info1',info1);
vaData.put('info2',info2);
vaData.put('info3',info3);
vaData.put('billing_type',billingType);

if(vaNumber.equals("")) {
	vaData.remove("virtual_account_number");
}
if(expiredTime.equals("")) {
	vaData.remove("expired_time");
}
if(reusableStatus.equals("")) {
	vaData.remove("reusable_status");
}

if(info1.equals("")) {
	vaData.remove("info1");
}
if(info2.equals("")) {
	vaData.remove("info2");
}
if(info3.equals("")) {
	vaData.remove("info3");
}

if(billingType.equals("")) {
	vaData.remove("billing_type");
}
parameters.put('virtual_account_info', vaData);

'========Set Customer Data========'
customerData.put('name',customerName);
customerData.put('email',customerEmail);
if(customerName.equals("")) {
	customerData.remove("name");
}
if(customerEmail.equals("")) {
	customerData.remove("email");
}
if(!testCase.contains('without object customer')){
	parameters.put('customer', customerData);
}
'========Set Additional Info Data========'
JSONArray settlement = new JSONArray()
settlementData1 = new LinkedHashMap(2);
settlementData1.put("bank_account_settlement_id", bankAccountSettlementId1);
settlementData1.put("type", type1);
settlementData1.put("value", value1);

if(bankAccountSettlementId1.equals("")) {
	settlementData1.remove("bank_account_settlement_id");
}
if(type1.equals("")) {
	settlementData1.remove("type");
}
if(value1.equals("")) {
	settlementData1.remove("value");
}

settlement.put(settlementData1);

settlementData2 = new LinkedHashMap(2);
settlementData2.put("bank_account_settlement_id", bankAccountSettlementId2);
settlementData2.put("type", type2);
settlementData2.put("value", value2);

if(bankAccountSettlementId2.equals("")) {
	settlementData2.remove("bank_account_settlement_id");
}
if(type2.equals("")) {
	settlementData2.remove("type");
}
if(value2.equals("")) {
	settlementData2.remove("value");
}
settlement.put(settlementData2);

additionalInfoData.put('settlement',settlement);

if(bankAccountSettlementId2.equals("") && type2.equals("") && value2.equals("") && bankAccountSettlementId1.equals("") && type1.equals("") && value1.equals("")) {
	additionalInfoData.remove("settlement");
}

integrationData.put('plugin',plugin);

additionalInfoData.put('integration',integrationData);
if(plugin.equals("")) {
	additionalInfoData.remove("integration");
}

parameters.put('additional_info', additionalInfoData);

if (plugin.equals("wrongData")) {
	parameters.put('additional_info', 'wrongData');
}

if(removeAdditionalInfo == 'yes') {
	parameters.remove("additional_info");
}

restParameters = parameters.toString()
WebUI.comment('Json Request Body = '+restParameters)



'Convert Json Request Body To Sha 256 Base 64'
base64JsonBody = CustomKeywords.'utils.Utils.generateSha256Base64Hash'(restParameters)
if (testCase == 'Invalid digest value')
 {
   base64JsonBody = CustomKeywords.'utils.Utils.generateWordsSHA256'(restParameters)
 }
WebUI.comment('Base 64 Json Body = '+base64JsonBody)


'Convert Request Header To HmacSha 256 Base 64'
clientIdRequest = 'Client-Id:'+ clientId
requestIdRequest = 'Request-Id:'+ requestId
requestTimestampRequest = 'Request-Timestamp:' + requestTimestamp
requestTargetRequest = 'Request-Target:' + requestTarget
digestRequest = 'Digest:'+base64JsonBody
signatureFormat = 'HMACSHA256='

componentDataHmac = clientIdRequest +'\n'+ requestIdRequest +'\n'+ requestTimestampRequest +'\n'+ requestTargetRequest +'\n'+ digestRequest
if (testCase == 'Invalid signature')
{
	componentDataHmac = clientIdRequest + requestIdRequest + requestTimestampRequest + digestRequest
}

WebUI.comment('Component Data HmacSha 256  = '+componentDataHmac)
String secretKey = sharedKey;

merchantSignature = CustomKeywords.'utils.Utils.generatHmacSha256Base64Hash'(componentDataHmac, secretKey)
if (testCase == 'Invalid signature format name')
{	
	signatureFormat = 'SHA256='
}
WebUI.comment('Signature HmacSha 256 = '+merchantSignature)

'Set Header Request'
String authHeader = "whateverYouNeedForAuthentication"
String signatureEncodeValue = signatureFormat+merchantSignature

GlobalVariable.G_signatureEncodeValueRequest = signatureEncodeValue
 
TestObjectProperty header1 = new TestObjectProperty("Authorization", ConditionType.EQUALS, authHeader)
TestObjectProperty header2 = new TestObjectProperty("Content-Type", ConditionType.EQUALS, "application/json")
TestObjectProperty header3 = new TestObjectProperty("Accept", ConditionType.EQUALS, "application/json")
TestObjectProperty header4 = new TestObjectProperty("Client-Id", ConditionType.EQUALS, clientId)
TestObjectProperty header5 = new TestObjectProperty("Request-Id", ConditionType.EQUALS, requestId)
TestObjectProperty header6 = new TestObjectProperty("Request-Timestamp", ConditionType.EQUALS, requestTimestamp)
TestObjectProperty header7 = new TestObjectProperty("Signature", ConditionType.EQUALS, signatureEncodeValue)
ArrayList defaultHeaders = Arrays.asList(header1, header2, header3, header4, header5, header6, header7)

'Register Request'
WebUI.comment(restParameters)

RequestObject ro = new RequestObject("objectId")
ro.setRestUrl(endpoint)
ro.setHttpHeaderProperties(defaultHeaders)
ro.setRestRequestMethod("POST")
ro.setBodyContent(new HttpTextBodyContent(restParameters))
 
ResponseObject respObj = WS.sendRequest(ro)
respObj.setContentType('application/json')


//'Log Request'
//if (containerEnv == null) {
//	logEndpoint = 'http://localhost:5000/api/users'
//	JSONObject logReqParameters = new JSONObject()
//	JSONObject logResParameters = new JSONObject()
//	
//	logReqParameters.put('testcase', no+". "+testCase)
//	logReqParameters.put('selector', "req")
//	logReqParameters.put('data', parameters)
//	restLogReq = logReqParameters.toString()
//	
//	RequestObject logger = new RequestObject("objectId")
//	logger.setRestUrl(logEndpoint)
//	logger.setHttpHeaderProperties(defaultHeaders)
//	logger.setRestRequestMethod("POST")
//	logger.setBodyContent(new HttpTextBodyContent(restLogReq))
//	
//	WS.sendRequest(logger)
//	
//	JsonSlurper initSlurper = new JsonSlurper()
//	
//	Map dataRes = initSlurper.parseText(respObj.getResponseText())
//	logResParameters.put('selector', "res")
//	logResParameters.put('data', dataRes)
//	restLogRes = logResParameters.toString()
//	
//	RequestObject loggerAsResp = new RequestObject("objectId")
//	loggerAsResp.setRestUrl(logEndpoint)
//	loggerAsResp.setHttpHeaderProperties(defaultHeaders)
//	loggerAsResp.setRestRequestMethod("POST")
//	loggerAsResp.setBodyContent(new HttpTextBodyContent(restLogRes))
//	WS.sendRequest(loggerAsResp)
//}



'Register Response'
WebUI.comment(respObj.getResponseText())
responseBody = respObj.getResponseText().toString()

WebUI.comment('Json Response Body = '+responseBody)

JsonSlurper slurper = new JsonSlurper()
Map parsedJson = slurper.parseText(respObj.getResponseText())

statusCode = respObj.getStatusCode()
WebUI.verifyEqual(statusCode, expectedStatusCode)

if (fixVerifyInvoiceNumber == 'yes')
	{
			invoice_number = parsedJson.order.invoice_number
			WebUiBuiltInKeywords.verifyMatch(invoice_number, invoiceNumber, false, FailureHandling.CONTINUE_ON_FAILURE)	
	}

if (fixVerifyVaNumber == 'yes')
	{
		
			va_number = parsedJson.virtual_account_info.virtual_account_number
			WebUiBuiltInKeywords.verifyMatch(va_number, vaNumber, false, FailureHandling.CONTINUE_ON_FAILURE)	
	}

	
if (verifySignature == 'yes')
	{verifySignature
		'Convert Json Response Body To Sha 256 Base 64'
		String base64JsonResponseBody = CustomKeywords.'utils.Utils.generateSha256Base64Hash'(responseBody)
		WebUI.comment('Base 64 Json Body = '+base64JsonResponseBody)
		
		
		Map headers = respObj.getHeaderFields()
		String clientIdHeader = headers["Client-Id"]
		String requesIdHeader = headers["Request-Id"]
		String responseTimestampsHeader = headers["Response-Timestamp"]
		//String requestTargetHeader = headers["Request-Target"]
		String signatureHeader = headers["Signature"]
		
		WebUI.comment('clientIdHeader = '+clientIdHeader)
		WebUI.comment('requesIdHeader = '+requesIdHeader)
		WebUI.comment('responseTimestampsHeader = '+responseTimestampsHeader)
		//WebUI.comment('requestTargetHeader = '+requestTargetHeader)
		
		clientIdHeader = clientIdHeader.replace('[', '')
		clientIdHeader = clientIdHeader.replace(']', '')
		
		requesIdHeader = requesIdHeader.replace('[', '')
		requesIdHeader = requesIdHeader.replace(']', '')
		
		responseTimestampsHeader = responseTimestampsHeader.replace('[', '')
		responseTimestampsHeader = responseTimestampsHeader.replace(']', '')
		
		GlobalVariable.G_responseTimestampsHeader = responseTimestampsHeader
		
		signatureHeader = signatureHeader.replace('[', '')
		signatureHeader = signatureHeader.replace(']', '')
		
		//requestTargetHeader = requestTargetHeader.replace('[', '')
		//requestTargetHeader = requestTargetHeader.replace(']', '')
		
		fixSharedKey = sharedKey
		WebUI.comment('shared key = '+fixSharedKey)
		
		
		WebUI.verifyEqual(clientIdHeader, clientId)

		
		WebUI.verifyEqual(requesIdHeader, requestId)
		//WebUI.verifyEqual(requestTargetHeader, requestTarget)
		
		
		'Convert String Json Response Body To HmacSha 256 Base 64'
		clientIdResponse = 'Client-Id:'+ clientIdHeader
		requestIdResponse = 'Request-Id:'+ requesIdHeader
		responseTimestamp = 'Response-Timestamp:' + responseTimestampsHeader
		requestTargetResponse = 'Request-Target:' + requestTarget
		DigestResponse = 'Digest:'+base64JsonResponseBody
		
		String componentDataHmacResponse = clientIdResponse +'\n'+ requestIdResponse +'\n'+ responseTimestamp +'\n'+ requestTargetResponse +'\n'+ DigestResponse
		WebUI.comment('Component Data Hmac Response = '+componentDataHmacResponse)
		String secretKeyResponse = sharedKey;
		
		merchantSignatureResponse = CustomKeywords.'utils.Utils.generatHmacSha256Base64Hash'(componentDataHmacResponse, secretKeyResponse)
		WebUI.comment('Signature Hmac Sha256 Response = '+merchantSignatureResponse)
		
		fixMerchantSignatureResponse = "HMACSHA256="+merchantSignatureResponse
		
		GlobalVariable.G_signatureEncodeValueResponse = fixMerchantSignatureResponse
		
		WebUI.verifyEqual(signatureHeader, fixMerchantSignatureResponse)
		
		GlobalVariable.G_paymentCode = parsedJson.virtual_account_info.virtual_account_number
		GlobalVariable.G_howToPayPage = parsedJson.virtual_account_info.how_to_pay_page
		GlobalVariable.G_howToPayAPI = parsedJson.virtual_account_info.how_to_pay_api
		createdDate = parsedJson.virtual_account_info.created_date
		expiredDate = parsedJson.virtual_account_info.expired_date
		createdDateUTC = parsedJson.virtual_account_info.created_date_utc
		expiredDateUTC = parsedJson.virtual_account_info.expired_date_utc
			
		String signedHowToPay = parsedJson.virtual_account_info.virtual_account_number
		String componentSign = signedHowToPay
		String postFix = merchantSignatureResponse = CustomKeywords.'utils.Utils.generatHmacSha256Base64Hash'(componentSign, sharedKey)
		String newPostFix = CustomKeywords.'utils.Utils.getSafeUrl'(componentSign, sharedKey)
		
		verifyHowToPayPage = howToPayPage + GlobalVariable.G_paymentCode + '/' + newPostFix
		verifyHowToPayAPI = howToPayAPI + GlobalVariable.G_paymentCode + '/' + newPostFix
		
		if(result == 'success'){
		String NULL = 'NULL'
		
		GlobalVariable.G_paymentCode = GlobalVariable.G_paymentCode == null ? NULL : GlobalVariable.G_paymentCode
		WebUiBuiltInKeywords.verifyNotMatch(GlobalVariable.G_paymentCode, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		WebUiBuiltInKeywords.verifyMatch(GlobalVariable.G_howToPayPage, verifyHowToPayPage, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUiBuiltInKeywords.verifyMatch(GlobalVariable.G_howToPayAPI, verifyHowToPayAPI, false, FailureHandling.CONTINUE_ON_FAILURE)

		createdDate = createdDate == null ? NULL : createdDate
		WebUiBuiltInKeywords.verifyNotMatch(createdDate, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		expiredDate = expiredDate == null ? NULL : expiredDate
		WebUiBuiltInKeywords.verifyNotMatch(expiredDate, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		createdDateUTC = createdDateUTC == null ? NULL : createdDateUTC
		WebUiBuiltInKeywords.verifyNotMatch(createdDateUTC, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		
		expiredDateUTC = expiredDateUTC == null ? NULL : expiredDateUTC
		WebUiBuiltInKeywords.verifyNotMatch(expiredDateUTC, NULL, false, FailureHandling.CONTINUE_ON_FAILURE)
		  
	} else {
	//	type = parsedJson.error.type
	//	code = parsedJson.error.code
		message = parsedJson.error.message
	
	//	WebUiBuiltInKeywords.verifyMatch(type, errorType, false, FailureHandling.CONTINUE_ON_FAILURE)
	//	WebUiBuiltInKeywords.verifyMatch(code, errorCode, false, FailureHandling.CONTINUE_ON_FAILURE)
		WebUiBuiltInKeywords.verifyMatch(message, errorMessage, false, FailureHandling.CONTINUE_ON_FAILURE)
	}
}