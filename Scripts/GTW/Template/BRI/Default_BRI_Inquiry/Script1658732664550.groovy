import org.json.JSONObject
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil

def containerEnv = System.getenv("CONTAINER_ENV");
print("CONTAINER_ENV : "+containerEnv);

endpoint = GlobalVariable.hostNameSIT+"/bri-virtual-account/VABriInquiry"
//if (containerEnv != null)
//{
//  endpoint =  GlobalVariable.hostNameDev+"bri-virtual-account/VABriInquiry"
//}
WebUI.comment(endpoint)

'Inquiry Process'
String authHeader = "whateverYouNeedForAuthentication"
 
TestObjectProperty header1 = new TestObjectProperty("Authorization", ConditionType.EQUALS, authHeader)
TestObjectProperty header2 = new TestObjectProperty("Content-Type", ConditionType.EQUALS, "application/json")
TestObjectProperty header3 = new TestObjectProperty("Accept", ConditionType.EQUALS, "application/json")
ArrayList defaultHeaders = Arrays.asList(header1, header2, header3)

JSONObject parameters = new JSONObject()

parameters.put('IdApp',idApp);
parameters.put('PassApp',passApp);
parameters.put('TransmisiDateTime',transmisiDateTime);
parameters.put('BankID',bankId);
parameters.put('TerminalID',terminalId);
parameters.put('BrivaNum',brivaNum);
if(idApp.equals("")) {
	parameters.remove("IdApp");
}
if(passApp.equals("")) {
	parameters.remove("PassApp");
}
if(transmisiDateTime.equals("")) {
	parameters.remove("TransmisiDateTime");
}
if(bankId.equals("")) {
	parameters.remove("BankID");
}
if(terminalId.equals("")) {
	parameters.remove("TerminalID");
}
if(brivaNum.equals("")) {
	parameters.remove("BrivaNum");
}

restParameters = parameters.toString()

'Inquiry Request'
WebUI.comment(restParameters)

RequestObject ro = new RequestObject("objectId")
ro.setRestUrl(endpoint)
ro.setHttpHeaderProperties(defaultHeaders)
ro.setRestRequestMethod("POST")
ro.setBodyContent(new HttpTextBodyContent(restParameters))
 
ResponseObject respObj = WS.sendRequest(ro)
respObj.setContentType('application/json')

'Inquiry Response'
WebUI.comment('Response Body Inquiry = '+respObj.getResponseText())

statusCode = respObj.getStatusCode()
WebUI.verifyEqual(statusCode, expectedStatusCode)

JsonSlurper slurper = new JsonSlurper()
Map parsedJson = slurper.parseText(respObj.getResponseBodyContent())
WebUI.comment('statusCode = '+ statusCode)
//if(statusCode == 200){
if (billingType == 'FIX_BILL')
	{
		WebUiBuiltInKeywords.verifyMatch(parsedJson.BillDetail.BillAmount, verifyAmount, false, FailureHandling.CONTINUE_ON_FAILURE)
	}
else if (billingType == 'NO_BILL')
	{
		WebUiBuiltInKeywords.verifyMatch(parsedJson.BillDetail.BillAmount, '0', false, FailureHandling.CONTINUE_ON_FAILURE)
	}

WebUiBuiltInKeywords.verifyMatch(parsedJson.BillDetail.BillName, verifyCustomerName, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.BillDetail.BrivaNum, brivaNum, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.StatusBill, statusBill, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.Currency, currency, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.Info1, verifyInfo1, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.Info2, verifyInfo2, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.Info3, verifyInfo3, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.Info4, verifyInfo4, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.Info5, verifyInfo5, false, FailureHandling.CONTINUE_ON_FAILURE)
//}
