import org.json.JSONObject
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil

def containerEnv = System.getenv("CONTAINER_ENV");
print("CONTAINER_ENV : "+containerEnv);

endpoint = GlobalVariable.hostNameSIT+"/bri-virtual-account/VABriPayment"
//if (containerEnv != null)
//{
//  endpoint =  GlobalVariable.hostNameDev+"bri-virtual-account/VABriPayment"
//}
WebUI.comment(endpoint)

'Payment Process'
String authHeader = "whateverYouNeedForAuthentication"
 
TestObjectProperty header1 = new TestObjectProperty("Authorization", ConditionType.EQUALS, authHeader)
TestObjectProperty header2 = new TestObjectProperty("Content-Type", ConditionType.EQUALS, "application/json")
TestObjectProperty header3 = new TestObjectProperty("Accept", ConditionType.EQUALS, "application/json")
ArrayList defaultHeaders = Arrays.asList(header1, header2, header3)

JSONObject parameters = new JSONObject()

parameters.put('IdApp',idApp);
parameters.put('PassApp',passApp);
parameters.put('TransaksiDateTime',transaksiDateTime);
parameters.put('TransmisiDateTime',transmisiDateTime);
parameters.put('BankID',bankId);
parameters.put('TerminalID',terminalId);
parameters.put('BrivaNum',brivaNum);
parameters.put('PaymentAmount',paymentAmount);
parameters.put('TransaksiID',transaksiId);
if(idApp.equals("")) {
	parameters.remove("IdApp");
}
if(passApp.equals("")) {
	parameters.remove("PassApp");
}
if(transaksiDateTime.equals("")) {
	parameters.remove("TransaksiDateTime");
}
if(transmisiDateTime.equals("")) {
	parameters.remove("TransmisiDateTime");
}
if(bankId.equals("")) {
	parameters.remove("BankID");
}
if(terminalId.equals("")) {
	parameters.remove("TerminalID");
}
if(brivaNum.equals("")) {
	parameters.remove("BrivaNum");
}
if(paymentAmount.equals("")) {
	parameters.remove("PaymentAmount");
}
if(transaksiId.equals("")) {
	parameters.remove("TransaksiID");
}

restParameters = parameters.toString()

'Payment Request'
WebUI.comment(restParameters)

RequestObject ro = new RequestObject("objectId")
ro.setRestUrl(endpoint)
ro.setHttpHeaderProperties(defaultHeaders)
ro.setRestRequestMethod("POST")
ro.setBodyContent(new HttpTextBodyContent(restParameters))
 
ResponseObject respObj = WS.sendRequest(ro)
respObj.setContentType('application/json')

'Payment Response'
WebUI.comment('Response Body Payment = '+respObj.getResponseText())

statusCode = respObj.getStatusCode()
WebUI.verifyEqual(statusCode, expectedStatusCode)

JsonSlurper slurper = new JsonSlurper()
Map parsedJson = slurper.parseText(respObj.getResponseBodyContent())


WebUiBuiltInKeywords.verifyMatch(parsedJson.StatusPayment.ErrorCode, errorCodePayment, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.StatusPayment.ErrorDesc, errorDescPayment, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.StatusPayment.IsError, isError, false, FailureHandling.CONTINUE_ON_FAILURE)

WebUiBuiltInKeywords.verifyMatch(parsedJson.Info1, verifyInfo1, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.Info2, verifyInfo2, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.Info3, verifyInfo3, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.Info4, verifyInfo4, false, FailureHandling.CONTINUE_ON_FAILURE)
WebUiBuiltInKeywords.verifyMatch(parsedJson.Info5, verifyInfo5, false, FailureHandling.CONTINUE_ON_FAILURE)
