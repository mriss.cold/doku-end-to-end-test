<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS001-EndToEndE-Invoice</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3f433463-2439-415e-a06c-7d4adb7aaba4</testSuiteGuid>
   <testCaseLink>
      <guid>5b71a31f-fb24-4e36-9e85-45e59fc00657</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MPE/TC001-PaymentWithVABRI</testCaseId>
      <usingDataBindingAtTestSuiteLevel>false</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>16650671-9587-4322-9763-c5e849acd9a8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>daabf235-313a-4cb1-8006-9856510742ed</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3d188de5-eb37-41ea-93f6-a785b8ffecb5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MPE/TC002-PaymentWithVACIMB</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c7ff5ee2-1ae3-4237-aa2b-e87ad6de1101</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8d0885f0-f400-4b72-addf-f2ac0c3319e5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2f389942-04ed-4e28-9f76-227e8c019fde</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MPE/TC003-PaymentWithVADanamon</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bee76c4a-bb79-4aef-90f0-1d601f49352a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b1c00654-fa1f-4bec-b3dc-37726811c69a</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
