<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ProspectListTs</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c7c0e1cc-c60d-4586-a180-095c008d7db8</testSuiteGuid>
   <testCaseLink>
      <guid>eb517b5a-7f6f-4cfc-bd81-6ef806ca3eb1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep00Tc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6c7893b1-b409-4807-a71d-251ab6cb0c19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep02Tc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f6a2976f-0865-443b-86dd-f21526bc7cd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep03Tc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4a1fd271-a201-4115-b03e-0bb1f7a492c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep04CorporateTc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6326f31c-0803-471e-9620-691c0df98911</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep04PersonalTc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7590e05d-8e28-44a7-a4e7-7b4697a7a987</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep05CorporateTc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1401151e-042b-4958-8703-668263b3e597</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep06PersonalTc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8475de40-b26d-4fce-a17a-be7b888bf3ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep07CorporateTc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a198ad49-8b1c-4f76-bdbb-55bd76b7b228</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep07PersonalTc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>758e08a3-bee4-4d0d-91c8-f2bc2ca1bffb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep08CorporateTc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>63be9dd1-fb85-4f51-9b0e-0514cef36501</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep09CorporateTc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>526413f1-bcf6-4cea-8973-6c676eab44ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep09PersonalTc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>781b72fe-a684-473e-9f71-288ecde91f8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyNewProspectConsumedSuccessfullyUntilStep10PersonalTc</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9f98b245-820b-4815-812f-8cc4ff4b2a1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SE/prospect_list/VerifyFeatureAssignSalesIsWorking</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
