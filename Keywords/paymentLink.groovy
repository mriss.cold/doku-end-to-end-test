import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By as By
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor

import internal.GlobalVariable

public class paymentLink {

	@Keyword
	def filteringPending() {

		WebUI.refresh()
		WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - iframe'), 30)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR001 - FilterPending'))
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR002 - ButtonFilterSearch'))
		WebDriver driver = DriverFactory.getWebDriver()
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR003 - GetNumberInvoice'))
		WebUI.delay(5)
		def getURLCheckout = WebUI.getText(findTestObject('Object Repository/MPE X NCPE/OR004 - GetURLCheckout'))
		println(getURLCheckout)
		WebUI.delay(3)
		JavascriptExecutor js = ((driver) as JavascriptExecutor)
		js.executeScript('window.open();', [])
		int currentTab1 = WebUI.getWindowIndex()
		WebUI.switchToWindowIndex(currentTab1 + 1)
		WebUI.takeScreenshot()
		WebUI.navigateToUrl(getURLCheckout)
	}


	@Keyword
	def paymentwithVACIMB(String email, String password) {

		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR005 - ChooseOptionVA'))
		WebUI.takeScreenshot()
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR014 - ChooseVACIMB'))
		def getNoVA = WebUI.getText(findTestObject('Object Repository/MPE X NCPE/OR008 - NoVA'))
		println getNoVA
		WebUI.takeScreenshot()

		WebUI.callTestCase(findTestCase('CPE/TC-Login Mercant'), [('email') : email, ('password') : password, ('status') : 'Success'],
		FailureHandling.STOP_ON_FAILURE)

		WebUI.navigateToUrl("https://app-uat.doku.com/bo/integration/simulator")
		WebUI.maximizeWindow()
		WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - iframe'), 30)

		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR015 - ChooseSimulatorCIMB'))
		WebUI.setText(findTestObject('Object Repository/MPE X NCPE/OR010 - InputNoVA'), getNoVA)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR011 - ButtonSubmitVA'))
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR008 - ButtonPayVA'))
		WebUI.delay(5)
		WebUI.takeScreenshot()
		WebUI.closeBrowser()
	}

	@Keyword
	def backToPL(String email, String password) {

		WebUI.callTestCase(findTestCase('CPE/TC-Login Mercant'), [('email') : email, ('password') : password, ('status') : 'Success'],
		FailureHandling.STOP_ON_FAILURE)
		WebUI.navigateToUrl(GlobalVariable.url_paymentLink)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/button_Success_dropdownBasic1'))
		WebUI.delay(5)
		WebUI.takeScreenshot()
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/button_Export to PDF'))
		WebUI.delay(5)
		WebUI.takeScreenshot()
		WebUI.closeBrowser()
	}

	@Keyword
	def paymentWithVADanamon(String email, String password) {

		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR005 - ChooseOptionVA'))
		WebUI.takeScreenshot()
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR016 - ChooseVADanamon'))
		def getNoVA = WebUI.getText(findTestObject('Object Repository/MPE X NCPE/OR008 - NoVA'))
		println getNoVA
		WebUI.takeScreenshot()

		WebUI.callTestCase(findTestCase('CPE/TC-Login Mercant'), [('email') : email, ('password') : password, ('status') : 'Success'],
		FailureHandling.STOP_ON_FAILURE)

		WebUI.navigateToUrl("https://app-uat.doku.com/bo/integration/simulator")
		WebUI.maximizeWindow()
		WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - iframe'), 30)

		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR017 - ChooseSimulatorDanamon'))
		WebUI.setText(findTestObject('Object Repository/MPE X NCPE/OR010 - InputNoVA'), getNoVA)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR011 - ButtonSubmitVA'))
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR008 - ButtonPayVA'))
		WebUI.delay(5)
		WebUI.takeScreenshot()
		WebUI.closeBrowser()
	}

	@Keyword

	def paymentWithVAPermata() {

		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR005 - ChooseOptionVA'))
		WebUI.takeScreenshot()
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR012 - ChooseVAPermata'))

		def getNoVA = WebUI.getText(findTestObject('Object Repository/MPE X NCPE/OR008 - NoVA'))
		println getNoVA
		WebUI.takeScreenshot()

		WebUI.callTestCase(findTestCase('CPE/TC-Login Mercant'), [('email') : email, ('password') : password, ('status') : 'Success'],
		FailureHandling.STOP_ON_FAILURE)

		WebUI.navigateToUrl("https://app-uat.doku.com/bo/integration/simulator")
		WebUI.maximizeWindow()
		WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - iframe'), 30)

		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR014 - ChooseSimulatorPermata'))
		WebUI.setText(findTestObject('Object Repository/MPE X NCPE/OR010 - InputNoVA'), getNoVA)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR011 - ButtonSubmitVA'))
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR008 - ButtonPayVA'))
		WebUI.delay(5)
		WebUI.takeScreenshot()
		WebUI.closeBrowser()
	}


	@Keyword

	def paymentWithVAMandiri() {

		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR005 - ChooseOptionVA'))
		WebUI.takeScreenshot()
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR018 - ChooseVAMandiri'))

		def getNoVA = WebUI.getText(findTestObject('Object Repository/MPE X NCPE/OR008 - NoVA'))
		println getNoVA
		WebUI.takeScreenshot()

		WebUI.callTestCase(findTestCase('CPE/TC-Login Mercant'), [('email') : email, ('password') : password, ('status') : 'Success'],
		FailureHandling.STOP_ON_FAILURE)

		WebUI.navigateToUrl("https://app-uat.doku.com/bo/integration/simulator")
		WebUI.maximizeWindow()
		WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - iframe'), 30)

		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR019 - ChooseSimulatorMandiri'))
		WebUI.setText(findTestObject('Object Repository/MPE X NCPE/OR010 - InputNoVA'), getNoVA)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR011 - ButtonSubmitVA'))
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR008 - ButtonPayVA'))
		WebUI.delay(5)
		WebUI.takeScreenshot()
		WebUI.closeBrowser()
	}

	@Keyword

	def paymentWithVABSI() {

		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR005 - ChooseOptionVA'))
		WebUI.takeScreenshot()
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR020 - ChooseVABSI'))

		def getNoVA = WebUI.getText(findTestObject('Object Repository/MPE X NCPE/OR008 - NoVA'))
		println getNoVA
		WebUI.takeScreenshot()

		WebUI.callTestCase(findTestCase('CPE/TC-Login Mercant'), [('email') : email, ('password') : password, ('status') : 'Success'],
		FailureHandling.STOP_ON_FAILURE)

		WebUI.navigateToUrl("https://app-uat.doku.com/bo/integration/simulator")
		WebUI.maximizeWindow()
		WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - iframe'), 30)

		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR021 - ChooseSimulatorBSI'))
		WebUI.setText(findTestObject('Object Repository/MPE X NCPE/OR010 - InputNoVA'), getNoVA)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR011 - ButtonSubmitVA'))
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR008 - ButtonPayVA'))
		WebUI.delay(5)
		WebUI.takeScreenshot()
		WebUI.closeBrowser()
	}

	@Keyword

	def paymentWithVABankLainnya() {

		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR005 - ChooseOptionVA'))
		WebUI.takeScreenshot()
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR022 - ChooseVABankLainnya'))

		def getNoVA = WebUI.getText(findTestObject('Object Repository/MPE X NCPE/OR008 - NoVA'))
		println getNoVA
		WebUI.takeScreenshot()

		WebUI.callTestCase(findTestCase('CPE/TC-Login Mercant'), [('email') : email, ('password') : password, ('status') : 'Success'],
		FailureHandling.STOP_ON_FAILURE)

		WebUI.navigateToUrl("https://app-uat.doku.com/bo/integration/simulator")
		WebUI.maximizeWindow()
		WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - iframe'), 30)

		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR023 - ChooseSimulatorBankLainnya'))
		WebUI.setText(findTestObject('Object Repository/MPE X NCPE/OR010 - InputNoVA'), getNoVA)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR011 - ButtonSubmitVA'))
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR008 - ButtonPayVA'))
		WebUI.delay(5)
		WebUI.takeScreenshot()
		WebUI.closeBrowser()
	}

	@Keyword
	def paymentWithAkulaku(String phoneAkulaku, String passwordAkulaku, String otp) {

		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR018 - ChooseOptionPaylater'))
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR019 - ChooseAkulaku'))
		WebUI.enableSmartWait()
		WebUI.setText(findTestObject('Object Repository/MPE X NCPE/OR020 - InputPhoneNumberAkulaku'), phoneAkulaku)
		WebUI.setText(findTestObject('Object Repository/MPE X NCPE/OR021 - InputPasswordAkulaku'), passwordAkulaku)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR022 - ButtonClickLoginAkulaku'))
		WebUI.delay(2)
		WebUI.setText(findTestObject('Object Repository/MPE X NCPE/OR023 - inputOTPAkulaku'), otp)
		WebUI.click(findTestObject('Object Repository/MPE X NCPE/OR022 - ButtonClickLoginAkulaku'))
		WebUI.takeScreenshot()
		WebUI.disableSmartWait()
	}
}
