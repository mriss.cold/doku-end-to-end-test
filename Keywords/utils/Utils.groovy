package utils
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject as JSONObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ResponseObject

//import doku.mib.util.MIBEncryptionUtil as MIBEncryptionUtil;
import groovy.json.JsonSlurper
import groovy.util.slurpersupport.NodeChild

import java.security.KeyFactory;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64 as B64;

public class Utils {
	@Keyword
	def generateInvoice() {
		Date dte=new Date();
		long milliSeconds = dte.getTime();
		String strLong = Long.toString(milliSeconds);
		return strLong;
	}

	@Keyword
	def generateRandom() {
		Random random = new Random();
		StringBuilder sb = new StringBuilder();

		// first not 0 digit
		sb.append(random.nextInt(9) + 1);

		// rest of 11 digits
		for (int i = 0; i < 11; i++) {
			sb.append(random.nextInt(10));
		}

		return sb.toString();
	}

	//words SHA1
	@Keyword
	def createWords(String element){
		MessageDigest sha1 = MessageDigest.getInstance("SHA1")
		byte[] digest = sha1.digest(element.getBytes())
		return new BigInteger(1, digest).toString(16).padLeft( 40, '0' )
	}

	static def generateWordsSHA256(String words) {
		MessageDigest sha256 = MessageDigest.getInstance("SHA-256")
		byte[] digest  = sha256.digest(words.getBytes());
		String result = new  BigInteger(1, digest).toString(16).padLeft(40, '0')
		return result;
	}


	//SHA256
	def sha256Hash = { element ->
		java.security.MessageDigest.getInstance("SHA-256").digest(element.bytes).collect {String.format("%02x",it)}.join('')
	}
	//SHA1
	def sha1Hash = {element ->
		java.security.MessageDigest.getInstance("SHA-1").digest(element.bytes).collect {String.format("%02x",it)}.join('')
	}
	//generateWords
	@Keyword
	def generateSignature(String formatWords, String hashGeneratorType){
		String element = formatWords
		String words = ''
		switch (hashGeneratorType) {
			case "SHA1":
				words = sha1Hash(element)
				break
			case "SHA256":
				words = sha256Hash(element)
				break
		}
		println "---generate words with "+hashGeneratorType+"---"
		println 'the element : '+element
		println 'the words : '+words
		println '---'
		return words
	}

	@Keyword
	def generatHmacSha256Base64Hash(String componentData, String secretKey) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {

		Mac sha256Hmac = Mac.getInstance("HmacSHA256");
		SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes("UTF-8"), "HmacSHA256");
		sha256Hmac.init(secret_key);

		return Base64.encodeBase64String(sha256Hmac.doFinal(componentData.getBytes("UTF-8")));
	}

	@Keyword
	def getSafeUrl(String componentData, String secretKey) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {

		Mac sha256Hmac = Mac.getInstance("HmacSHA256");
		SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes("UTF-8"), "HmacSHA256");
		sha256Hmac.init(secret_key);

		//		return Base64.getUrlEncoder().encodeToString(sha256Hmac.doFinal(componentData.getBytes(UTF_8)));
		return Base64.encodeBase64URLSafeString(sha256Hmac.doFinal(componentData.getBytes("UTF-8")));
	}

	//	@Keyword
	//	def generatHmacSha256Base64Hash(String componentData, String secretKey) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
	//
	//	byte[] decodedKey = secretKey.getBytes();
	//	SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "HmacSHA256");
	//	Mac hmacSha256 = Mac.getInstance("HmacSHA256");
	//	hmacSha256.init(originalKey);
	//	hmacSha256.update(componentData.getBytes());
	//	byte[] HmacSha256DigestBytes = hmacSha256.doFinal();
	//
	//	return Base64.getEncoder().encodeToString(HmacSha256DigestBytes);
	//	}

	@Keyword
	def generateSha256Base64Hash(String componentData){
		String component = componentData
		MessageDigest digest = MessageDigest.getInstance("SHA-256");

		byte[] hash = digest.digest(component.getBytes(StandardCharsets.UTF_8));
		return DatatypeConverter.printBase64Binary(hash)
	}


	@Keyword
	def getXMLValue(String xmlValue, String tagData) {
		String resultData = new XmlSlurper().parseText(xmlValue).getProperty(tagData).toString();
		println(resultData);
		return (resultData);
	}

	@Keyword
	def generateEndPoint(String env, String path) {
		println("CONTAINER_DEV: " + env)
		String endPoint = ""
		if (env != null){
			if (path.contains('MVA')){
				endPoint = "http://cm-mva:8080/"
			} else if (path.contains('MIB')){
				endPoint = "http://cm-mib:8080/"
			} else {
				endPoint = "http://cm-mpg:8080/"
			}
		} else {
			endPoint = "http://localhost:8080/"
		}
		endPoint += path
		println("END_POINT: " + endPoint)
		return endPoint
	}

	@Keyword
	def jsonParsingToMap (ResponseObject response) {
		String temp = response.getResponseBodyContent()
		def jsonSlurper = new JsonSlurper()
		def object = jsonSlurper.parseText(temp)
		return object //as Map
	}

	//	@Keyword
	//	def generateSignatureBriCeria(String wordsComponents) {
	//		MIBEncryptionUtil encUtil = new MIBEncryptionUtil()
	//		println(wordsComponents)
	//		def words = encUtil.encrypt(wordsComponents)
	//		println(words)
	//		return words
	//	}

	@Keyword
	def xmlParsing (ResponseObject response) {
		String temp = response.getResponseBodyContent()
		def xmlSlurper = new XmlSlurper()
		def object = xmlSlurper.parseText(temp)
		return object
	}

	def getXmlPropertyValue (ResponseObject response, String key) {
		NodeChild object = xmlParsing(response)
		return object.getProperty(key).text()
	}

	@Keyword
	def checkStringValue(String data) {
		if (data != '' && data != null) {
			return true
		}
		return false
	}

	@Keyword
	def boolean checkXMLValue(ResponseObject object , String data) {

		String finalData = new XmlSlurper().parseText(object.getResponseBodyContent()).getProperty(data).toString();
		if(finalData != null && finalData != ''){
			return true;
		}
		return false;
	}

	@Keyword
	static def generateDate() {
		TimeZone.setDefault(TimeZone.getTimeZone('GMT+7'))
		def now = new Date()
		String req_request_date_time= now.format('yyyyMMddHHmmss')
		println 'payment date :'+req_request_date_time
		return req_request_date_time
	}


	@Keyword
	static def generateLocalTime() {
		TimeZone.setDefault(TimeZone.getTimeZone('GMT+7'))
		def now = new Date()
		String req_request_date_time= now.format('MMddHHmmss')
		println 'payment date :'+req_request_date_time
		return req_request_date_time
	}

	@Keyword
	static def generateGMTTime() {
		TimeZone.setDefault(TimeZone.getTimeZone('GMT+0'))
		def now = new Date()
		String req_request_date_time= now.format('MMddHHmmss')
		println 'payment date :'+req_request_date_time
		return req_request_date_time
	}

	@Keyword
	static def generateLocalTime2() {
		TimeZone.setDefault(TimeZone.getTimeZone('GMT+7'))
		def now = new Date()
		String req_request_date_time= now.format('dd/MM/yyyy HH:MM:ss')
		println 'payment date :'+req_request_date_time
		return req_request_date_time
	}

	@Keyword
	def checkJsonValue(ResponseObject object , String key) {
		JSONObject jsonObject = new JSONObject(object.getResponseBodyContent())

		String keyValue =  jsonObject.getString(key)
		if(keyValue != null && keyValue !='') {

			return true;
		}
		return false;
	}

	@Keyword
	def getJsonValue(ResponseObject obj, String val){

		JSONObject jsonObj = new JSONObject(obj.getResponseBodyContent())
		String keyVal =  jsonObj.getString(val)
		return (keyVal)
	}

	@Keyword
	static def generateAmount() {
		DecimalFormat df2 = new DecimalFormat("0");
		double min = 1;
		double max = 99999;
		double rand = new Random().nextDouble();
		String result = df2.format(min + (rand * (max - min)));
		return result
	}

	@Keyword
	static def generateDate1HourBeforeNow() {
		TimeZone.setDefault(TimeZone.getTimeZone('GMT+6'))
		def now = new Date()
		String req_request_date_time= now.format('yyyy-MM-dd HH:mm:ss.SSS')
		println 'payment date :'+req_request_date_time
		return req_request_date_time
	}

	@Keyword
	static def generateDateNow() {
		TimeZone.setDefault(TimeZone.getTimeZone('GMT+7'))
		def now = new Date()
		String req_request_date_time= now.format('yyyy-MM-dd HH:mm:ss.SSS')
		println 'payment date :'+req_request_date_time
		return req_request_date_time
	}

	@Keyword
	static def generateDate24HourBeforeNow() {
		TimeZone.setDefault(TimeZone.getTimeZone('GMT-17'))
		def now = new Date()
		String req_request_date_time= now.format('yyyy-MM-dd HH:mm:ss.SSS')
		println 'payment date :'+req_request_date_time
		return req_request_date_time
	}

	@Keyword
	static def generateDate23HourBeforeNow() {
		TimeZone.setDefault(TimeZone.getTimeZone('GMT-16'))
		def now = new Date()
		String req_request_date_time= now.format('yyyy-MM-dd HH:mm:ss.SSS')
		println 'payment date :'+req_request_date_time
		return req_request_date_time
	}

	@Keyword
	static def generateDate1HourAfterNow() {
		TimeZone.setDefault(TimeZone.getTimeZone('GMT+8'))
		def now = new Date()
		String req_request_date_time= now.format('yyyy-MM-dd HH:mm:ss.SSS')
		println 'payment date :'+req_request_date_time
		return req_request_date_time
	}

	//	@Keyword
	//	static def encodeId(String encrpyt){
	//		String encode = new String(Base64.encode(encrpyt));
	//		return encode;
	//	}




	public String generateSignatureRSA(String input, String strPk) throws Exception {
		// Remove markers and new line characters in private key
		String realPK = strPk.replaceAll("-----END PRIVATE KEY-----", "")
				.replaceAll("-----BEGIN PRIVATE KEY-----", "")
				.replaceAll("\n", "");

		byte[] b1 = B64.getDecoder().decode(realPK);
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(b1);
		KeyFactory kf = KeyFactory.getInstance("RSA");

		Signature privateSignature = Signature.getInstance("SHA256withRSA");
		privateSignature.initSign(kf.generatePrivate(spec));
		privateSignature.update(input.getBytes("UTF-8"));
		byte[] s = privateSignature.sign();
		return B64.getEncoder().encodeToString(s);
	}

	//Random number cvv
	@Keyword
	def static randomCvv() {
		int min = 100;
		int max = 999;

		String random_int = (int)Math.floor(Math.random()*(max-min+1)+min)
		return random_int
	}
}
