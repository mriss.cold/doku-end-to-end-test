package devex.utils

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory

import java.nio.charset.StandardCharsets
import java.security.InvalidKeyException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

import com.kms.katalon.core.testobject.ResponseObject
//import org.json.JSONObject

import org.json.JSONObject as JSONObject
import org.apache.commons.lang.RandomStringUtils

import javax.crypto.spec.SecretKeySpec
import javax.crypto.Mac
import java.security.SignatureException

import java.time.format.DateTimeFormatter
import java.time.LocalDateTime

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.text.DateFormat;

public class Utils {
	//url form test oco di dev.dokupay
	@Keyword
	static def getURLFormTestOCO() {

		def containerEnv = System.getenv("CONTAINER_ENV");

		String url = GlobalVariable.pathFormOCO+"/form/FormTestOpenshiftATMsimulator.html"

		//if not null, we assume this script run on environment dev openshift (NTT)
		print("CONTAINER_ENV : "+containerEnv);
		if (containerEnv != null) {
			url = GlobalVariable.pathFormOCO+"/form/FormTestOpenshiftATMsimulator.html"
		}

		return url
	}

	//url atm simulator di dev.dokupay
	@Keyword
	static def getURLATMSimulator() {

		def containerEnv = System.getenv("CONTAINER_ENV");

		String url = GlobalVariable.pathurl+"/integration/simulator/"

		//if not null, we assume this script run on environment dev openshift (NTT)
		print("CONTAINER_ENV : "+containerEnv);
		if (containerEnv != null) {
			url = GlobalVariable.pathurl+"/integration/simulator/"
		}

		return url
	}

	//url form test check status oco di dev.dokupay
	@Keyword
	static def getURLFormTestCheckStatusOCO() {

		def containerEnv = System.getenv("CONTAINER_ENV");

		String url = GlobalVariable.pathFormOCO+"/form/FormCheckStatusOco.html"

		//if not null, we assume this script run on environment dev openshift (NTT)
		print("CONTAINER_ENV : "+containerEnv);
		if (containerEnv != null) {
			url = GlobalVariable.pathFormOCO+"/form/FormCheckStatusOco.html"
		}

		return url
	}

	//url test integration dari merchant side
	@Keyword
	static def getURLMerchantIntegration() {

		def containerEnv = System.getenv("CONTAINER_ENV");

		String url = GlobalVariable.pathurl+"/integration/test-scenario/"

		//if not null, we assume this script run on environment dev openshift (NTT)
		print("CONTAINER_ENV : "+containerEnv);
		if (containerEnv != null) {
			url = GlobalVariable.pathurl+"/integration/test-scenario/"
		}

		return url
	}

	//url test integration dari admin side (test collection)
	@Keyword
	static def getURLAdminSide() {

		def containerEnv = System.getenv("CONTAINER_ENV");

		String url = GlobalVariable.pathurl+"/integration/test-scenario/test-collections"

		//if not null, we assume this script run on environment dev openshift (NTT)
		print("CONTAINER_ENV : "+containerEnv);
		if (containerEnv != null) {
			url = GlobalVariable.pathurl+"/integration/test-scenario/test-collections"
		}

		return url
	}


	//url test integration dari admin side (test scenario)
	@Keyword
	static def getURLAdminSideTestScenario() {

		def containerEnv = System.getenv("CONTAINER_ENV");

		String url = GlobalVariable.pathurl+"/integration/test-scenario/test-case"

		//if not null, we assume this script run on environment dev openshift (NTT)
		print("CONTAINER_ENV : "+containerEnv);
		if (containerEnv != null) {
			url = GlobalVariable.pathurl+"/integration/test-scenario/test-case"
		}

		return url
	}

	//url back offcie RR untuk login
	@Keyword
	static def getURLBackOfficeRR() {

		def containerEnv = System.getenv("CONTAINER_ENV");

		String url = GlobalVariable.pathurl+"/bo/login"

		//if not null, we assume this script run on environment dev openshift (NTT)
		print("CONTAINER_ENV : "+containerEnv);
		if (containerEnv != null) {
			url = GlobalVariable.pathurl+"/bo/login"
		}

		return url
	}

	//url untuk java demo app
	@Keyword
	static def getURLJavaDemoApp() {

		def containerEnv = System.getenv("CONTAINER_ENV");

		String url = GlobalVariable.pathurl+"/demo/java-library/"

		//if not null, we assume this script run on environment dev openshift (NTT)
		print("CONTAINER_ENV : "+containerEnv);
		if (containerEnv != null) {
			url = GlobalVariable.pathurl+"/demo/java-library/"
		}

		return url
	}

	//url untuk php demo app
	@Keyword
	static def getURLPHPDemoApp() {

		def containerEnv = System.getenv("CONTAINER_ENV");

		String url = GlobalVariable.pathurl+"/demo/php-library/index.php"

		//if not null, we assume this script run on environment dev openshift (NTT)
		print("CONTAINER_ENV : "+containerEnv);
		if (containerEnv != null) {
			url = GlobalVariable.pathurl+"/demo/php-library/index.php"
		}

		return url
	}

	//url untuk node.js demo app
	@Keyword
	static def getURLNodejsDemoApp() {

		def containerEnv = System.getenv("CONTAINER_ENV");

		String url = GlobalVariable.pathurl+"/demo/nodejs-library"

		//if not null, we assume this script run on environment dev openshift (NTT)
		print("CONTAINER_ENV : "+containerEnv);
		if (containerEnv != null) {
			url = GlobalVariable.pathurl+"/demo/nodejs-library"
		}

		return url
	}

	//url untuk python demo app
	@Keyword
	static def getDemoAppUrl(String targetPath) {

		def containerEnv = System.getenv("CONTAINER_ENV");

		String url = GlobalVariable.pathurl+targetPath

		//if not null, we assume this script run on environment dev openshift (NTT)
		print("CONTAINER_ENV : "+containerEnv);
		if (containerEnv != null) {
			url = GlobalVariable.pathurl+targetPath
		}

		return url
	}

	//generate 11 digit with parameter
	@Keyword
	static def generator = { String alphabet, int n ->
		new Random().with {
			(1..n).collect { alphabet[ nextInt( alphabet.length() ) ] }.join()
		}
	}

	//generate 11 digit without parameter
	@Keyword
	static def generateUserAccountRandom() {
		return generator( (('0'..'9')).join(), 11 )

	}

	/**
	 * Execute/evaluate xpath
	 * @param String or testObject
	 * @param RESULT_TYPE: [NUMBER_TYPE|STRING_TYPE|BOOLEAN_TYPE]
	 * @return result from xpath
	 */

	@Keyword
	def exexpath(String xpth, String RESULT_TYPE) {
		def resultTypes = ['NUMBER_TYPE': 'numberValue','STRING_TYPE': 'stringValue', 'BOOLEAN_TYPE': 'booleanValue']
		WebDriver wd = DriverFactory.getWebDriver()
		try{
			String execCmd = 'return (document.evaluate(\''+xpth+'\', document, null, XPathResult.ANY_TYPE, null)).'+resultTypes[RESULT_TYPE]+';'
			return ((JavascriptExecutor) wd).executeScript(execCmd,[xpth])
		} catch (Exception e){
			println e
			return null
		}
	}

	def exexpath(TestObject to, String RESULT_TYPE) {
		try{
			exexpath(to.findPropertyValue("xpath"), RESULT_TYPE)
		} catch (Exception e){
			println e
			return null
		}
	}

	//encrypt using SHA256
	@Keyword
	static def generateSignatureSHA256(String words) {
		MessageDigest sha256 = MessageDigest.getInstance("SHA-256")
		byte[] digest  = sha256.digest(words.getBytes());
		String result = new  BigInteger(1, digest).toString(16).padLeft(40, '0')
		return result;
	}

	//SHA256
	def sha256Hash = { element ->
		java.security.MessageDigest.getInstance("SHA-256").digest(element.bytes).collect {String.format("%02x",it)}.join('')
	}

	//SHA1
	def sha1Hash = {element ->
		java.security.MessageDigest.getInstance("SHA-1").digest(element.bytes).collect {String.format("%02x",it)}.join('')
	}

	//generateWords
	@Keyword
	def generateWordsAsString(String formatWords, String hashGeneratorType){
		String element = formatWords
		String words = ''
		switch (hashGeneratorType) {
			case "SHA1":
				words = sha1Hash(element)
				break
			case "SHA256":
				words = sha256Hash(element)
				break
		}
		return words
	}


	//generate invoice
	@Keyword
	static def generateInvoice() {
		Date dte=new Date();
		long milliSeconds = dte.getTime();
		String strLong = Long.toString(milliSeconds);
		return strLong;
	}

	//get specific tag in json response
	@Keyword
	def getJsonValue(ResponseObject obj, String val){

		JSONObject jsonObj = new JSONObject(obj.getResponseBodyContent())
		String keyVal =  jsonObj.getString(val)
		return (keyVal)
	}

	//generate random number
	def generateRandomNumber(){
		String number = RandomStringUtils.randomNumeric(11)
		return number
	}

	//generate date
	@Keyword
	def generateDate(){
		String dateString = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now());
	}

	//generate using HmacSHA256
	static def generateWordsHmacSHA256(String words, String sharedKey) throws java.security.SignatureException
	{
		String result
		try {

			// get an hmac_sha1 key from the raw key bytes
			SecretKeySpec signingKey = new SecretKeySpec(sharedKey.getBytes(), "HmacSHA256");

			// get an hmac_sha1 Mac instance and initialize with the signing key
			Mac mac = Mac.getInstance("HmacSHA256");
			mac.init(signingKey);

			// compute the hmac on input data bytes
			byte[] rawHmac = mac.doFinal(words.getBytes());
			result= rawHmac.encodeHex()
			println rawHmac

		} catch (Exception e) {
			throw new SignatureException("Failed to generate HmacSHA256 : " + e.getMessage());
		}

		return result
		println sharedKey
	}

	//generate date ISO 8601
	@Keyword
	def generateDateISO8601() {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());
	}

	//generate hmacsha256 for header
	public static String hmacSHA256(String component, String secret) throws InvalidKeyException, NoSuchAlgorithmException {
		byte[] decodedKey = secret.getBytes();
		SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "HmacSHA256");
		Mac hmacSha256 = Mac.getInstance("HmacSHA256");
		hmacSha256.init(originalKey);
		hmacSha256.update(component.getBytes());
		byte[] HmacSha256DigestBytes = hmacSha256.doFinal();
		return Base64.getEncoder().encodeToString(HmacSha256DigestBytes);
	}

	//hash body json using sha256 then encoded using base64 for bri syariah
	@Keyword
	def getComponent(def request_body){
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(request_body.getBytes(StandardCharsets.UTF_8));
		byte[] digest = md.digest();
		return Base64.getEncoder().encodeToString(digest);
		WS.comment('encoded data:   '+encoded)
		WS.comment('body data:   '+body_data)
		return encoded
	}

	//generate signature virtual account bri syariah
	@Keyword
	def generateSignatures(String client_id, String requestId, String timestamp, String body, String request_target,String sharedKey){
		//String sharedKey = 'TYUI56&*'
		def	sign = "Client-Id:"+client_id+"\n"+"Request-Id:"+requestId+"\n"+"Request-Timestamp:"+timestamp+"\n"+"Request-Target:"+request_target+"\n"+"Digest:"+body
		def	signature_result = hmacSHA256(sign, sharedKey)
		def signature =  'HMACSHA256='+signature_result
		WS.comment('sign:  '+sign)
		WS.comment('signature_result:  '+signature_result)
		return signature
	}

	//generate signature v2
	@Keyword
	def generateSignaturesV2(String client_id, String requestId, String timestamp,String request_target,String body,String sharedKey){
		//		String sharedKey = 'TYUI56&*'
		def	sign = "Client-Id:"+client_id+"\n"+"Request-Id:"+requestId+"\n"+"Request-Timestamp:"+timestamp+"\n"+"Request-Target:"+request_target+"\n"+"Digest:"+ body
		def	signature_result = hmacSHA256(sign, sharedKey)
		def signature =  'HMACSHA256='+signature_result
		WS.comment('sign:  '+sign)
		WS.comment('signature_result:  '+signature_result)
		return signature
	}
	@Keyword
	def generateSignaturesV3(String client_id, String requestId, String timestamp,String request_target,String sharedKey){
		//		String sharedKey = 'TYUI56&*'
		def	sign = "Client-Id:"+client_id+"\n"+"Request-Id:"+requestId+"\n"+"Request-Timestamp:"+timestamp+"\n"+"Request-Target:"+request_target
		def	signature_result = hmacSHA256(sign, sharedKey)
		def signature =  'HMACSHA256='+signature_result
		WS.comment('sign:  '+sign)
		WS.comment('signature_result:  '+signature_result)
		return signature
	}

	//generate signature v2
	@Keyword
	def generateSignaturesOvo(String client_id, String requestId, String timestamp,String request_target, String body,String sharedKey){
		//		String sharedKey = 'TYUI56&*'
		def	sign = "Client-Id:"+client_id+"\n"+"Request-Id:"+requestId+"\n"+"Request-Timestamp:"+timestamp+"\n"+"Request-Target:"+request_target+"\n"+"Digest:"+ body
		def	signature_result = hmacSHA256(sign, sharedKey)
		def signature =  'HMACSHA256='+signature_result
		WS.comment('sign:  '+sign)
		WS.comment('signature_result:  '+signature_result)
		return signature
	}

	//Generate Signature
	@Keyword
	def generateSignatureOvoV1(String amount,String clientId,String invoiceNumber,String ovoId,String sharedKey) {
		def	sign = amount+clientId+invoiceNumber+ovoId+sharedKey
		def	signature_result = generateSignatureSHA256(sign)
		def signature =signature_result
		WS.comment('sign:  '+sign)
		WS.comment('signature_result:  '+signature_result)
		return signature
	}

}
