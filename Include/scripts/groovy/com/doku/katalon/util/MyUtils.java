package com.doku.katalon.util;

import java.io.File;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.kms.katalon.core.configuration.RunConfiguration;
import com.kms.katalon.core.testobject.SelectorMethod;
import com.kms.katalon.core.testobject.TestObject;
import com.kms.katalon.core.webui.driver.DriverFactory;

/**
 * Case Samples:
 * - UpperCamelCase
 * - lowerCamelCase
 * - snake_case
 * - kebab-case
 * 
 * @author Daniel Joi Partogi Hutapea
 */


public class MyUtils
{
	public static final String KEY_UNIQUE_ID = "KEY_UNIQUE_ID";
	public static final String KEY_NAMA_LENGKAP = "KEY_NAMA_LENGKAP";
	public static final String KEY_NAMA_LENGKAP_ORIGINAL = "KEY_NAMA_LENGKAP_ORIGINAL";
	public static final String KEY_NAMA_MEREK = "KEY_NAMA_MEREK";
	public static final String KEY_EMAIL = "KEY_EMAIL";
	public static final String KEY_EMAIL_PERUSAHAAN = "KEY_EMAIL_PERUSAHAAN";
	public static final String KEY_BUSINESS_TYPE = "KEY_BUSINESS_TYPE";
	public static final String KEY_NAMA_PERUSAHAAN = "KEY_NAMA_PERUSAHAAN";
	public static final String KEY_EMAIL_SALES = "KEY_EMAIL_SALES";
	public static final String KEY_PHONE_NUMBER = "KEY_PHONE_NUMBER";
	public static final String KEY_PHONE_NUMBER_PERUSAHAAN = "KEY_PHONE_NUMBER_PERUSAHAAN";
	public static final String KEY_ABOUT_BRAND = "KEY_ABOUT_BRAND";
	public static final String KEY_KODE_POS = "KEY_KODE_POS";
	public static final String KEY_ALAMAT_LENGKAP = "KEY_ALAMAT_LENGKAP";
	public static final String KEY_WEBSITE = "KEY_WEBSITE";
	public static final String KEY_FACEBOOK = "KEY_FACEBOOK";
	public static final String KEY_INSTAGRAM = "KEY_INSTAGRAM";
	public static final String KEY_TWITTER = "KEY_TWITTER";
	public static final String KEY_WHATSAPP = "KEY_WHATSAPP";
	public static final String KEY_BUSINESS_ID = "KEY_BUSINESS_ID";
	public static final String KEY_KATEGORI_BISNIS = "KEY_KATEGORI_BISNIS";
	public static final String KEY_NO_NPWP = "KEY_NO_NPWP";
	public static final String KEY_NO_NIB = "KEY_NO_NIB";

	public static final ThreadLocal<Map<String, Object>> MAP_VARS_TL = new ThreadLocal<Map<String, Object>>()
	{
		protected Map<String, Object> initialValue()
		{
	        return new HashMap<>();
	    }
	};

	
	public static String getUniqueId()
	{
		String uniqueId;
		
		synchronized(getMapVars())
		{
			uniqueId = getOrDefault(KEY_UNIQUE_ID, System.currentTimeMillis()+ "");
			put(KEY_UNIQUE_ID, uniqueId);
		}
		return uniqueId;
	}

	public static void clearMapVars()
	{
		synchronized(getMapVars())
		{
			getMapVars().clear();
		}
	}

	public static Map<String, Object> getMapVars()
	{
		return MAP_VARS_TL.get();
	}

	public static void put(String key, Object value)
	{
		getMapVars().put(key, value);
	}

	@SuppressWarnings("unchecked")
	public static <V extends Object> V get(String key)
	{
		return (V) getMapVars().get(key);
	}

	@SuppressWarnings("unchecked")
	public static <V extends Object> V getOrDefault(String key, V defaultValue)
	{
		return (V) getMapVars().getOrDefault(key, defaultValue);
	}

	public static String generatePhoneNumber()
	{
		return "8" + Instant.now().getEpochSecond();
	}

	/**
	 * No KTP should be 16 digit number.
	 */
	public static String generateNoKtp()
	{
		return "999" + getUniqueId();
	}
	
	/**
	 * No NPWP should be 15 digit number.
	 */
	public static String generateNoNpwp()
	{
		return "00" + getUniqueId();
	}

	
	public static String generateNoNib()
	{
		return getUniqueId();
	}
	
	public static String getDataFilesAbsolutePath(String resourceRelativePath)
	{
		String userDir = RunConfiguration.getProjectDir();
	    File file = new File(userDir + "/Data Files" + resourceRelativePath);
	    return file.getAbsolutePath();
	}
	
	public static String getSelectOptionValue(String xpathSelectOption, String xpathIframeParent) 
	{
		WebDriver webDriver = DriverFactory.getWebDriver();
		WebElement iframeParentWe = webDriver.findElement(By.xpath(xpathIframeParent));
		webDriver.switchTo().frame(iframeParentWe);
		Select select = new Select(webDriver.findElement(By.xpath(xpathSelectOption)));
		String actualSelectValue = select.getFirstSelectedOption().getAttribute("value");
		webDriver.switchTo().parentFrame();
		return actualSelectValue;
	}
	
	public static String getSelectOptionValue(TestObject selectOptionTo, TestObject iframeParentTo) 
	{
		return getSelectOptionValue(selectOptionTo.getSelectorCollection().get(SelectorMethod.XPATH), iframeParentTo.getSelectorCollection().get(SelectorMethod.XPATH));
	}
}
