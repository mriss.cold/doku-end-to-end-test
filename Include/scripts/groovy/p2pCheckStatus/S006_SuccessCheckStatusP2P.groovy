package p2pCheckStatus
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import utils.Utils as Utils
import org.apache.commons.lang.RandomStringUtils
import java.sql.DriverManager
import groovy.json.JsonSlurper



class S006_SuccessCheckStatusP2P {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	def clientId=""
	//def sharedKey="/3qkYfeqVExhlDS+IC+3BA==|DEV-AA|2|2|DJ0Tyfb1+rXREz+4m4NEHg=="
	def sharedKey=""
	def paymentCode=""
	def invoiceNumber=""
	def requestId=""
	def amount=""
	def name=""
	def email=""
	def expiredTime=""
	def info1="Test1"
	def info2="Test2"
	def info3="Test3"
	def kota="Jakarta Barat"
	def provinsi="DKI Jakarta"
	def username="apriwida+02sept202101@doku.com"
	def sendReq
	def minAmount=""
	def maxAmount=""
	def result
	def responseJson=""
	def status=""
	def requestIdCreatedOrder=""
	def response=""
	def merchantUniqueReference=""

	@Given("Customer succes generate P2P transaction with valid clientId and sharedKey (.*) (.*)")
	def customer_success_generate_P2P_transaction(String clientId, String sharedKey) {
		WebUI.comment("Client Id: "+clientId)
		WebUI.comment("Shared Key: "+sharedKey)
		invoiceNumber = RandomStringUtils.randomAlphanumeric(11);
		WebUI.comment("Invoice Number: "+invoiceNumber)
		Date date = new Date()
		Utils util = new Utils()
		def currentDate = date.format('yyyymmddhhmmss')
		String timeStamp = util.generateDateISO8601()
		requestIdCreatedOrder = RandomStringUtils.randomNumeric(11);
		merchantUniqueReference=RandomStringUtils.randomAlphanumeric(10)

		"------------------------Create Request Body----------------------------"
		RequestObject request = findTestObject('Object Repository/P2P/Wr001_CreateOrder', [('invoiceNumber') : invoiceNumber,('merchantUniqueReference') : merchantUniqueReference])
		String jsonBody =request.getHttpBody()

		"------------------------Payload Request---------------------------------"
		WebUI.comment('Request Body: '+ jsonBody)
		String encoded_body= util.getComponent(jsonBody)
		WebUI.comment('Encoded Payload: '+ encoded_body)

		'------------------------Generate Signature------------------'
		//String signature =util.generateSignaturesV2(clientId,requestId,timeStamp,"/cimb-virtual-account/v2/payment-code",sharedKey)
		String signature = util.generateSignaturesV2(clientId,requestIdCreatedOrder,timeStamp,"/akulaku-peer-to-peer/v2/generate-order",encoded_body,sharedKey)
		WebUI.comment(' Signature '+signature)

		"----------------------Header Signature-------------------------"
		TestObjectProperty header1 = new TestObjectProperty('Signature', ConditionType.EQUALS, signature)
		TestObjectProperty header2 = new TestObjectProperty('Request-Id', ConditionType.EQUALS, requestIdCreatedOrder)
		TestObjectProperty header3 = new TestObjectProperty('Client-Id', ConditionType.EQUALS, clientId)
		TestObjectProperty header4 = new TestObjectProperty('Request-Timestamp', ConditionType.EQUALS, timeStamp)
		TestObjectProperty header5 = new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json')

		ArrayList<TestObjectProperty> defaultHeaders = Arrays.asList(header1, header2, header3, header4, header5)

		WebUI.comment('Passed headers: ' + defaultHeaders)
		request.setHttpHeaderProperties(defaultHeaders)

		"------------------------Send Request-------------------------------"
		sendReq = WS.sendRequest(request);
		WebUI.comment('SEND: '+sendReq)

		"------------------------Get Response---------------------------------"
		result = sendReq.getResponseBodyContent()
		WebUI.comment('Response: '+result)

		"-------------------------Get status Code-------------------------------"
		String status_code = sendReq.getStatusCode()

		"-----------------------Get Response---------------------------"
		if(WS.verifyResponseStatusCode(sendReq, 200, FailureHandling.OPTIONAL)) {
			/*JsonSlurper slurper = new JsonSlurper()
			 Map parsedJson = slurper.parseText(result)
			 String paycode = parsedJson.virtual_account_info.virtual_account_number
			 paymentCode=paycode
			 WebUI.comment("Payment code: "+paymentCode)*/
			WebUI.comment('Response: '+result)
		}
		else {
			KeywordUtil.markFailed(result)
		}
	}

	@When("P2P merchant hit check status with valid clientId and sharedKey (.*) (.*)")
	def p2p_check_status_with_invoiceNumber(String clientId, String sharedKey) {
		"-----------------------------Initiate Data----------------------"
		WebUI.comment("invoiceNumber: "+invoiceNumber)
		WebUI.comment("client id : "+clientId)
		WebUI.comment("Shared key: "+sharedKey)
		Date date = new Date()
		Utils util = new Utils()
		def currentDate = date.format('yyyymmddhhmmss')
		String timeStamp = util.generateDateISO8601()
		String systraceNumber = RandomStringUtils.randomNumeric(11)
		String requestId = RandomStringUtils.randomNumeric(64);

		"---------------------------Generate Signature-----------------------"
		String signature = util.generateSignaturesV3(clientId,requestId,timeStamp,"/orders/v1/status/"+invoiceNumber,sharedKey)

		"----------------------------Signature hit API-------------------------"
		TestObjectProperty header1 = new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json')

		TestObjectProperty header2 = new TestObjectProperty('Client-Id', ConditionType.EQUALS, clientId)

		TestObjectProperty header3 = new TestObjectProperty('Signature', ConditionType.EQUALS, signature)

		TestObjectProperty header4 = new TestObjectProperty('Request-Id', ConditionType.EQUALS, requestId)

		TestObjectProperty header5 = new TestObjectProperty('Request-Timestamp', ConditionType.EQUALS, timeStamp)

		ArrayList<TestObjectProperty> defaultHeaders = Arrays.asList(header1, header2, header3, header4, header5)


		"----------------------------Created Request Object-------------------"
		RequestObject requestCheckStatus=findTestObject('Object Repository/CheckStatusApi/CheckStatusUat', [('id') :invoiceNumber])


		"--------------------------headers hit API-------------------------------"
		WebUI.comment('headers hit api :' + defaultHeaders)
		requestCheckStatus.setHttpHeaderProperties(defaultHeaders)
		def response = WS.sendRequest(requestCheckStatus)
		WebUI.delay(3)
		responseJson = response.getResponseBodyContent();
		String StatusCode=response.getStatusCode()

		if(WS.verifyResponseStatusCode(response, 200, FailureHandling.OPTIONAL)) {
			JsonSlurper slurper = new JsonSlurper()
			Map parsedJson = slurper.parseText(responseJson)
			status = parsedJson.get("transaction").get("status")
			//WebUI.comment("Response: "+responseJson)
			WebUI.comment(status)
		}
		else {
			KeywordUtil.markFailed("Response: "+response.getResponseBodyContent())
		}
	}

	@Then("P2P merchant verify check status P2P transaction is (.*)")
	def P2P_merchant_verify_the_status(String expectedStatus) {
		WebUI.comment('Status Response Success Expected :'+expectedStatus)
		WebUI.comment('response api success '+responseJson)
		JsonSlurper slurper = new JsonSlurper()
		Map parsedJson = slurper.parseText(responseJson)
		String status = parsedJson.transaction.status
		WebUI.comment("Response: "+responseJson)
		if(WebUI.verifyMatch(status, expectedStatus, true, FailureHandling.OPTIONAL))
		{
			WebUI.comment(status);
		}
		else {
			KeywordUtil.markFailedAndStop("Gagal Check Status")
		}
	}

	@And("P2P customer hit callback API for status SUCCESS payment with staus <status")
	def p2p_customer_hit_callback_api(String status) {

	}
}