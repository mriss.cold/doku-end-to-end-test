package paymentLinkNCPE
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.SimpleDateFormat

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable



class paymentLink {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("Go to Url Payment Link")
	def navigateToPaymentLink() {
		//WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - iframe'), 30)
		WebUI.navigateToUrl(GlobalVariable.url_paymentLink)
		WebUI.waitForElementPresent(findTestObject('Object Repository/NCPE/OR001 - ButtonAddPaymentLink'), 30)
	}

	@When("I click button add payment link")
	def clickButtonPayment() {
		WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - iframe'), 30)
		WebUI.click(findTestObject('Object Repository/NCPE/OR001 - ButtonAddPaymentLink'))
	}

	@And("Choose button single payment link")
	def chooseSinglePayment() {
		//		WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - iframe'), 30)
		//		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/NCPE/OR002 - ButtonSinglePaymentLink'))
	}

	@And("I fill form input (.*), (.*), (.*), (.*), (.*), (.*), and (.*)")
	def inputForm(String nama, String email, String address, String order_number, String item_name1, String price1, String qty1) {
		//WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - Iframe'), 30)

		WebUI.waitForElementPresent(findTestObject('Object Repository/NCPE/OR003 - CustomerName'), 30)

		WebUI.setText(findTestObject('Object Repository/NCPE/OR003 - CustomerName'), nama)
		WebUI.setText(findTestObject('Object Repository/NCPE/OR004 - CustomerEmail'), email)
		WebUI.setText(findTestObject('Object Repository/NCPE/OR005 - CustomerAddress'), address)

		WebUI.waitForElementPresent(findTestObject('Object Repository/NCPE/OR020 - OrderNumberInput'), 30)
		WebUI.setText(findTestObject('Object Repository/NCPE/OR020 - OrderNumberInput'), order_number)

		WebUI.setText(findTestObject('NCPE/OR013 - Item1'), item_name1)
		WebUI.setText(findTestObject('NCPE/OR015 - Price1'), price1)
		WebUI.setText(findTestObject('NCPE/OR017 - Quantity1'), qty1)

		WebUI.delay(3)
		WebUI.takeScreenshot()
	}

	@And("I fill input expiration date time")
	def expirationTime() {

		//WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - iframe'), 30)

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
		Calendar c = Calendar.getInstance()
		c.add(Calendar.DATE, 1)
		String expiration_time = sdf.format(c.getTime())
		println expiration_time

		WebUI.click(findTestObject('NCPE/OR010 - CustomerExpiryDate'))
		WebUI.setText(findTestObject('NCPE/OR010 - CustomerExpiryDate'), expiration_time)
		WebUI.delay(3)

		SimpleDateFormat sdf1 = new SimpleDateFormat("HH")
		Calendar c1 = Calendar.getInstance()
		c1.add(Calendar.HOUR_OF_DAY, 1)
		String expiration_hours = sdf1.format(c1.getTime())
		println expiration_hours

		WebUI.click(findTestObject('NCPE/OR012 - CustomerHours'))
		WebUI.setText(findTestObject('NCPE/OR012 - CustomerHours'), expiration_hours)
		WebUI.delay(3)

		SimpleDateFormat sdf2 = new SimpleDateFormat("mm")
		Calendar c2 = Calendar.getInstance()
		c2.add(Calendar.MINUTE, 1)
		String expiration_second = sdf1.format(c2.getTime())
		println expiration_second

		WebUI.click(findTestObject('NCPE/OR011 - CustomerMinuts'))
		WebUI.setText(findTestObject('NCPE/OR011 - CustomerMinuts'), expiration_second)
		WebUI.delay(3)
		WebUI.takeScreenshot()
	}

	@And("I fill form input again item (.*), (.*), and (.*)")
	def inputItemAgain(String item_name2, String price2, String qty2) {
		WebUI.delay(5)
		WebUI.click(findTestObject('NCPE/OR019 - ButtonAddItems'))
		WebUI.setText(findTestObject('NCPE/OR014 - Item2'), item_name2)
		WebUI.setText(findTestObject('NCPE/OR016 - Price2'), price2)
		WebUI.setText(findTestObject('NCPE/OR018 - Quantity2'), qty2)
	}
	@And("I checklist bill for address")
	def chooseBilling() {

		//WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - Iframe'), 30)
		WebUI.click(findTestObject('NCPE/OR006 - CheckboxBillTo'))
		WebUI.takeScreenshot()
	}

	@And("I input (.*), (.*) and (.*)")
	def inputBIlling(String bill_name, String phone, String bill_email) {

		//WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - Iframe'), 30)
		WebUI.setText(findTestObject('NCPE/OR007 - BillToName'), bill_name)
		WebUI.setText(findTestObject('NCPE/OR008 - BillToEmail'), bill_email)
		WebUI.setText(findTestObject('NCPE/OR009 - BillToPhone'), phone)
		WebUI.delay(3)
		WebUI.takeScreenshot()
	}

	@And("I click button create Link")
	def clickButton() {

		//WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - Iframe'), 30)
		WebUI.delay(3)
		WebUI.executeJavaScript('window.scrollTo(0, document.body.scrollHeight)', null)
		WebUI.verifyElementPresent(findTestObject('NCPE/OR021 - ButtonSubmit'), 20)
		WebUI.takeScreenshot()
		WebUI.maximizeWindow()
		WebUI.scrollToElement(findTestObject('NCPE/OR021 - ButtonSubmit'), 20)
		WebUI.click(findTestObject('NCPE/OR021 - ButtonSubmit'))
		WebUI.takeScreenshot()
		WebUI.verifyElementPresent(findTestObject('NCPE/OR022 - ModalPaymentLink'), 20)
		WebUI.click(findTestObject('NCPE/OR024 - ButtonFinalLinkPayment'))
		WebUI.takeScreenshot()
		WebUI.delay(3)
		WebUI.takeScreenshot()
		WebUI.delay(3)
	}


	@Then("I verify the toast alert successfully")
	def verifySuccess() {
		//WebUI.switchToFrame(findTestObject('Object Repository/NCPE/OR025 - Iframe'), 30)
		WebUI.takeScreenshot()
		WebUI.delay(3)
		WebUI.verifyElementText(findTestObject('NCPE/OR023 - LabelTextSuccess'), "Payment Link Successfully Created")
		WebUI.takeScreenshot()
		WebUI.delay(3)
	}
}