#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Create Invoice

  @tag1
  Scenario Outline: Create Payment Link complete successfully
    Given Go to Url Payment Link
    When I click button add payment link
    And  Choose button single payment link
    And I fill form input <nama>, <email>, <address>, <order_number>, <item_name1>, <price1>, and <qty1>
    And I fill input expiration date time
    And I fill form input again item <item_name2>, <price2>, and <qty2>
    And I checklist bill for address
    And I input <bill_name>, <phone>, and <bill_email>
    And I click button create Link
    Then I verify the toast alert successfully

    Examples: 
      | nama | email | address | order_number | item_name1 | price1 | qty1 | item_name2 | price2 | qty2 | bill_name | phone | bill_email |
      | Jasmine Nabila | jasmine@doku.com | Bekasi,tambun utara | INV/SIT/1234562022-07-25. | test1 | 25000 | 4 | test2 | 15000 | 3 | billing_jasmine | 081510229216 | jasminebills@doku.com |