#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Success Generate Virtual Account Number 
  I want to use this template for my feature file

 	#@tag1
  #Scenario Outline: Success verify check status API with invoiceNumber and valid clientId and sharedKey on Virtual Account channel
   # Given Merchant success generate va number code with invoice Number <clientId> and sharedKey <expiredTime> <amount> <reusableStatus> <info> <name> <email> <channel> <billingType>
   # When Merchant hit VA Check Status API with invoiceNumber and <clientId>
   # Then Merchant verify VA status is <expectedStatus>

    #Examples: 
 		#| clientId 		| sharedKey 			|	expiredTime	| amount  | reusableStatus 	| info				| name	| email 									| 	expectedStatus	| channel | billingType |
    #| 123-abc		|	chUnKr1nkZ 			|	120					| 90000		|	false						| Online_Shop	| Lily	| lily.anastasia@doku.com	|		PENDING					| VA 			| FIX_BILL |
    #| 123-abc			|									|	120					| 90000		|	false						| Online_Shop	| Lily	| lily.anastasia@doku.com	|		PENDING					| VA 			| FIX_BILL |
    #|MCH-0212-1641892329374 |						|	120					| 90000		|	false						| Online_Shop	| Lily	| lily.anastasia@doku.com	|		PENDING					| VA 			| FIX_BILL |
  #  @tag1
  #Scenario Outline: Success verify check status API with invoiceNumber and valid clientId and sharedKey on Virtual Account channel
   # Given Merchant success generate va number with invoice Number and additional Info <clientId> and sharedKey <expiredTime> <amount> <reusableStatus> <info> <name> <email> <channel> <billingType> <kota> <provinsi>
    #When Merchant hit VA Check Status API with invoiceNumber and <clientId>
    #Then Merchant verify VA status is <expectedStatus>

    #Examples: 
 		#| clientId 									| sharedKey 			|	expiredTime	| amount  | reusableStatus 	| info				| name	| email 									| 	expectedStatus	| channel | billingType |
    #| 123-abc										|	chUnKr1nkZ 			|	120					| 90000		|	false						| Online_Shop	| Lily	| lily.anastasia@doku.com	|		PENDING					| VA 			| FIX_BILL |
    #| MCH-0212-1641892329374			|									|	120					| 90000		|	false						| Online_Shop	| Lily	| lily.anastasia@doku.com	|		PENDING					| VA 			| FIX_BILL |
    
    
  @tag1
  Scenario Outline: Success verify check status API with requestId and valid clientId and sharedKey on Virtual Account channel
   	Given  Merchant success generate va number code with invoice Number <clientId> and sharedKey <expiredTime> <amount> <reusableStatus> <info> <name> <email> <channel> <billingType>
    When Merchant hit VA Check Status API with invoiceNumber and <clientId>
    And Merchant hit VA Check Status API with requestId and <clientId>
   Then Merchant verify VA status is <expectedStatus>

    Examples: 
 		| clientId 									| sharedKey 		|	expiredTime	| amount  | reusableStatus 	| info				| name	| email 									| 	expectedStatus	| channel | billingType |
    #| MCH-0212-1641892329374		|	 							|	120					| 90000		|	false						| Online_Shop	| Lily	| lily.anastasia@doku.com	|		PENDING					| VA 			| FIX_BILL |
    |MCH-0003-8279340109246 	|	 							|	120					| 90000		|	false						| Online_Shop	| Lily	| lily.anastasia@doku.com	|		PENDING					| VA 			| FIX_BILL |
    
    
    @tag1
  	Scenario Outline: Success check status when virtual account number is paid in Payment simulator page
    	Given Merchant success generate va number code with invoice Number <clientId> and sharedKey <expiredTime> <amount> <reusableStatus> <info> <name> <email> <channel> <billingType>
   		And Customer simulate order payment in Simulator Page
   		When Merchant hit VA Check Status API with invoiceNumber and <clientId>
    	Then Merchant verify VA status is <expectedStatus>

    Examples: 
 		| clientId 									| sharedKey 		|	expiredTime	| amount  | reusableStatus 	| info				| name	| email 									| 	expectedStatus	| channel | billingType |
   	| MCH-0003-8279340109246  	| 							|	120					| 90000		|	false						| Online_Shop	| Lily	| lily.anastasia@doku.com	|		SUCCESS					| VA 			| FIX_BILL |
    
    
    @tag1
  Scenario Outline: Success check status when va number is expired 
    Given Merchant success generate va number code with invoice Number <clientId> and sharedKey <expiredTime> <amount> <reusableStatus> <info> <name> <email> <channel> <billingType>
   	And Customer do payment when va number is expired
    When Merchant hit VA Check Status API with invoiceNumber and <clientId>
    Then Merchant verify VA status is <expectedStatus>

    Examples: 
 		| clientId 									| sharedKey 		|	expiredTime	| amount  | reusableStatus 	| info				| name	| email 									| 	expectedStatus	| channel | billingType |
    | MCH-0003-8279340109246		|	 							|	1						| 90000		|	true						| Online_Shop	| Lily	| lily.anastasia@doku.com	|		EXPIRED					| VA 			| FIX_BILL |