#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Success check status for Va Pending
  I want to use this template for my feature file

  @tag1
  Scenario Outline: Success check status for Va Pending with valid clientId, and invoiceNumber and sharedKey
    Given Customer generate va order with valid clientId and sharedKey <clientId> <sharedKey>
  	When Merchant hit check status API with valid clientId,invoiceNumber and sharedKey <clientId> <sharedKey>
  	Then Merchant verify the transaction status is <expectedTransactionStatus> 

    Examples: 
      | clientId  							| sharedKey 							| expectedTransactionStatus  |
      | MCH-0677-1637908268211	| SK-5AzDCd0i91t2a7U38Qc4 | PENDING |